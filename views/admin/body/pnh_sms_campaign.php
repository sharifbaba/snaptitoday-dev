<div class="container">
<h2>SMS Campaign</h2>
<form id="pnh_sms_camp_frm" method="post">
<table cellpadding="5">
<tr><td width="100" valign="center">To :</td>
<td>
<div style="float:left;padding:5px;border:1px solid #aaa;">
<?php foreach($frans as $f){?>
<div onclick='$("input",$(this)).toggleCheck()' style="white-space:nowrap;margin-bottom:3px;margin-right:3px;float:left;padding:3px;background:#eee;cursor:pointer;"><input type="checkbox" name="fids[]" value="<?=$f['franchise_id']?>" checked> <?=$f['franchise_name']?>, <?=$f['town']?>, <?=$f['city']?>, <?=$f['territory_name']?></div>
<?php }?>
<div class="clear"></div>
<div style="padding-top:5px;">select : <input onclick='$("input",$(this).parent().parent()).attr("checked",true)' type="button" value="All"> <input onclick='$("input",$(this).parent().parent()).attr("checked",false)' type="button" value="None"></div>
</div>
<div class="clear"></div>
</td>
</tr>
<tr><td>Send to :</td><td><label><input checked="checked" type="radio" name="send_to" value="1"> Both login mobiles</label> &nbsp; &nbsp;   <label><input type="radio" name="send_to" value="2"> Login mobile1 only</label></td></tr>
<tr>
<td>Type</td><td>
	<select name="type">
		<option value="1" selected="selected">Notification</option>
		<option value="2">Deal Promotion</option>
	</select>
</td>
</tr>
<tr id="deal_promo_blk" >
	<td><b>Choose Deal</b></td>
	<td valign="middle" style="vertical-align: middle;">
		<div id="srch_results"></div>
		<input type="text" class="inp" style="width:320px;" id="p_srch">
		<input type="hidden" name="sel_itemid" value="0">
		MRP:<input type="text" name="sel_itemmrp" size="2" value="0"> Offer:<input type="text" size="2" name="sel_itemprice" value="0">
	</td>
</tr>
<tr id="notify_msg_blk"><td>Message :</td><td><textarea style="width:500px;height:150px;" name="msg"></textarea></td></tr>
<tr><td></td><td><input type="submit" value="Send"></td></tr>
</table>
</form>
</div>

<style>
#deal_promo_blk,#notify_msg_blk{display: none;}
#srch_results{
	margin-left: 2px;
	position: absolute;
	display: none;
	width: 400px;
	overflow-y: auto;
	background: #EEE;
	border: 1px solid #AAA;
	max-height: 200px;
	min-width: 300px;
	max-width: 326px;
	overflow-x: hidden;
	margin-top: 30px;
}
#srch_results a{
	display: block;
	padding: 5px 6px;
	font-size: 12px;
	display: inline-table;
	width: 300px;
	text-transform: capitalize;
	border-bottom: 1px dotted #DDD;
	background: white;
} 
#srch_results a:hover{
background: #CCC;
color: black;
text-decoration: none;
}
</style>

<script type="text/javascript">
var jHR=0,search_timer;

$('select[name="type"]').change(function(){
	if($(this).val() == 2)
	{
		$('#deal_promo_blk').show();
		$('#notify_msg_blk').hide();
	}else
	{
		$('#deal_promo_blk').hide();
		$('#notify_msg_blk').show();
	}
}).trigger('change');

function add_deal_callb(name,pid,mrp,price,store_price)
{
	$('#srch_results').html('').hide();
	
	$("#p_srch").val(name).focus();
	$("input[name='sel_itemid']").val(pid);
	$("input[name='sel_itemmrp']").val(mrp);
	$("input[name='sel_itemprice']").val(price);
	
}

$("#p_srch").keyup(function(){
	q=$(this).val();
	if(q.length<3)
		return true;
	if(jHR!=0)
		jHR.abort();
	window.clearTimeout(search_timer);
	search_timer=window.setTimeout(function(){
	jHR=$.post('<?=site_url("admin/pnh_jx_searchdeals")?>',{q:q},function(data){
		$("#srch_results").html(data).show();
	});},200);
});

$('#pnh_sms_camp_frm').submit(function(){
	var error_msg = '';

		if(!$('input[name="fids[]"]:checked').length)
		{
			alert("Please select atleast one franchise");
			return false;
		}
	
		if($('select[name="type"]').val() == 1)
		{
			if($.trim($('textarea[name="msg"]').val()) == '')
			{
				alert("Please enter message for campaign");
				return false;
			}	
		}else
		{
			if(!($('input[name="sel_itemid"]').val()*1))
			{
				alert("Please choose a deal for promotion");
				return false;
			}
		}

		if(!confirm("Are you sure want to proceed ? "))
		{
			return false;
		}
		
});

</script>

<?php
