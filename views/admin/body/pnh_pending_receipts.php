<div class="container">
<h2>Pending Receipts</h2>
<table class="datagrid">
<thead><tr><th>Receipt ID</th><th>Franchise</th><th>Type</th><Th>Amount</Th><th>Instrument Type</th><Th>Instrument Date</Th><th>Instrument No</th><th>Remarks</th><th>Added on</th><th>Created By</th><th></th></tr></thead>
<tbody>
<?php foreach($receipts as $r){?>
<tr>
<td><?=$r['receipt_id']?></td>
<td><a href="<?=site_url("admin/pnh_franchise/{$r['franchise_id']}")?>"><?=$r['franchise_name']?></a></td>
<td><?=$r['receipt_type']==0?"Security Deposit":"Topup"?></td>
<td>Rs <?=$r['receipt_amount']?></td>
<td><?php $modes=array("cash","Cheque","DD","Transfer");?><?=$modes[$r['payment_mode']]?></td>
<td><?=date("d/m/y",$r['instrument_date'])?></td>
<td><?=$r['instrument_no']?></td>
<td><?=$r['remarks']?></td>
<td><?=date("g:ia d/m/y",$r['created_on'])?></td>
<td><?=$r['admin']?></td>
<td>
<a href="javascript:void(0)" onclick='act_rec(<?=$r['receipt_id']?>)'>Activate</a> &nbsp; &nbsp;
<a href="javascript:void(0)" onclick='can_rec(<?=$r['receipt_id']?>)'>Cancel</a> &nbsp; &nbsp;
</td>
</tr>
<?php }?>
</tbody>
</table>
<form id="ac_form" method="post">
<input type="hidden" name="type" class="a_type">
<input type="hidden" name="rid" class="a_rid">
<input type="hidden" name="msg" class="a_reason">
</form>
</div>
<script>
function act_rec(rid)
{
	reason=prompt("Reason/Remark");
	if(!reason || reason.length==0)
		return;
	$(".a_rid").val(rid);
	$(".a_type").val("act");
	$(".a_reason").val(reason);
	$("#ac_form").submit();
}
function can_rec(rid)
{
	reason=prompt("Reason");
	if(!reason || reason.length==0)
		return;
	$(".a_rid").val(rid);
	$(".a_type").val("can");
	$(".a_reason").val(reason);
	$("#ac_form").submit();
}
</script>
<?php
