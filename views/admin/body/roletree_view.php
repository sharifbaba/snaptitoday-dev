<script type="text/javascript" src="<?php echo base_url().'/js/jquery.jOrgChart.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'/js/prettify.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'/js/jquery.qtip-1.0.0-rc3.js'?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url().'/css/jquery.jOrgChart.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'/css/custom.css'?>">
	
<?php 
	function build_subs($obj,$role,$emp_id,$html)
	{
		$sql = "SELECT a.employee_id,a.job_title AS role_id,a.name,c.employee_id AS sub_emp_id,b.parent_emp_id AS sup_emp_id,c.job_title AS sub_role_id,c.name AS sub_emp_name
					FROM m_employee_info a
					LEFT JOIN `m_employee_rolelink` b ON a.employee_id = b.parent_emp_id  AND b.is_active = 1 
					LEFT JOIN m_employee_info c ON c.employee_id = b.employee_id
					WHERE  a.job_title = ? AND a.employee_id = ? ";   
		$res = $obj->db->query($sql,array($role,$emp_id));
		 
		$html = '';
		
		$sub_roles_data = $res->result_array();
		
		if($res->num_rows())
		{
			
			
			if($role > 2)
			{
				$trr_name="SELECT b.territory_name
							FROM m_town_territory_link a
							JOIN pnh_m_territory_info b ON b.id=a.territory_id
							WHERE employee_id=? AND a.is_active=1
							GROUP BY territory_id";
             	$trr_name_res = $obj->db->query($trr_name,array($sub_roles_data[0]['employee_id']));
             	
             
				 $twn_name="SELECT b.town_name
						FROM m_town_territory_link a
						JOIN pnh_towns b ON b.id=a.town_id
						WHERE employee_id=? AND a.is_active=1
						GROUP BY town_id";
             	$twn_name_res = $obj->db->query($twn_name,array($sub_roles_data[0]['employee_id']));
              
             
             
			}else{
				$trr_name_res=$obj->db->query("select territory_name from pnh_m_territory_info");
             	$twn_name_res=$obj->db->query("select town_name from pnh_towns");
			}
			
			$total_terr = $trr_name_res->num_rows();
			$total_twn = $twn_name_res->num_rows();
			
			$s=$t='';
			if(!$trr_name_res->num_rows())
				return ;
			$d = $trr_name_res->num_rows();
			foreach($trr_name_res->result_array() as $t1){
				$s.= $t1['territory_name'];
				
				$d--;
				if($d) $s.= ' , ';
				
			}
			
			$d = $twn_name_res->num_rows();
			/* 	$twn_nms = $twn_names; */
			foreach($twn_name_res->result_array() as $twn){
				$t.=$twn['town_name'];
				$d--;
				if($d) $t.= ' , ';
			}

			 $link= site_url("admin/view_employee/".$sub_roles_data[0]['employee_id']);
             $name = $sub_roles_data[0]['name'];
             $title  = "  $total_terr Territories :$s ";
             
             if($role > 4)
             {
             	$title  .= "</br> $total_twn Towns : $t ";
             	$total_twn = "($total_twn)";
             }else
             {
             	$total_twn = '';
             }
                       
			 
             $html = '<li class="tooltip role_swatch role_swatch_'.$role.'">';  
             if($role >= 4){
                	$html .= "<a class=\"tootip\" title=\"$title\" href='$link'>$name [$s] $total_twn  </a>";
                 }else
			 $html .= "<a class=\"tootip\" title=\"$title\" href='$link'>$name ($total_terr) $total_twn  </a>";
			 $html .= '<ul>';
				foreach($sub_roles_data as $subrole){
					$html .= build_subs($obj,$subrole['sub_role_id'],$subrole['sub_emp_id'],$html)."\r\n";
				}
			$html .= '</ul>';
			$html .= '</li>'."\r\n";
		}	
			
		
		return $html; 	
	}	
?>

<div id="container">
	<div id="main_column" class="clear">
		<div class="tools-container" >
				<div style="float: right;font-size: 12px;padding:5px;">
					<span style="background: #90CA77; height: 15px; width: 15px; display: inline-block;">&nbsp;</span> - SuperAdmin  
					<span style="background: #81C6DD; height: 15px; width: 15px; display: inline-block;">&nbsp;</span> - Business Head  
					<span style="background: #E9B64D; height: 15px; width: 15px; display: inline-block;">&nbsp;</span> - Manager  
				    <span style="background: #FF9900; height: 15px; width: 15px; display: inline-block;">&nbsp;</span> - Territory Manager 
				    <span style="background: #E48743; height: 15px; width: 15px; display: inline-block;">&nbsp;</span> - Bussiness Executive 
				</div>
				<h1 class="mainbox-title">Employees Role Tree</h1>
		</div>
		<div>
			
			<ul id="org" style="display:none">
	        	<?php echo build_subs($this,1,1,'');?>
			</ul>
			<div id="chart" class="orgChart"></div>
		</div>
	</div>
</div>

<style>
.role_swatch{
	/* border:4px solid #f8f8f8 !important; */
	border:4px solid #AAAAAA !important;
	padding:5px;
	color: #454545 !important;
	text-transform: capitalize;
	font-size: 11px !important;
	font-weight: bold;
}
.role_swatch a{
	display: inline-block;
}
.role_swatch_1{
	background: #90CA77 !important;
}
.role_swatch_2{
	background: #81C6DD !important;
}
.role_swatch_3{
	background: #E9B64D !important;
}
.role_swatch_4{
	background: #FF9900  !important;
}
.role_swatch_5{
	background:#E48743   !important;
}
.tools-container span{margin-left: 10px;}
</style>

<script>

$("#org").jOrgChart({
    chartElement : '#chart',
    dragAndDrop : false
});


$(document).ready(function() {
    $('#container a[href][title]').qtip({
        content: {
            text: true // Use each elements title attribute
         },
         style: 'cream' // Give it some style
      });
});


</script>
