<?php 

$finance_role_const_val=$this->db->query("select value from user_access_roles where const_name='FINANCE_ROLE'")->row()->value;

if(!isset($partial_list))
	$partial_list=false;
$trans=array();
foreach($orders as $o)
	$trans[]=$o['transid'];
$braw=$this->db->query("select b.batch_id,i.invoice_no,i.transid from king_invoice i join shipment_batch_process_invoice_link b on b.invoice_no=i.invoice_no where i.transid in ('".implode($trans,"','")."') group by i.invoice_no")->result_array();
foreach($braw as $b)
{
	if(!isset($batches[$b['transid']]))
		$batches[$b['transid']]=array();
	if(!isset($invoices[$b['transid']]))
		$invoices[$b['transid']]=array();
	$batches[$b['transid']][]=$b['batch_id'];
	$invoices[$b['transid']][]=$b['invoice_no'];
}
$pending_flag=false;
if($this->uri->segment(3)=="1")
	$pending_flag=true;
?>
<div class="container">
<div>

<div class="dash_bar">
<a href="<?=site_url("admin/orders/1")?>"></a>
<span><?php $pending=$this->db->query("select count(distinct(transid)) as l from king_orders where status=0")->row()->l;?><?=$pending?></span>
Pending Orders
</div>

<?php /*?>
<div class="dash_bar">
<span><?=$this->db->query("select count(distinct(transid)) as l from king_orders where status=3")->row()->l?></span>
Shipped Orders
</div>
*/ ?>

<div class="dash_bar">
<a href="<?=site_url("admin/orders")?>"></a>
<span><?=$this->db->query("select count(distinct(transid)) as l from king_orders where time>?",mktime(0,0,0,date("n"),1))->row()->l?></span>
Orders this month
</div>

<?php 
	
	if($this->erpm->auth(FINANCE_ROLE,true))
	{
?>
<div class="dash_bar">
<span><?=$this->db->query("select count(distinct(transid)) as l from king_orders where time between ? and ?",array(mktime(0,0,0,date("n")-1,1),mktime(0,0,0,date("n"),date("t"))))->row()->l?></span>
Orders prev month
</div>

<div class="dash_bar">
<span>Rs <?=number_format($this->db->query("select sum(i_price*quantity) as l from king_orders where time>?",mktime(0,0,0,date("n"),1))->row()->l)?></span>
Value this month
</div>

<div class="dash_bar">
<span>Rs <?=number_format($this->db->query("select sum(i_price*quantity) as l from king_orders where time between ? and ?",array(mktime(0,0,0,date("n")-1,1),mktime(0,0,0,date("n"),1)))->row()->l)?></span>
Value prev month
</div>
<?php } ?>
<div class="clear"></div>

<div class="dash_bar">
showing <b><?=count($orders)?></b> orders
</div>
<?php if(!$partial_list){?>
<div class="dash_bar" style="padding:7px;">
Date range: <input type="texT" size="8" class="inp" id="ds_range" value="<?=$this->uri->segment(4)?>"> to <input size="8" type="text" class="inp"id="de_range" value="<?=$this->uri->segment(5)?>"> <input type="button" value="Show" onclick='showrange()'>
</div>
<?php }?>
<div class="dash_bar_red">
<a href="<?=site_url("admin/partial_shipment")?>"></a>
Partial Shipment Orders
</div>

<div class="dash_bar">
<a href="<?=site_url("admin/disabled_but_possible_shipment")?>"></a>
Disabled but possible
</div>

<div class="clear"></div>

</div>

<?php if(!$partial_list){?>
	Stock Unavailability Report for : 
	<a target="_blank" href="<?=site_url("admin/stock_unavail_report/".($this->uri->segment(2)=="partial_shipment"?"1":"0")."/".($this->uri->segment(4)?$this->uri->segment(4):0)."/".($this->uri->segment(5)?$this->uri->segment(5):0)."/0")?>">All Orders</a> / 
	<a target="_blank" href="<?=site_url("admin/stock_unavail_report/".($this->uri->segment(2)=="partial_shipment"?"1":"0")."/".($this->uri->segment(4)?$this->uri->segment(4):0)."/".($this->uri->segment(5)?$this->uri->segment(5):0)."/2")?>">Snapittoday Orders</a> / 
	<a target="_blank" href="<?=site_url("admin/stock_unavail_report/".($this->uri->segment(2)=="partial_shipment"?"1":"0")."/".($this->uri->segment(4)?$this->uri->segment(4):0)."/".($this->uri->segment(5)?$this->uri->segment(5):0)."/1")?>">PNH Orders</a>
<?php }?>



<div style="clear: both;">
	<input type="button" style="float: right; " value="Today Orders Summary" onclick="$('#ord_summ_dlg').dialog('open')">
	<h2 ><?=!isset($pagetitle)?"Recent 50 ":""?>Orders <?=isset($pagetitle)?$pagetitle:""?></h2>
</div>


<div style="background:#eee;padding:5px;">
	Show : <label><input type="checkbox" class="pnh_o_c">PNH Orders</label> <label><input type="checkbox" class="n_o_c">Other Orders</label>
</div>


 

<form action="<?=site_url("admin/bulk_endisable_for_batch")?>" method="post">
<table class="datagrid" width="100%">
<thead>
<tr>
<th><input type="checkbox" id="batch_en_disable_all" value="1"></th>
<th>Trans ID</th>
<?php if($partial_list){?>
<th>Pending</th>
<th>Available</th>
<?php }?>
<th>Deal/Product Details</th>
<th width="120">Ship To</th>
<th width="150">Ordered on</th>
<th>Status</th>
<th>Contact</th>

<?php if(!$partial_list){?>
<th>Invoices</th>
<th><nobr>Process Batches</nobr></th>
<?php }?>
<th style="padding:3px;" width="10"><span style="font-size:68%">Batch Enabled</span></th>
</tr>
</thead>
<tbody>
<?php foreach($orders as $o){?>
<tr <?=$o['priority']?"style='background:#ff8;'":""?> class="<?=$o['is_pnh']?"pnh_o":"n_o"?>">
<td><input type="checkbox" class="batch_en_disable" name="trans[]" value="<?=$o['transid']?>"></td>
<td>
<?php if($o['priority']){?>
<span class="order_high_priority"></span>
<?php }?>
<a href="<?=site_url("admin/trans/{$o['transid']}")?>" class="link"><?=$o['transid']?></a>
<br />
<a href="<?=site_url("admin/user/{$o['userid']}")?>"><?=$o['name']?></a>
</td>


<?php if($partial_list){?>
<td><?=$o['pending']?></td>
<td><?=$o['possible']?></td>
<?php }?>

<td style="padding:0px;">
		<table class="subdatagrid" cellpadding="0" cellspacing="0">
			<thead>
				<th>Slno</th>
				<th>OID</th>
				<th width="200">ITEM</th>
				<th>QTY</th>
				<th>MRP</th>
				<th>Amount</th>
			</thead>
			<tbody>
				<?php 
					$o_item_list = $this->db->query("select a.status,a.id,a.itemid,b.name,a.quantity,i_orgprice,i_price,i_discount,i_coup_discount from king_orders a
														join king_dealitems b on a.itemid = b.id 
														where a.transid = ? 
													",$o['transid'])->result_array();
					$oi = 0;
					foreach($o_item_list as $o_item)
					{
						$is_cancelled = ($o_item['status']==3)?1:0
				?>
					<tr class="<?php echo ($is_cancelled)?'cancelled_ord':''?>">
						<td width="20"><?php echo ++$oi; ?></td>
						<td width="40"><?php echo $o_item['id'] ?></td>
						<td><?php echo anchor('admin/pnh_deal/'.$o_item['itemid'],$o_item['name']) ?></td>
						<td width="20"><?php echo $o_item['quantity'] ?></td>
						<td width="40"><?php echo $o_item['i_orgprice'] ?></td>
						<td width="40"><?php echo round($o_item['i_orgprice']-($o_item['i_coup_discount']+$o_item['i_discount']),2) ?></td>
					</tr>	
				<?php 		
					}
				?>
			</tbody>
		</table>
	</td>
	
<td><?=ucfirst($o['ship_city'])?></td>
<td><?=date("g:ia d M y",$o['init'])?></td>
<td><?php switch($o['status']){
case 0: echo "Pending"; break;
case 1: 
	if(isset($invoices[$o['transid']]) && $this->db->query("select 1 from shipment_batch_process_invoice_link where packed=1 and invoice_no in ('".implode("','",$invoices[$o['transid']])."')")->num_rows()==0) echo "Invoiced"; else echo "Packed"; break;
case 2: echo "Shipped"; break;
case 3: echo "Canceled"; break;
}?>
</td>
<td><?=$o['ship_phone']?></td>


<?php if(!$partial_list){?>
<td>
<?php 
if(!isset($invoices[$o['transid']]))
	echo '-';
else {
	foreach($invoices[$o['transid']] as $b){?>
<a href="<?=site_url("admin/invoice/{$b}")?>"><?=$b?></a>
<?php }		
}
?>
</td>
<td><?php 
if(!isset($batches[$o['transid']]))
	echo '-';
else {
	foreach($batches[$o['transid']] as $b){?>
<a href="<?=site_url("admin/batch/{$b}")?>">BATCH<?=$b?></a>
<?php }		
}
?>
</td>
<?php }?>
<td align="Center" style="font-size:75%"><?=$o['batch_enabled']?"<span class='green'>YES</span>":"<span class='red'>NO</span>"?></td>
</tr>
<?php } if(empty($orders)){?>
<tr><td colspan="100%">no orders to show</td></tr>
<?php }?>
</tbody>
</table>
<span style="padding:5px 10px 10px 10px;background:#eee;">Batch Process Flag : <input type="submit" name="enable" value="Enable selected"> <input type="submit" name="disable" value="Disable selected"></span>
</form>
</div>


<script>
function do_show_orders()
{
	if($(".n_o_c").attr("checked"))
		$(".n_o").show();
	else
		$(".n_o").hide();
	if($(".pnh_o_c").attr("checked"))
		$(".pnh_o").show();
	else
		$(".pnh_o").hide();
}
$(function(){
	$(".n_o_c,.pnh_o_c").change(function(){
		do_show_orders();
	}).attr("checked",true);
	$("#ds_range,#de_range").datepicker();
	$("#batch_en_disable_all").click(function(){
		if($(this).attr("checked"))
			$(".batch_en_disable").attr("checked",true);
		else
			$(".batch_en_disable").attr("checked",false);
	});
});
function showrange()
{
	if($("#ds_range").val().length==0 ||$("#ds_range").val().length==0)
	{
		alert("Pls enter date range");
		return;
	}
	location='<?=site_url("admin/orders/".(!$this->uri->segment(3)?"0":$this->uri->segment(3)))?>/'+$("#ds_range").val()+"/"+$("#de_range").val(); 
}
</script>

<div id="ord_summ_dlg" style="margin:2px !important;padding:3px !important;font-family: arial;"  title="Orders Summary">
<?php 
		$st = strtotime(date('Y-m-d').' 00:00:00');
		$en = strtotime(date('Y-m-d').' 23:59:59'); 
		$ttl_ord_summ = $this->db->query("select count(*) as total,is_pnh,partner_id,c.name as partner_name  
													from king_transactions a 
													join king_orders b on a.transid = b.transid 
													left join partner_info c on c.id = a.partner_id 
													where 1  
													group by is_pnh,partner_id  
													order by total,is_pnh,partner_id,partner_name ",array($st,$en))->result_array();
		$ord_sum_sites = array('snapittoday'=>0,'paynearhome'=>0); 
		foreach($ttl_ord_summ as $ttlords)
		{
			if($ttlords['partner_id'])
			{
				$ord_sum_sites[$ttlords['partner_name']] = $ttlords['total'];
			}else if(!$ttlords['partner_id'] && !$ttlords['is_pnh'])
			{
				$ord_sum_sites['snapittoday'] = $ttlords['total'];
			}
			else if(!$ttlords['partner_id'] && $ttlords['is_pnh'])
			{
				$ord_sum_sites['paynearhome'] = $ttlords['total'];
			}
		}
	?>
<div style="background: #f9f9f9;overflow: hidden;text-align: right;border;1px solid #cfcfcf">
	<div style="background: #FFF;color:#555;padding:7px;border-left: 3px solid yellow;text-align: center;" >
		<b style="font-size: 15px;">Overall</b>
	</div>
	<div style="clear: both;">
	<?php 
		$colors = array('magenta','#cd0000','#dfdfdf','#f7f7f7','#aaa','#fffff0','#cc11df','maroon','grey','lightblue');
		$c = 0;
		
		arsort($ord_sum_sites);
		
		foreach($ord_sum_sites as $oby=>$ttl_ord)
		{
	?>
			<div style="float: left;background:<?php echo $colors[$c]?>;color:#000;padding:7px 10px;width: 80px;text-align: center;" >
				<?php echo ucwords($oby)?> <br /><b style="font-size: 16px;"><?php echo $ttl_ord; ?></b>
			</div> 
	<?php
			$c++; 		
		}
	?>
	</div>
</div>

<?php 
	$st = strtotime(date('Y-m-d').' 00:00:00');
		$en = strtotime(date('Y-m-d').' 23:59:59'); 
		$ttl_ord_summ = $this->db->query("select count(*) as total,is_pnh,partner_id,c.name as partner_name  
													from king_transactions a 
													join king_orders b on a.transid = b.transid 
													left join partner_info c on c.id = a.partner_id 
													where a.init between ? and ?   
													group by is_pnh,partner_id  
													order by total,is_pnh,partner_id,partner_name ",array($st,$en))->result_array();
		$ord_sum_sites = array('snapittoday'=>0,'paynearhome'=>0); 
		foreach($ttl_ord_summ as $ttlords)
		{
			if($ttlords['partner_id'])
			{
				$ord_sum_sites[$ttlords['partner_name']] = $ttlords['total'];
			}else if(!$ttlords['partner_id'] && !$ttlords['is_pnh'])
			{
				$ord_sum_sites['snapittoday'] = $ttlords['total'];
			}
			else if(!$ttlords['partner_id'] && $ttlords['is_pnh'])
			{
				$ord_sum_sites['paynearhome'] = $ttlords['total'];
			}
		}
?>

<div style="background: #f9f9f9;overflow: hidden;text-align: right;border;1px solid #cfcfcf">
	<div style="background: #FFF;color:#555;padding:7px;border-left: 3px solid yellow;text-align: center;" >
		<b style="font-size: 15px;">Orders Today</b>
	</div>
	<div style="clear: both;">
	<?php 
		$colors = array('magenta','#cdFFFF','#dfdfdf','#f7f7f7','#aaa','#fffff0','#cc11df','maroon','grey','lightblue');
		$c = 0;
		
		arsort($ord_sum_sites);
		
		foreach($ord_sum_sites as $oby=>$ttl_ord)
		{
	?>
			<div style="float: left;background:<?php echo $colors[$c]?>;color:#000;padding:7px 10px;width: 80px;text-align: center;" >
				<?php echo ucwords($oby)?> <br /><b style="font-size: 16px;"><?php echo $ttl_ord; ?></b>
			</div> 
	<?php
			$c++; 		
		}
	?>
	</div>
</div>


	
	
</div>

<script type="text/javascript">
	$(function(){
		$('#ord_summ_dlg').dialog({width:'700',height:'500',resizable:false,autoOpen:false,modal:true,open:function(){ $(event.target).dialog('widget')
            .css({ position: 'fixed' })
            .position({ my: 'center', at: 'center', of: window }); }});
	});
</script>


<style>
	.subdatagrid{width: 100%}
	.subdatagrid th{padding:5px;font-size: 11px;background: #F4EB9A;color: maroon}
	.subdatagrid td{padding:3px;font-size: 12px;}
	.subdatagrid td a{color: #121213;}
	.cancelled_ord td{text-decoration: line-through;color: #cd0000 !important;}
	.cancelled_ord td a{text-decoration: line-through;color: #cd0000 !important;}
</style>

<?php
