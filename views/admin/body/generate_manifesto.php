<div class="container">
<h2>Generate Manifesto</h2>

<form method="post" action="<?php echo site_url('admin/generate_manifesto_byrange');?>"> 
	<table id="frm" class="datagrid">
		<tr>
			<td>
				<b>Start Date</b> :
				<div><input type="text" id="from_date" name="from" value="<?php echo $from?>" /></div>
			</td>
			<td>
				<b>End Date</b> :
				<div><input type="text" id="to_date" name="to" value="<?php echo $to?>" /></div>
			</td>
			<td>
				<b>Territory</b> :
				<div>
					<select name="sel_terr_ids[]" data-placeholder="Choose" multiple="multiple" class="chz-select" style="width: 200px;">
							<?php 
								if(!$sel_terr_ids)
									$sel_terr_ids = array();
								$tr_list = $this->db->query("select * from pnh_m_territory_info order by territory_name ")->result_array();
								foreach($tr_list as $tr_det)
								{
									$sel = in_array($tr_det['id'],$sel_terr_ids)?'selected':'';
							?>
									<option <?php echo $sel;?> value="<?php echo $tr_det['id']?>" ><?php echo ucwords($tr_det['territory_name'])?></option>
							<?php 		
								}
							?>		
					</select>
				</div>
			</td>
			<td>
				&nbsp;
				<div><input type="submit" value="Generate" style="float:right" /></div>
			</td>
		</tr>
	</table>
</form>

<script type="text/javascript">
$('select[name="sel_terr_ids[]"]').chosen();
</script>

<br />
<?php if($_POST){?>
<div style="margin-top: 10px;">
<?php 	
	$manifesto_list = array();
	if($outscan_res->num_rows())
	{
		foreach($outscan_res->result_array() as $row)
		{	
			if(!isset($manifesto_list[$row['franchise_id']]))
				$manifesto_list[$row['franchise_id']] = array();
			array_push($manifesto_list[$row['franchise_id']],$row);
		}
	
?>			
	<div align="right" style="width: 90%;clear: both;">
		<span style="text-align: left">
			<b>Filter by </b>
			 
			<b>Territory</b>
			<select class="chzn-select" id="sel_terrlist"></select>
			&nbsp;
			<b>Town</b>
			<select class="chzn-select" id="sel_townlist"></select>
			&nbsp;
			&nbsp;
			<input type="button" value="Print Manifesto" id="print_manifesto" />	
		</span>
	</div>
		
	<table id="manifesto_list" class="datagrid" style="border-collapse: collapse;width: 90%;clear: both;">
		<thead>
			<th width="20"><b>Slno</b></th>
			<th width="60"><b>Invoiceno</b> 
				<input type="checkbox" value="1" checked="checked" name="chk_print_visible">
			</th>
			<th width="300"><b>Name</b></th>
			<th width="130"><b>City/Town</b></th>
			<th width="60"><b>Pincode</b></th>
			<th width="60"><b>Mobile</b></th>
			<th width="200"><b>Seal & Signature </b></th>
		</thead>
		<tbody>
		<?php 
			 	$slno = 1;
				foreach($manifesto_list as $fr_id=>$fr_manifesto)
				{	
					$row_span = count($fr_manifesto);
					$k = 0 ;
					foreach($fr_manifesto as $fr_man_inv)
					{
						$fr_man_inv['territory_name'] = trim($fr_man_inv['territory_name']);
						$fr_man_inv['town_name'] = trim($fr_man_inv['town_name']);
		?>
						 
					<tr class="fr_<?php echo $fr_man_inv['franchise_id'] ?> twn_<?php echo str_replace(' ','_',$fr_man_inv['town_name']);?> terr_<?php echo str_replace(' ','_',$fr_man_inv['territory_name']);?>">
						<td align="center"><?php echo $slno++?></td>
						<td><?php echo $fr_man_inv['invoice_no']?>
							<input type="checkbox" checked="checked" value="<?php echo $fr_man_inv['invoice_no'];?>" class="chk_print" >
						</td>
						<?php 
							if($k == 0) {
						?>
								<td class="town_name terr_name" terr_name="<?php echo str_replace(' ','_',$fr_man_inv['territory_name']); ?>" twn_name="<?php echo str_replace(' ','_',$fr_man_inv['town_name']); ?>" rowspan="<?php echo $row_span?>" style="vertical-align: middle;text-align: center;"><?php echo $fr_manifesto[0]['franchise_name'];?>
									<input type="checkbox" checked="checked" value="<?php echo $fr_man_inv['franchise_id'] ?>" class="chk_print_fr" >
								</td>
								<td rowspan="<?php echo $row_span?>" style="vertical-align: middle;text-align: center;"><?php echo $fr_manifesto[0]['town_name'];?></td>
								<td rowspan="<?php echo $row_span?>" style="vertical-align: middle;text-align: center;"><?php echo $fr_manifesto[0]['postcode'];?></td>
								<td rowspan="<?php echo $row_span?>" style="vertical-align: middle;text-align: center;"><?php echo $fr_manifesto[0]['login_mobile1'];?></td>
								<td rowspan="<?php echo $row_span?>" ><div style="height: 60px;width: 200px;background: #fdfdfd;"></div></td>
						<?php 		
								$k = 1;
							}
						?>
					</tr>
		<?php 
					}
				}
			 
		?>
		</tbody>
	</table>
	<?php }else{
		echo "<b>No Outscans found for the selected dates </b>";
	}?>
</div>
<?php }else{
?>		
	<div id="manifesto_log">	
		<h3>Manifesto Log</h3>
		<?php 
			if($manifesto_hist_res->num_rows)
			{
		?>
		<table class="datagrid" cellpadding="0" cellspacing="0">
			<thead>
				<th>Slno</th>
				<th>Name</th>
				<th>Invoice nos</th>
				<th>Prints</th>
				<th>Createdon</th>
				<th>LastPrintedon</th>
			</thead>
			<tbody>
			<?php 
				if($manifesto_hist_res->num_rows)
				{
					$slno = $pg+1;
					foreach($manifesto_hist_res->result_array() as $manifesto_hist)
					{
						echo '<tr>';
						echo '	<td>'.$slno.'</td>';
						echo '	<td>'.$manifesto_hist['name'].'</td>';
						echo '	<td><a class="preview_inv">'.implode('</a> <a class="preview_inv">',explode(',',$manifesto_hist['invoice_nos'])).'</a></td>';
						echo '	<td>'.$manifesto_hist['total_prints'].'</td>';
						echo '	<td>'.format_datetime($manifesto_hist['created_on']).'</td>';
						echo '	<td>'.format_datetime($manifesto_hist['modified_on']).'</td>';
						echo '	<td><input type="button" value="Print" onclick="print_manifesto('.$manifesto_hist['id'].')"></td>';
						echo '</tr>';
						$slno++;
					}
				}
			?>
			</tbody>
		</table>
		<div align="left" class="pagination">
			<?php echo $manifesto_hist_pagi?>
		</div>
		<?php }else{
			echo "No history found";
		}?>
	</div>
<?php 	  	 
} ?>
	
</div>
<div style="display: none;">
<form id="gen_manifestoprint" target="hndl_ganmanifestoprint" action="<?php echo site_url('admin/gen_manifestoprint')?>" method="post">
	<input type="hidden" name="from_d" value="<?php echo $this->input->post('from')?>">
	<input type="hidden" name="to_d" value="<?php echo $this->input->post('to')?>">
	<input type="hidden" name="id" value="0">
	<input type="hidden" name="sel_terr_ids" value="<?php echo implode(',',$sel_terr_ids);?>">
	<textarea name="exclude_invs"></textarea>
</form>
<iframe id="hndl_ganmanifestoprint" name="hndl_ganmanifestoprint" style="width: 1px;height: 1px;border:0px;"></iframe>
</div>
<style>
.hideinprint td{background: #cdcdcd !important;}
.pagination{padding:4px;}
.pagination a{display: inline-block;padding:3px;color: #cd0000}
.preview_inv{cursor: pointer;}
</style>
<script>
$('.preview_inv').click(function(){
	window.open(site_url+'/admin/invoice/'+$(this).text());
});
function print_manifesto(id){
	$('#gen_manifestoprint input[name="id"]').val(id);
	$('#gen_manifestoprint').submit();
}

var terrList = new Array();
	$('.terr_name').each(function(){
		if($.inArray($(this).attr('terr_name'),terrList)==-1)
		{
			terrList.push($(this).attr('terr_name'));
		}
	});

var townList = new Array();
	$('.town_name').each(function(){
		if($.inArray($(this).attr('twn_name'),townList)==-1)
		{
			townList.push($(this).attr('twn_name'));
		}
	});

	$(function(){
		$('.dg_print').hide();
	});

	
	 
	//townList = sort(townList);
	var twn_html = '';
	for(var k=0;k<townList.length;k++){
		twn_html += '<option rel="'+townList[k]+'" value="'+townList[k]+'">'+townList[k]+'</option>';
	}

	$('#sel_townlist').html(twn_html);

	$('#sel_townlist').change(function(){
		$('#sel_terrlist').val("").trigger("liszt:updated");
		var twn_name = $(this).val();
		$('#manifesto_list tbody tr').hide();
		if($(this).val())
		{
			$('#manifesto_list tbody tr.twn_'+twn_name).show();
		}else
		{
			$('#manifesto_list tbody tr').show();
		}
	});

	$('#sel_townlist').html($("#sel_townlist option").sort(function(a, b) { 
	    var arel = $(a).attr('rel');
	    var brel = $(b).attr('rel');
	    return arel == brel ? 0 : arel < brel ? -1 : 1 
	}));

	$('#sel_townlist').prepend('<option value="">Show All</option>');


	$('#sel_townlist').chosen({no_results_text: "No results matched"});



	var terr_html = '';
	for(var k=0;k<terrList.length;k++){
		terr_html += '<option rel="'+terrList[k]+'" value="'+terrList[k]+'">'+terrList[k]+'</option>';
	}

	$('#sel_terrlist').html(terr_html);

	$('#sel_terrlist').change(function(){

		$('#sel_townlist').val("").trigger("liszt:updated");
		
		var terr_name = $(this).val();
		$('#manifesto_list tbody tr').hide();
		if($(this).val())
		{
			$('#manifesto_list tbody tr.terr_'+terr_name).show();
		}else
		{
			$('#manifesto_list tbody tr').show();
		}
	});

	$('#sel_terrlist').html($("#sel_terrlist option").sort(function(a, b) { 
	    var arel = $(a).attr('rel');
	    var brel = $(b).attr('rel');
	    return arel == brel ? 0 : arel < brel ? -1 : 1 
	}));

	$('#sel_terrlist').prepend('<option value="">Show All</option>');


	$('#sel_terrlist').chosen({no_results_text: "No results matched"});


	
	

$('textarea[name="exclude_invs"]').val('');
$("#print_manifesto").click(function(){
	$ele = $('#manifesto_list');
	var exclude_invnos = new Array();
		exclude_invnos.push(0);
	$('tbody tr',$ele).each(function(){
		if($(this).hasClass('hideinprint'))
		{
			exclude_invnos.push($('.chk_print',this).val());
		}
	});

	$('textarea[name="exclude_invs"]').val(exclude_invnos.join(','));
	$('#gen_manifestoprint').submit();
});

prepare_daterange('from_date','to_date');
$('.chk_print').change(function(){
	if($(this).attr('checked'))
	{
		$(this).parent().parent().removeClass('hideinprint');
	}	
	else
	{
		$(this).parent().parent().addClass('hideinprint');
	}
	
	setTimeout(function(){
		var i=0;
		$('#manifesto_list tbody tr').each(function(){
			$('td:first',this).text('');
			if(!$(this).hasClass('hideinprint') && !$(this).hasClass('nomark'))
			{
				i++;
				$('td:first',this).text(i);
				
			}
		});
	},200)
});
$('.chk_print_fr').change(function(){
	var fid = $(this).val();
	var invchks = $('.fr_'+fid+' .chk_print');
	if($(this).attr('checked'))
	{
		invchks.attr('checked',true);
	}else
	{
		invchks.attr('checked',false);
	}
	invchks.trigger('change');
		
});

$('input[name="chk_print_visible"]').change(function(){
	if($(this).attr('checked'))
		$('.chk_print:visible').attr('checked',true);
	else
		$('.chk_print:visible').attr('checked',false);

	$('.chk_print:visible').trigger('change');

	
});

$(function(){
	$('#frm .dg_print').hide().remove();	
});

</script>
<?php
