<div class="container">
<h2><?=isset($pagetitle)?$pagetitle:"Franchises"?></h2>
<div>

<div class="dash_bar">
<a href="<?=site_url("admin/pnh_franchises")?>"></a>
<span><?=count($frans)?></span>
Franchises
</div>

<div class="dash_bar_right">
Generate Print by Territory : <select id="sel_p_terry">
<option value="0">select</option><?php foreach($this->db->query("select id,territory_name as name from pnh_m_territory_info order by name asc")->result_array() as $t){?>
<option value="<?=$t['id']?>"><?=$t['name']?></option>
<?php }?>
</select>
</div>

<div class="dash_bar">
View by Territory : <select id="sel_terry">
<option value="0">select</option><?php foreach($this->db->query("select id,territory_name as name from pnh_m_territory_info order by name asc")->result_array() as $t){?>
<option value="<?=$t['id']?>"><?=$t['name']?></option>
<?php }?>
</select>
</div>

<div class="dash_bar">
View by Town : <select id="sel_town">
<option value="0">select</option><?php foreach($this->db->query("select id,town_name as name from pnh_towns order by name asc")->result_array() as $t){?>
<option value="<?=$t['id']?>"><?=$t['name']?></option>
<?php }?>
</select>
</div>
<div class="clear"></div>
</div>

<a href="<?=site_url("admin/pnh_addfranchise")?>">Add Franchise</a>

<table class="datagrid datagridsort" width="100%" style="margin-top:10px;">
<thead><tr><th>Sno</th><th>Franchise Name</th><th>FID</th><th>Type</th><th>City</th><th>Territory</th><th>Current Balance</th><th>Assigned to</th><th>Class</th><th></th></tr></thead>
<tbody>
<?php $i=0; foreach($frans as $f){?>
<tr>
<td>
<?=++$i?></td>
<td>
 <?php if($f['is_suspended']){?><img src="<?=IMAGES_URL?>suspended.png" style="position:absolute;opacity:0.4;margin-top:-2px;margin-left:-6px;"><?php }?>
<a class="link" href="<?=site_url("admin/pnh_franchise/{$f['franchise_id']}")?>"></a><?=$f['franchise_name']?></td>
<td><?=$f['pnh_franchise_id']?></td>
<td><?=$f['is_lc_store']?"LC Store":"Franchise"?></td>
<td><?=$f['city']?></td>
<td><?=$f['territory_name']?></td>
<td><?=$f['current_balance']?></td>
<td><?=$f['owners']?></td>
<td><?=$f['class_name']?></td>
<td>
<a style="white-space:nowrap" href="<?=site_url("admin/pnh_franchise/{$f['franchise_id']}")?>">view</a> &nbsp;&nbsp;&nbsp; 
<a style="white-space:nowrap" href="<?=site_url("admin/pnh_edit_fran/{$f['franchise_id']}")?>">edit</a> &nbsp;&nbsp;&nbsp; 
<a style="white-space:nowrap" href="<?=site_url("admin/pnh_manage_devices/{$f['franchise_id']}")?>">manage devices</a> &nbsp;&nbsp;&nbsp;
<a  style="white-space:nowrap" href="<?=site_url("admin/pnh_assign_exec/{$f['franchise_id']}")?>">assign executives</a>
</td>
</tr>
<?php }?>
</tbody>
</table>

</div>
<style>
table.datagridsort thead tr th, table.datagridsort tfoot tr th {
	border: 1px solid #FFF;
	font-size: 13px; 
}
</style>
<script>
$(function(){
	$("#sel_p_terry").change(function(){
		v=$(this).val();
		if(v==0)
			return;
		window.open("<?=site_url("admin/pnh_print_franchisesbyterritory")?>/"+v);
	}).val("0");
	$("#sel_terry").change(function(){
		v=$(this).val();
		if(v==0)
			return;
		location="<?=site_url("admin/pnh_franchisesbyterritory")?>/"+v;
	}).val("0");
	$("#sel_town").change(function(){
		v=$(this).val();
		if(v==0)
			return;
		location="<?=site_url("admin/pnh_franchisesbytown")?>/"+v;
	}).val("0");
});
$('.datagridsort').tablesorter({sortList:[[0,0]]});
</script>

<?php
