<h3 style="margin:0px;">Order Confirmation</h3>
<table class="datagrid noprint">
<thead><Tr><th>Sno</th><th>Product Name</th><th>MRP</th><th>Offer Price</th><th>Margin (A)</th><th>Scheme discount (B)</th><th>Balance Discount (C)</th><th>Total Discount (A+B+C)</th><th>Unit Price</th><th>Qty</th><th>Order price</th></Tr></thead>
<tbody>
<?php $i=1; foreach($items as $item){?>
<tr>
<td><?=$i++?></td>
<td><?=$item['name']?></td>
<td><?=$item['mrp']?></td>
<td><?=$item['price']?></td>
<td><?=$item['price']/100*$item['base_margin']?> (<?=$item['base_margin']?>%)</td>
<td><?=$item['price']/100*$item['sch_margin']?> (<?=$item['sch_margin']?>%)</td>
<td><?=$item['price']/100*$item['bal_discount']?> (<?=$item['bal_discount']?>%)</td>
<td><?=($item['price']/100*($item['sch_margin']+$item['base_margin']+$item['bal_discount']))?> (<?=$item['base_margin']+$item['sch_margin']+$item['bal_discount']?>%)</td>
<td><?=$item['final_price']?></td>
<td>x<?=$item['qty']?></td>
<td><?=$item['final_price']*$item['qty']?></td>
</tr>
<?php }?>
</tbody>
</table>
<h3 style="margin:0px;">Price changes</h3>
<table class="datagrid noprint">
<thead><tr><th>PNH ID</th><th>Product</th><th>Old MRP</th><th>New MRP</th><th>Old Price</th><th>New Price</th><th>Change</th></tr></thead>
<tbody>
<?php $c=0; foreach($deals as $d){?>
<tr>
<td><?=$d['pnh_id']?></td><td><a href="<?=site_url("admin/pnh_deal/{$d['pnh_id']}")?>" target="_blank"><?=$d['name']?></a></td>
<td><?=$d['old_mrp']?></td>
<td style="background:#f37"><?=$d['new_mrp']?></td>
<td><?=$d['old_price']?></td>
<td style="background:#f37"><?=$d['new_price']?></td>
<td><?=$d['new_price']-$d['old_price']?></td>
</tr>
<?php $c+=$d['new_price']-$d['old_price']; } if(empty($deals)){?>
<tr>
<td colspan="100%">no price changes</td>
</tr>
<?php }else{?>
<tr>
<td colspan="100%"><input type="checkbox" class="price_c_com" value="yes"> Price change communicated to franchise</td>
</tr>
<?php }?>
</tbody>
</table>

<h3 style="margin:0px;">Redeem loyalty points</h3>
<?php $mpointsr=$this->db->query("select points,concat(first_name,' ',last_name) as m_name from pnh_member_info where pnh_member_id=?",$mid)->row_array(); $mpoints=0; if(!empty($mpointsr)) $mpoints=$mpointsr['points'];?>
<table class="datagrid">
<thead>
	<th>MemberID</th>
	<th>Name</th>
	<th>Points</th>
</thead>
<tbody>
	<tr>
		<td><?php echo $mid ?></td>
		<td><b><?=$mpointsr['m_name']?></b></td>
		<td>
			<div style="padding:5px;background: #FFF">
			<b><?=$mpoints?></b> Available 
			<br />
			<?php if($mpoints>=150){?>
					<span id=""><input type="checkbox" id="redeem_cont" name="redeem" value="1"></span>Redeem <input class="redeem_points" type="text" class="inp" size=4 name="redeem_points" value="100"> (max. 100)
				<?php }else echo 'Minimum of 150 points required to redeem';?>
			</div>	
		</td>
	</tr>
	<tr><td colspan=2>Total amount to be collected from member :</td><td><?=$total?></td></tr>		
</tbody>
</table>
  
</table>
<?php
