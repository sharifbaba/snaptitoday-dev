<div class="container">

<div style="overflow: hidden;">
<div style="float:right;background: #ffffd0;overflow: hidden;padding:2px 5px;font-size: 12px;">
	Date range : <input type="text" class="inp fil_style" size=10 id="s_from"> to <input type="text" class="inp fil_style" size=10 id="s_to"> <input type="button" class="fil_style" style="padding:3px 6px;" value="Go" onclick='go_date()'>
	&nbsp;
	&nbsp;
	Brand: <select id="fil_brand" class="fil_style"></select>
	Franchise: <select id="fil_franchise" class="fil_style" style="max-width: 150px;"></select>  
</div>
<h2 style="font-size: 16px;"><?=$pagetitle?></h2>
</div>

<table class="datagrid" style="width: 100%">

<thead>
	<tr>
		<th>Quote</th>
		<th>Franchise<br><span style="font-size:10px;font-weight:normal;">sort</span><a href="<?php echo site_url("admin/pnh_quotes/0/$st_d/$en_d/fid/a");?>"><img src="<?=base_url()?>images/desc.gif"></a><a href="<?php echo site_url("admin/pnh_quotes/0/$st_d/$en_d/fid/d");?>"><img src="<?=base_url()?>images/asc.gif"></a></th>
		<th>Products</th>
		<th>Status</th>
		<th>Created On</th>
		<th>Created By</th>
		<th>Updated On</th>
		<th>Updated By</th>
	</tr>
</thead>

<tbody>

<?php 
	$fr_ids = array();
	$brand_ids = array();

?>

<?php  $colors=array("#AAFFAA","#11EE11","#FFAAAA"); foreach($quotes as $q){
$status=array("Orders placed",'Price authorized','pending');
  
if($this->db->query("select 1 from pnh_quotes_deal_link where order_status=0 and quote_id=?",$q['quote_id'])->num_rows()==0) $s=0; else if($this->db->query("select 1 from pnh_quotes_deal_link where final_price=0 and quote_id=?",$q['quote_id'])->num_rows()==0) $s=1 ; else $s=2; ?>

<?php 
	$fr_ids[$q['franchise_id']]=$q['franchise_name'];
?>

<tr style="background:<?=$colors[$s]?>;" class="quotes <?php echo 'fr_'.$q['franchise_id'];?>">
<td><a href="<?=site_url("admin/pnh_quote/{$q['quote_id']}")?>" class="link"><?=$q['quote_id']?></a></td>
<td><?=$q['franchise_name']?></td>
<td>
<?php foreach($this->db->query("select b.name as brand_name,d.brandid,i.name,i.orgprice as mrp,i.price,q.pnh_id,q.* from pnh_quotes_deal_link q join king_dealitems i on i.pnh_id=q.pnh_id and i.is_pnh=1 join king_deals d on d.dealid = i.dealid join king_brands b on b.id = d.brandid  where q.quote_id=?",$q['quote_id'])->result_array() as $d){
		$brand_ids[$d['brandid']]=$d['brand_name'];
?>
<div class="<?php echo 'br_'.$d['brandid'];?>"><?=$d['name']?> : <b>Rs <?=$d['dp_price']==0?"na":$d['dp_price']?></b></div>
<?php }?>
</td>
<td><?=$status[$s]?></td>
<td><?=date("g:ia d/m/y",$q['created_on'])?></td>
<td><?=$q['created_by']?></td>
<td><?=$q['created_on']==0?"na":date("g:ia d/m/y",$q['created_on'])?></td>
<td><?=$q['updated_by']?></td>
</tr>
<?php }?>
</tbody>

</table>

</div>
<?php 
	asort($fr_ids);
	$fr_list = '<option value="">Choose</option>';
	foreach($fr_ids as $fid=>$fname)
	{
		$fr_list .= '<option value="'.$fid.'">'.$fname.'</option>';
	}
	
	asort($brand_ids);
	$br_list = '<option value="">Choose</option>';
	foreach($brand_ids as $br_id=>$br_name)
	{
		$br_list .= '<option value="'.$br_id.'">'.$br_name.'</option>';
	}
	
?>

<style>
	.fil_style{font-size: 11px;padding:3px !important;}
</style>

<script>


$('#fil_franchise').html('<?php echo $fr_list?>');
$('#fil_franchise').change(function(){
	$('#fil_brand').val('');
	$('.quotes').hide();
	if(!$(this).val())
		$('.quotes').show();
	else
		$('.fr_'+$(this).val()).show();
});

$('#fil_brand').html('<?php echo $br_list?>');
$('#fil_brand').change(function(){
	$('#fil_franchise').val('');
	$('.quotes').hide();
	if(!$(this).val())
		$('.quotes').show();
	else
	{
		$('.br_'+$(this).val()).parent().parent().show();
	}
		
});

function go_date()
{
	from=$("#s_from").val();
	to=$("#s_to").val();
	if(from.length==0 || to.length==0)
	{
		alert("Check date");return;
	}
	location="<?=site_url("admin/pnh_quotes")?>/<?=$this->uri->segment(3)?$this->uri->segment(3):0?>/"+from+"/"+to;
}

$(function(){
	$("#s_from,#s_to").datepicker();
});
</script>

<?php

