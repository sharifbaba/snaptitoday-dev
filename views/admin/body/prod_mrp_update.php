<div class="container">
<h2>Product MRP bulk update</h2>
select brand : <select id="sel_brand">
<option value=0>select</option>
<?php foreach($this->db->query("select id,name from king_brands order by name asc")->result_array() as $b){?>
<option value="<?=$b['id']?>"><?=$b['name']?></option>
<?php }?>
</select>

<div style="padding:20px;">
<form method="post">
<?php if(isset($prods)){?>
<table class="datagrid">
<theaD><tr><th>Product Name</th><th>MRP</th><th>New Mrp</th></tr></theaD>
<tbody>
<?php foreach($prods as $p){?>
<tr>
<td><?=$p['name']?></td>
<td>Rs <?=$p['mrp']?></td>
<td><input type="hidden" name="pid[]" value="<?=$p['product_id']?>"><input type="text" name="mrp[]" size=8 value=""></td>
</tr>
<?php }?>
</tbody>
</table>
<input type="submit" value="Update MRPs">
<?php }?>
</form>
</div>

</div>

<script>

$(function(){
	$("#sel_brand").change(function(){
		location='<?=site_url("admin/prod_mrp_update")?>/'+$(this).val();
	});
});
</script>
<?php
