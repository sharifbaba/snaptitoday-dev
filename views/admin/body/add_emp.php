
<script type="text/javascript">
				$(function () {
					$("#txtDate").datepicker({
					changeMonth: true,
					changeYear: true,
					yearRange: '1970:1995'
					});

					$(".chzn-select").chosen({no_results_text: "No results matched"}); 

				});
	</script>
	
	
<div class="container">
	<div id="main_column" class="clear">
		<div class="cm-notification-container "></div>
		<div class="tools-container">
			<span class="action-add">
			     <a	href="<?php echo site_url('admin/list_employee')?>" class="fl_right">List Employee</a>
			</span>
			<h1 class="mainbox-title">Add & Manage Employees</h1>
	  </div>
	  
	<form id="formID" enctype="multipart/form-data" action="<?php echo site_url('admin/process_addemployee')?>" method="post">
			
         <table cellspacing="5" cellpadding="2">
				<tr>
					<td>Employee Name : <span class="red_star">*</span></td>
					<td><input type="text" name="emp_name"
						value="<?php echo set_value('emp_name')?>"><?php echo form_error('emp_name','<div class="error">','</div>')?>
					</td>
				</tr>
				
				<tr>
					<td>User Name : <span class="red_star">*</span></td>
					<td><input type="text" name="user_name"
						value="<?php echo set_value('user_name')?>"><?php echo form_error('user_name','<div class="error">','</div>')?>
					</td>
				</tr>

				<tr>
					<td>Father Name :</td>
					<td><input type="text" name="father_name" 
						value="<?php echo set_value('father_name')?>">
					</td>
				</tr>

				<tr>
					<td>Mother Name :</td>
					<td><input type="text" name="mother_name" 
						value="<?php echo set_value('mother_name')?>">
					</td>
				</tr>

				<tr>
					<td>D.O.B :</td>
					<td><input type="text" id="txtDate" name="dob" value="<?php echo set_value('dob')?>">
						
					</td>
				</tr>

				<tr>
					<td>Gender : <span class="red_star">*</span></td>
					<td><select name="gender" >
							<option value="">Choose</option>
							<option value="Male" <?php echo set_select('gender','Male')?> >Male</option>
							<option value="Female" <?php echo set_select('gender','Female')?> >Female</option>
							<?php echo set_select('gender')?>
					</select><?php echo form_error('gender','<div class="error">','</div>')?>
					</td>
				</tr>

				<tr>
					<td>Qualification :</td>
					<td><input type="text" name="edu" 
						value="<?php echo set_value('edu')?>">
					</td>
				</tr>

				<tr>
					<td>Email Id : <span class="red_star">*</span></td>
					<td><input type="email" name="email_id" 
						value="<?php echo set_value('email_id')?>"><?php echo form_error('email_id','<div class="error">','</div>')?>
					</td>
				</tr>

				<tr>
					<td>Address : <span class="red_star">*</span></td>
					<td><textarea rows="1" style="height: 60px; width: 140px;"
							cols="25" name="address" ><?php echo set_value('address')?></textarea><?php echo form_error('address','<div class="error">','</div>')?>
					</td>
				</tr>

				<tr>
					<td>City : <span class="red_star">*</span></td>
					<td><select name="city" >
							<option value="">Choose</option>
							<option value="Bangalore" <?php echo set_select('city','Bangalore')?> >Bangalore</option>
							<option value="Tumkur" <?php echo set_select('city','Tumkur')?> >Tumkur</option>
							<option value="Mandya" <?php echo set_select('city','Mandya')?> >Mandya</option>
							<option value="Hassan" <?php echo set_select('city','Hassan')?> >Hassan</option>
							<option value="Chickmangalur" <?php echo set_select('city','Chickmangalur')?> >Chickmangalur</option>
							<option value="Madekeri" <?php echo set_select('city','Madekeri')?> >Madekeri</option>
							
					</select><?php echo form_error('city','<div class="error">','</div>')?>
					</td>
				</tr>

				<tr>
					<td>PostCode :<span class="red_star">*</span></td>
					<td><input type="number" name="postcode" value="<?php echo set_value('postcode')?>">
						<?php echo form_error('postcode','<div class="error">','</div>')?>
					</td>
				</tr>

			<tr>
				<td>Contact no : <span class="red_star">*</span></td>
				<td>
					<ol id="contactList">
						<li><input type="text" name="contact_no[]" class="clearContent" value="<?php echo set_value('contact_no')?>" maxlength="10"></li>
					</ol>
				</td>
			</tr>
			<tr>                                                                      
					<td>Job Title : <span class="red_star">*</span></td>
					<td><select name="role_id" class="inst_type">
							<option value="">Choose</option>
							<?php if($access_roles){
								foreach ($access_roles as $roles){
							?>
							<option <?php echo set_select('role_id',$roles['role_id']);?>
								value="<?php echo ($roles['role_id']);?>">
								<?php echo $roles['role_name'];?>
							</option>
							<?php }
							}?>
					</select> <?php echo form_error('role_id','<div class="error">','</div>')?>
					</td>
				</tr>
				<tr>
					<td>Assigned under :<span class="red_star">*</span></td>
					<td><select name="assigned_under_id"></select>
				
				</tr>

				   <tr class="inst territory">
					<td class="label">Territory : <span class="red_star">*</span></td>
					<td><select class="chzn-select" style="width: 200px;" name="territory[]" >
					</select>
		
					</td>
				</tr>
				
				 
				
	 		<tr class="inst towns">
					<td class="label">Towns :<span class="red_star">*</span></td>
					<td><select class="chzn-select" style="width: 200px;" name="town[]" multiple="multiple">
					     <option value="0">Choose</option>
					</select>
		              </td>
					</tr>
				</tr>
             	<tr>
					<td>Upload Image : </td>
					<td><input type="file" name="image" id="file"
						value="<?php echo set_value('image')?>">
					</td>

				</tr>

				<tr>
					<td>Upload CV : </td>
					<td><input type="file" name="cv" id="file"
						value="<?php echo set_value('cv')?>">
					</td>

				</tr>

				<tr>
					<td><input  type="submit" value="Add Employee" align="left">
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<style>
	.manageListOptions{
	font-weight:bold;
	font-size:14px;
	cursor:pointer;
	background:#555;
	color:#FFF;
	padding:1px 4px;
	margin:1px;
	border-radius:5px;
	margin-left: 5px;
}
							
</style>
<script type="text/javascript">

$('#contactList').manageList();
							
		
var sel_role_id =0;
	$('select[name="role_id"]').change(function(){
		$('select[name="assigned_under_id"]').html('');
		 sel_role_id = $(this).val();
			$.getJSON(site_url+'/admin/get_superior_names/'+sel_role_id,'',function(resp){
				if(resp.status == 'error'){
					alert(resp.message);
				}else{
					var emp_list_html = '<option value="">Choose</option>';
						$.each(resp.emp_list,function(i,itm){
							emp_list_html += '<option value="'+itm.employee_id+'">'+itm.name+'</option>';
						});
						$('select[name="assigned_under_id"]').html(emp_list_html);
				}
			});
	});

		$('select[name="assigned_under_id"]').change(function(){
			
			$('select[name="territory[]"]').html('');
			var sel_sup_id = $(this).val();
				 
				
				$.getJSON(site_url+'/admin/suggest_territories/'+sel_sup_id,'',function(resp){
					if(resp.status == 'error'){
						alert(resp.message);
					}else{
						var terr_list_html = '';
						 
							
							$.each(resp.terr_list,function(i,itm){
								terr_list_html += '<option value="'+itm.id+'">'+itm.territory_name+'</option>';
							});
							$('select[name="territory[]"]').html(terr_list_html);
							
							$('#'+$('select[name="territory[]"]').attr('id')+'_chzn').remove();


							if(sel_role_id > 3)
							{
								$('select[name="territory[]"]').attr({'id':'','class':'','multiple':false}).val('').chosen();
							}
							else
							{
								$('select[name="territory[]"]').attr({'id':'','class':'','multiple':true}).val('').chosen();
							}
							
							
							$('select[name="territory[]"]').trigger('change');
					}
				});
		});
			
      $('select[name="territory[]"]').change(function(){
          
          $('select[name="town[]"]').html('').trigger("liszt:updated");
          if(sel_role_id > 3){
				sel_sup_id = 0;
		var sel_territory_id = $(this).val();
			$.getJSON(site_url+'/admin/suggest_towns/'+sel_territory_id,'',function(resp){
				if(resp.status == 'error'){
					alert(resp.message);
				}else{
					var town_list_html = '';
					
						$.each(resp.town_list,function(i,itm){
							town_list_html += '<option value="'+itm.town_id+'">'+itm.town_name+'</option>';
						});
						$('select[name="town[]"]').html(town_list_html).trigger("liszt:updated");
				}
			});
          }
	});

  $(".inst_type").change(function(){
		$(".inst").hide();

		$('.inst select').html('').val('').trigger("liszt:updated");
		
		if($(this).val()=="3")
		{
         $(".territory").show();
       
        }
		else if($(this).val()=="4")
		{		
			 $(".territory").show();
		}

		else if($(this).val()=="5")
		{
			$(".territory").show();
			$(".towns").show();
	    }
	}).val("0").change();

	$('#add_contact').click(function(e){
		e.preventDefault();
		$('#add_contact').append('<li><input type="text" name="contact_no[]" value=""> <a href="javascript:void(0)" class="remove_btn">X</a></li>');
		  $('.remove_btn').unbind('click').click(function(e){
			  e.preventDefault();
			  $(this).parent().remove();
		  });
		});

	 

	 
</script>

<style>
.red_star {
	color: rgb(205, 0, 0);
	font-size: 12px;
	font-weight: bold;
	margin-left: 5px;
}

#add_contact li{margin: 5px 0px;}
#add_contact .add_btn{font-size: 12px;text-decoration: none;color: #000;font-weight: bold;}
#add_contact .remove_btn{ color: rgb(205, 0, 0);font-size: 12px;  font-weight: bold;margin-left: 5px;text-decoration: none;}
#add_contact .inputbox{width: 100px;}  
</style>  

 

 




