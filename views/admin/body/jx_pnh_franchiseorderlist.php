<?php 
		
		if($stat)
		{
			$sql = "select sum(o.i_coup_discount) as com,t.amount,o.transid,o.status,o.time,o.actiontime,pu.user_id as userid,pu.pnh_member_id from king_transactions t join king_orders o on o.transid=t.transid join pnh_member_info pu on pu.user_id=o.userid where t.franchise_id=? and t.init between ? and ? group by o.transid order by o.time desc";
			$res = $this->db->query($sql,array($fid,$st_ts,$en_ts));	
		}else
		{
			$sql = "select sum(o.i_coup_discount) as com,t.amount,o.transid,o.status,o.time,o.actiontime,pu.user_id as userid,pu.pnh_member_id from king_transactions t join king_orders o on o.transid=t.transid join pnh_member_info pu on pu.user_id=o.userid where t.franchise_id=? group by o.transid order by o.time desc limit 20";
			$res = $this->db->query($sql,array($fid));
		}
		
		
		$order_stat=array("Confirmed","Invoiced","Shipped","Cancelled");
		
		if(!$res->num_rows())
		{
			echo "<div align='center'><h3 style='margin:2px;'>No Orders found for selected dates</h3></div>";	
		}else
		{
			if(!$stat)
				echo '<div align="left"><h3 style="margin:2px;">Latest 20 Orders</h3></div>';
			else
				echo '<div align="left"><h3 style="margin:2px;">Showing '.$res->num_rows().' Orders from '.format_date(date('Y-m-d',$st_ts)).' to '.format_date(date('Y-m-d',$en_ts)).' </h3></div>';
				
?> 
<table class="datagrid" width="100%">
	<theaD><tr><th>Time</th><th>Order</th><th>Amount</th><th>Commission</th><th>Deal/Product details</th><th>Status</th><th>Last action</th></tr></theaD>
	<tbody>
	
	<?php 
		 
		foreach($res->result_array() as $o)
		{
	?>
	<tr>
	<td><?=format_datetime(date('Y-m-d H:i:s',$o['time']))?></td>
	<td>
		<a href="<?=site_url("admin/trans/{$o['transid']}")?>" class="link"><?=$o['transid']?></a> <br /><br />
		Member ID: <a href="<?=site_url("admin/pnh_viewmember/{$o['userid']}")?>"><?=$o['pnh_member_id']?></a>
	</td>
	<td><?=round($o['amount'],2)?></td>
	<td><?=round($o['com'],2)?></td>
	<td style="padding:0px;">
		<table class="subdatagrid" cellpadding="0" cellspacing="0">
			<thead>
				<th>OID</th>
				<th>ITEM</th>
				<th>QTY</th>
				<th>MRP</th>
				<th>Amount</th>
				<th>Shipped</th>
			</thead>
			<tbody>
				<?php
					$ship_dets = array(); 
					$trans_ttl_orders = 0;
					$o_item_list = $this->db->query("select e.invoice_no,d.packed,d.shipped,e.invoice_status,d.shipped_on,a.status,a.id,a.itemid,b.name,a.quantity,i_orgprice,i_price,i_discount,i_coup_discount 
																from king_orders a
																join king_dealitems b on a.itemid = b.id
																left join proforma_invoices c on c.order_id = a.id 
																left join shipment_batch_process_invoice_link d on d.p_invoice_no = c.p_invoice_no 
																left join king_invoice e on e.invoice_no = d.invoice_no and d.packed = 1 and d.shipped = 1 
															where a.transid = ?
														group by a.id 
													",$o['transid'])->result_array();
					$trans_ttl_orders = count($o_item_list); 
					$trans_ttl_shipped = 0;
					$trans_ttl_cancelled = 0;
					foreach($o_item_list as $o_item)
					{
						$ord_status_color = '';
						$is_shipped = 0;
						$is_cancelled = ($o_item['status']==3)?1:0;
						if($is_cancelled)
						{
							$trans_ttl_cancelled += 1;
							$ord_status_color = 'cancelled_ord';
						}else
						{
							$is_shipped = ($o_item['shipped'])?1:0;;
							if($o_item['shipped'] && $o_item['invoice_status'])
							{
								$trans_ttl_shipped += 1;
								$ship_dets[$o_item['invoice_no']] = format_date($o_item['shipped_on']);
								$ord_status_color = 'shipped_ord';
							}else if($o_item['status'] == 0)
							{
								$ord_status_color = 'pending_ord';
							}
						}
				?>
					<tr class="<?php echo $ord_status_color;?> ">
						<td width="40"><?php echo $o_item['id'] ?></td>
						<td><?php echo anchor('admin/pnh_deal/'.$o_item['itemid'],$o_item['name']) ?></td>
						<td width="20"><?php echo $o_item['quantity'] ?></td>
						<td width="40"><?php echo $o_item['i_orgprice'] ?></td>
						<td width="40"><?php echo round($o_item['i_orgprice']-($o_item['i_coup_discount']+$o_item['i_discount']),2) ?></td>
						<td width="40" align="center"><?php echo ($is_shipped&& $o_item['invoice_status'])?'Yes':'No' ?></td>
					</tr>	
				<?php 		
					}
				?>
			</tbody>
		</table>
	</td>
	<td>
		<?php 
			if(($trans_ttl_orders-$trans_ttl_cancelled) == $trans_ttl_shipped)
			{
				echo "Shipped";
			}else if($trans_ttl_shipped)
			{
				echo "Partitally Shipped";
			}else {
				echo "UnShipped";
			}
			
			echo '<div style="font-size:11px;margin-top:10px;color:green">';
			foreach($ship_dets as $s_invno =>$s_shipdate)
			{
				echo ' <div style="margin:3px 0px"><a target="_blank" href="'.site_url('admin/invoice/'.$s_invno).'">'.$s_invno.'-'.$s_shipdate.'</a></div>';
			}
			echo '</div>';
		?>
	</td>
	<td><?=$o['actiontime']==0?"na":format_datetime(date('Y-m-d H:i:s',$o['actiontime']))?></td>
	</tr>
	<?php }?>
	
	
	</tbody>
	</table>
<?php 
	}
?>

<style>
	.pending_ord{color: blue;}
	.pending_ord a{color: blue !important;}
	.shipped_ord{color: green;}
	.shipped_ord a{color: green !important;}
</style>
