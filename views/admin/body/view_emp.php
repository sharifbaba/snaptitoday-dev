<?php 
$assigned_emp_det= $this->erpm->get_working_under_det($emp_details['employee_id']);
$assigned_terr_det=$this->erpm->get_assigned_territory_det($emp_details['employee_id']);
$assigned_twn_det=$this->erpm->get_assigned_town_det($emp_details['employee_id']);
$assignment_details=$this->erpm->to_get_assignmnt_details($emp_details['employee_id']);

$cfg_task_types_arr = array();
$task_type_list=$this->db->query("SELECT * FROM `pnh_m_task_types` ")->result_array();
foreach($task_type_list as $tsk_type_det)
{
	$cfg_task_types_arr[$tsk_type_det['id']] =$tsk_type_det['task_type'];
}


if($emp_details['role_id'] <= 2){
	$terr_list=$this->db->query("select * from pnh_m_territory_info")->result_array();
	$town_list=$this->db->query("select * from pnh_towns")->result_array();
}else
{
	$terr_list = $assigned_terr_det;
	$town_list = $assigned_twn_det;
}


?>

<div class="container">
	<div id="main_column" class="clear">
	 <div class="cm-notification-container "></div>
		<div class="tools-container">
			
		</div>
		<div>
			<table width="100%">
				<tr>
					<td valign="top">
				
						<div class="emp_bio">
							<a href="<?php echo site_url('admin/list_employee')?>">Back</a>
							<h4 style="margin:0px;font-size: 24px;"><b><?php echo ucwords($emp_details['name'])?></b>
								<a style="margin-left: 10px;font-size: 12px;"	href="<?php echo site_url('admin/edit_employee'.'/'.$emp_details['employee_id'])?>">edit</a>
							</h4>
							<p style="margin-top: 3px;">
								<?php echo $emp_details['role_name'] ?> <br>
								Email: <?php echo $emp_details['email'] ?> <br>
								Mob: <?php echo str_replace(',',', ',$emp_details['contact_no']) ?>
							</p>
							<p>
								<b>Address</b>:<br />
								<?php echo $emp_details['address']?>,
								<?php echo $emp_details['city']?>-<?php echo $emp_details['postcode']?>
							</p>
							
							<p>
								<b>Bio</b>:<br />
								<?php echo $emp_details['gender']?> 
								<?php echo $emp_details['qualification']?>
							</p>
							
							<?php if(strlen(trim($emp_details['cv_url']))!=0){?>
								<p><b>CV</b>: <a href="<?php echo base_url().'resources/employee_assets/cv/'.$emp_details['cv_url']?>">View/Download</a></p> 
							<?php }?>

								<?php if($emp_details['job_title']>1){?>
							<p>
								<b>Working/Assigned Under</b>:<br />
								<?php echo $assigned_emp_det['role_name'].'----'.$assigned_emp_det['name'] ;?>
								
							</p>
							
						</div>
						
						<div class="tabs">
						<b>Task History</b>
						<ul>
							<li><a href="#fu_tasks">Upcoming Tasks</a></li>
							<li><a href="#completed_tasks">Completed Tasks</a></li>
							<li><a href="#closed_tasks">Closed Tasks</a><li>
						</ul>
						<br />
							<div id="fu_tasks">
							<b>Upcoming Tasks</b>
								<?php if($upcoming_tasks){?>
									<table class="datagrid" width="100%">
									<thead>
										<th>Sl no</th>
										<th class="center">Tasks </th>
										<th>Assigned town</th>
										<th>On Date</th>
										<th>Due Date</th>
										<th>Task Status</th>
										<th>Assigned By</th>
										<th>Activity Log</th>
									</thead>
									<tbody>
									<?php  
										$i=1;
										
									  	foreach($upcoming_tasks as $task_det){
								     	
										
									?>
									<tr class="table-row">
										<td><?php echo $i;?></td>
										<td><?php 
											$task_types_arr = explode(',',$task_det['task_type']);
											foreach($task_types_arr as $task_type)
											echo $cfg_task_types_arr[$task_type].',';
										?>
										</td>
										<td><?php echo $task_det['town_name'];?></td>
										<td><?php echo $task_det['on_date'];?></td>
										<td><?php echo $task_det['due_date'];?></td>
										<td><a href="<?php echo site_url('admin/calender#taskview-'.$task_det['id'].'');?>"><?php echo $this->task_status[$task_det['task_status']];?></a></td>
										<td><?php echo $task_det['assigned_byname'];?></td>
										<td><a onclick="view_log('<?php echo $task_det['id'] ?>')" href="javascript:void(0)">View</a></td>
									</tr>
									</tbody>
									<?php 
									$i++;
										}
									?>
										</table>
									<?php 	
						                 }else
						                 	echo 'No Tasks';
					                 ?>
							
							<div class="clear"></div>

							</div>
							
							<div id="completed_tasks">
									<b>Completed Tasks</b>
								<?php if($completed_tasks){?>
									<table class="datagrid" width="100%">
									<thead>
										<th>Sl no</th>
										<th class="center">Tasks </th>
										<th>Assigned town</th>
										<th>On Date</th>
										<th>Due Date</th>
										<th>Task Status</th>
										<th>Assigned By</th>
										<th>Activity Log</th>
										
									</thead>
									<tbody>
									<?php  
										$i=1;
										
							 	foreach($completed_tasks as $completed_task_det){
								     	
										
									?>
									<tr class="table-row">
										<td><?php echo $i;?></td>
										<td><?php 
											$task_types_arr = explode(',',$completed_task_det['task_type']);
											foreach($task_types_arr as $task_type)
											echo $cfg_task_types_arr[$task_type].',';
										?>
										</td>
										<td><?php echo $completed_task_det['town_name'];?></td>
										<td><?php echo $completed_task_det['on_date'];?></td>
										<td><?php echo $completed_task_det['due_date'];?></td>
										<td><a href="<?php echo site_url('admin/calender#taskview-'.$completed_task_det['id'].'');?>"><?php echo $this->task_status[$completed_task_det['task_status']];?></a></td>
										<td><?php echo $completed_task_det['assigned_byname'];?></td>
										<td><a onclick="view_log('<?php echo $completed_task_det['id'] ?>')" href="javascript:void(0)">View</a></td>
										
									</tr>
									</tbody>
									<?php 
									$i++;
										}
									?>
										</table>
									<?php 	
						                 }else 
						                 	echo 'No Tasks';
					                 ?>
							</div>
							
							<div id="closed_tasks">
							<b>Closed Tasks</b>
							<?php if($closed_tasks){?>
									<table class="datagrid" width="100%">
									<thead>
										<th>Sl no</th>
										<th class="center">Tasks </th>
										<th>Assigned town</th>
										<th>On Date</th>
										<th>Due Date</th>
										<th>Task Status</th>
										<th>Assigned By</th>
										<th>Activity Log</th>
									</thead>
									<tbody>
									<?php  
										$i=1;
										
									  	foreach($closed_tasks as $closed_task_det){
								     	
										
									?>
									<tr class="table-row">
										<td><?php echo $i;?></td>
										<td>
										<?php 
											$task_types_arr = explode(',',$closed_task_det['task_type']);
											foreach($task_types_arr as $task_type)
											echo $cfg_task_types_arr[$task_type].',';
										?>
										</td>
										<td><?php echo $closed_task_det['town_name'];?></td>
										<td><?php echo $closed_task_det['on_date'];?></td>
										<td><?php echo $closed_task_det['due_date'];?></td>
										<td><a href="<?php echo site_url('admin/calender#taskview-'.$closed_task_det['id'].'');?>"><?php echo $this->task_status[$closed_task_det['task_status']];?></a></td>
										<td><?php echo $closed_task_det['assigned_byname'];?></td>
										<td><a onclick="view_log('<?php echo $closed_task_det['id'] ?>')" href="javascript:void(0)">View</a></td>
									</tr>
									</tbody>
									<?php 
									$i++;
										}
									?>
										</table>
									<?php 	
						                 }else 
						                 	echo 'No Tasks';
					                 ?>
							</div>
							<?php }?>
						</div>
					</td>
					<td width="100">
						<div class="avatar">
							<?php if($emp_details['photo_url']){?> <img alt="" height="100"
							src="<?php echo base_url().'resources/employee_assets/image/'.$emp_details['photo_url']?>">
							<?php }else{ 
								echo "no image updated";
	
							}?>
						</div>
						<br />
						<div class="module" style="width: 400px;">
							<b>Assigned Territory</b>
							<ul class="list-inline">
								<?php 
								foreach ($terr_list as $assigned_terr)
									echo '<li>'.$assigned_terr['territory_name'].'</li>';
								?>
							</ul>	
						</div>
						<?php  if($emp_details['role_id']==5){?>
						<br />
						<div class="module" style="width: 400px;">
							<b>Assigned Towns</b>
							<ul class="list-inline">
								<?php 
								foreach ($town_list as $assigned_twn) 
									echo '<li>'.$assigned_twn['town_name'].','.'</li>';
								?>
							</ul>	
						</div>
						<?php }else{?>
						<br />
						<div class="module" style="width: 400px;">
							<b>Assigned Towns</b>
							<ul class="list-inline">
								<?php 
								
									echo 'All';
								?>
							</ul>	
						</div>
						<?php }?>
							
						<br />
				
		
					</td>
					 
				</tr>
			</table>
		
        </div>
        
	<div id="view_activity_log" style="display: none; padding: 4px;" title="Activity Log History">
		<table width="100%" class="datagrid" id=task_log>
		<thead>
			<th>Task Start Date</th>
			<th>Task End Date</th>
			<th>Task Status</th>
			<th>Remarks</th>
			<th>Logged On</th>
			<th>Logged By</th>
		</thead>
		<tbody>
				
		</tbody>
		</table>
	</div>
</div>
</div>

<script>
	function view_log(task_id)
	{
		$('#view_activity_log').data('task_id',task_id).dialog('open');
	}
	
	$( "#view_activity_log" ).dialog({
		modal:true,
		autoOpen:false,
		width:'516',
		height:'auto',
		open:function(){
		dlg = $(this);
	
		// ajax request fetch activity log details
		$('#task_log tbody').html("");
		   $.post(site_url+'/admin/jx_view_activitylog',{task_id:$(this).data('task_id')},function(result){
		   if(result.status == 'error')
			{
				alert("Activity Log details not found");
				dlg.dialog('close');
		    }
		    else
			{
				$.each(result.activity_log,function(k,v){
					 var tblRow =
						 "<tr>"
						  +"<td>"+v.start_date+"</td>"
						  +"<td>"+v.end_date+"</td>"
						  +"<td>"+result.task_status_list[v.task_status]+"</td>"
						  +"<td>"+v.msg+"</td>"
						  +"<td>"+v.logged_on+"</td>"
						  +"<td>"+v.name+"</td>"
						  +"</tr>"
						  $(tblRow).appendTo("#task_log tbody");
				});
	
			}
				
		   },'json');
	}
	});

</script>
<style>
	.list-inline li{display: inline-block;text-transform: capitalize;}
</style>