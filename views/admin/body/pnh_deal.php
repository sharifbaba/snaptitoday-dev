
<div class="container">

<div style="float:right;padding:0px 50px;">
<?php $d=$deal; $stock=$this->erpm->do_stock_check(array($d['id'])); if(empty($stock)){?>
<h4 style="color:red">OUT OF STOCK</h4>
<?php }else{?>
<h4 style="color:green">IN-STOCK</h4>
<?php }?>
</div>

<h2>PNH Deal details</h2>
<a href="<?=site_url("admin/pnh_editdeal/{$deal['id']}")?>">Edit deal</a>
<br>
<a href="<?=site_url("admin/viewcat/{$deal['catid']}")?>"><?=$deal['category']?></a> &raquo; <a href="<?=site_url("admin/viewbrand/{$deal['brandid']}")?>"><?=$deal['brand']?></a> 
<img src="<?=IMAGES_URL?>items/<?=$deal['pic']?>.jpg" style="float:right;margin-right:20px;">
<table cellpadding=3 class="datagrid">
<tr><td>PNH PID :</td><Td><?=$deal['pnh_id']?></Td></tr>
<tr><td>Deal Name :</td><Td style="font-weight:bold;"><?=$deal['name']?></Td></tr>
<tr><td>Tagline :</td><td><?=$deal['tagline']?></td></tr>
<tr><td>MRP :</td><td>Rs <?=$deal['orgprice']?></td></tr>
<tr><td>Offer price :</td><td>Rs <?=$deal['price']?></td></tr>
<tr><td>Store price :</td><td>Rs <?=$deal['store_price']?></td></tr>
<tr><td>NYP price :</td><td>Rs <?=$deal['nyp_price']?></td></tr>
<tr><Td>Gender Attribute :</Td><td><?=$deal['gender_attr']?></td></tr>
<tr><td>Brand :</td><Td style="font-weight:bold;"><?=$deal['brand']?></Td></tr>
<tr><td>Category :</td><Td style="font-weight:bold;"><?=$deal['category']?></Td></tr>
<tr><td>Description :</td><Td style="font-weight:bold;"><?=$deal['description']?></Td></tr>
<?php $d=$deal;?>
<tr>
<td>Status :</td><td><?=$d['publish']==1?"Enabled":"Disabled"?> 
<a class="danger_link" href="<?=site_url("admin/pnh_pub_deal/{$d['id']}/{$d['publish']}")?>">change</a></td></tr>
</table>

<div>
<h4 style="margin-bottom:0px;">Make it as Special Margin Deal</h4>
<div style="color:#777;display:inline-block;padding:3px 5px;background:#eee;">(Calculate Margin on <b>Offer Price</b>)</div>
<form action="<?=site_url("admin/pnh_special_margin_deal/{$deal['id']}")?>" method="post">
<table class="datagrid noprint">
<tr><Td>Special Margin : </Td><td><input type="text" name="special_margin" class="inp" size=6> in <label><input type="radio" name="type" value=0 checked="checked">%</label> <label><input type="radio" name="type" value=1>Rs</label></td></tr>
<tr><td>From : </td><td><input type="text" class="inp" name="from" id="sm_from"></td></tr>
<tr><td>To : </td><td><input type="text" class="inp" name="to" id="sm_to"></td></tr>
<tr><td></td><Td><input type="submit" value="Submit"></Td></tr>
</table>
</form>
</div>


	
	<div style="float:left;margin-right:20px;">
	<h4 style="margin-bottom:0px;">Price changelog</h4>
	<table class="datagrid smallheader noprint">
	<thead><tr><th>Sno</th><th>Old MRP</th><th>New MRP</th><th>Old Price</th><th>New Price</th><th>Reference</th><th>Date</th></tr></thead>
	<tbody>
	<?php $i=1; foreach($this->db->query("select * from deal_price_changelog where itemid=? order by id desc",$deal['id'])->result_array() as $pc){?>
	<tr>
	<td><?=$i++?></td>
	<td>Rs <?=$pc['old_mrp']?></td>
	<td>Rs <?=$pc['new_mrp']?></td>
	<td>Rs <?=$pc['old_price']?></td>
	<td>Rs <?=$pc['new_price']?></td>
	<td>
	<?php if($pc['reference_grn']==0) echo "MANUAL";else{?>
	<a href="<?=site_url("admin/view_grn/{$pc['reference_grn']}")?>"><?=$pc['reference_grn']?></a>
	<?php }?>
	</td>
	<td><?=date("g:ia d/m/y",$pc['created_on'])?></td>
	</tr>
	<?php }?>
	</tbody>
	</table>
	</div>

<div style="margin-right:20px;">
<h4 style="margin-bottom:0px;">Special Margin history</h4>
<table class="datagrid smallheader">
<thead><tr><Th style="text-align: center;">Margin %</Th><Th style="text-align: center;">Margin Rs</Th><Th style="text-align: center;">Price</Th><th>From</th><th>To</th><th>Assigned on</th><th>Assigned by</th></tr></thead>
<tbodY>
<?php foreach($this->db->query("select s.*,a.name as admin from pnh_special_margin_deals s join king_admin a on a.id=s.created_by where s.itemid=? order by id desc",$deal['id'])->result_array() as $s){?>
<tr><td style="color: maroon;font-weight: bold"><?=$s['special_margin']?>%</td><td style="color: maroon;font-weight: bold"><?php echo (($deal['orgprice']*$s['special_margin']/100))?> </td><td style="color: green;font-weight: bold;"><?php echo ($deal['orgprice']-($deal['orgprice']*$s['special_margin']/100))?> </td><td><b><?=date("d/m/y",$s['from'])?></b></td><td><b><?=date("d/m/y",$s['to'])?></b></td><td><?=date("g:ia d/m/y",$s['created_on'])?></td><td><?=$s['admin']?></td></tr>
<?php }?>
</tbodY>
</table>
</div>


<div style="float:left;padding-right:20px;clear:both;">
<h4 style="margin-bottom:0px;">Linked Product Groups</h4>
<table class="datagrid smallheader noprint">
<thead><tr><th>Sno</th><th>ID</th><th>Group Name</th><th>Qty</th></tr></thead>
<tbody>
<?php foreach($this->db->query("select gl.*,g.group_name from m_product_group_deal_link gl join products_group g on g.group_id=gl.group_id where gl.itemid=?",$deal['id'])->result_array() as $i=>$p){?>
<tr><td><?=++$i?></td><td><?=$p['group_id']?></td><td><a href="<?=site_url("admin/product_group/{$p['group_id']}")?>"><?=$p['group_name']?></a></td><td><?=$p['qty']?></td></tr>
<?php }?>
</tbody>
</table>
</div>


<h4 style="margin-bottom:0px;">Linked Products</h4>
<table class="datagrid smallheader noprint">
<thead><tr><th>Sno</th><th>ID</th><th>Product Name</th><th>Qty</th></tr></thead>
<tbody>
<?php foreach($prods as $i=>$p){?>
<tr><td><?=++$i?></td><td><?=$p['product_id']?></td><td><a href="<?=site_url("admin/product/{$p['product_id']}")?>"><?=$p['product_name']?></a></td><td><?=$p['qty']?></td></tr>
<?php }?>
</tbody>
</table>


<div class="clear"></div>

<h4 style="margin-bottom:0px;">Recent Orders</h4>
<table class="datagrid smallheader noprint">
<thead><tr><th>Transid</th><th>Franchise</th><th>Amount</th><th>Date</th></tr></thead>
<tbody>
<?php foreach($this->db->query("select o.time,f.franchise_name,f.franchise_id,o.transid,t.amount from king_orders o join king_transactions t on t.transid=o.transid join pnh_m_franchise_info f on f.franchise_id=t.franchise_id where o.itemid=? order by o.time desc limit 5",$deal['id'])->result_array() as $d){?>
<tR>
<td><a class="link" href="<?=site_url("admin/trans/{$d['transid']}")?>"><?=$d['transid']?></a></td>
<td><a href="<?=site_url("admin/pnh_franchise/{$d['franchise_id']}")?>"><?=$d['franchise_name']?></a></td>
<td><?=$d['amount']?></td>
<td><?=date("g:ia d/m/y",$d['time'])?></td>
</tR>
<?php }?>
</tbody>
</table>

<h3 style="margin-bottom:0px;">Extra Images</h3><a href="<?=site_url("admin/pnh_deal_extra_images/{$deal['id']}")?>">Upload more pics</a>
<div>
<?php foreach($this->db->query("select id from king_resources where itemid=? and type=0",$deal['id'])->result_array() as $img){?>
<img src="<?=IMAGES_URL?>items/small/<?=$img['id']?>.jpg" style="float:left;margin:10px;border:1px solid #aaa;">
<?php }?>
</div>


</div>

<script>
$(function(){
$("#sm_from,#sm_to").datepicker();
});
</script>

<?php
