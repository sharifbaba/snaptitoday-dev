<?php 

	$pnh_exec_const_val=$this->db->query("select value from user_access_roles where const_name='PNH_EXECUTIVE_ROLE'")->row()->value;
	

	$tmenu=array();
	if(isset($menu))
		$tmenu=$menu;
	$menu=array();
	$user=$this->session->userdata("admin_user");
	$is_pnh_exec_only=false;
	if($user['access']==$pnh_exec_const_val)
			$is_pnh_exec_only=true;
	$sub="prod_menu";
/*
	$subs=array("prod"=>"Products","selling"=>"Selling","stock"=>"Warehousing","control"=>"Website Control","marketing"=>"Marketing","accounting"=>"Accounting","crm"=>"Customer Relationship","admin"=>"Admin Control");
	
	$menu["prod"]=array("menu"=>"Menu","categories"=>"Categories","deals"=>"Deals","brands"=>"Brands","products"=>"Products","freesamples"=>"Free Samples","vendors"=>"Vendors","variants"=>"Variants");
	$menu["selling"]=array("orders"=>"Orders","offline"=>"Offline Orders","callcenter"=>"callcenter","batch_process"=>"Shipment batch process","pending_batch_process"=>"Pending Shipment batch process","outscan"=>"Outscan Order","courier"=>"Courier");
	$menu['stock']=array("purchaseorders"=>"View POs","purchaseorder"=>"Purchase Order","po_product"=>"PO Productwise","apply_grn"=>"Update Stock","barcode"=>"Barcode","storage_locs"=>"Storage Locations","rackbins"=>"Rack & Bins");
	$menu['marketing']=array("featured_newsletter"=>"Newsletter","announcements"=>"Announcements","stats"=>"Stats","reports/kfile"=>"Generate K file","reports/order_summary"=>"Order Summary","coupons"=>"Coupons","cashback_campaigns"=>"Cashback Campaigns","pointsys"=>"Loyalty Points","headtotoe"=>"Head to toe");
	$menu['accounting']=array("vouchers"=>"Vouchers","create_voucher"=>"Create Voucher","pending_pay_grns"=>"Ready for payment","pending_grns"=>"Pending Unaccounted GRNs");
	$menu['crm']=array("support"=>"Customer Support","users"=>"Site Users","review"=>"Reviews","corporate"=>"Corporates","callcenter"=>"Call Center","tools"=>"Tools");
	$menu['control']=array("cache_control"=>"Cache Control","activity"=>"Activity","vars"=>"Vars");
	$menu['admin']=array("adminusers"=>"Admin Access","roles"=>"User Roles");
*/	

	$subs=array("prod"=>"Products","stock"=>"Warehousing","front"=>"Front-end","selling"=>"Sales","shipment"=>"Shipment","crm"=>"Customer Relationship","accounting"=>"Accounting & Admin","marketing"=>"Marketing","pnh"=>"Pay Near Home");
	
	$menu["prod"]=array("products"=>"Products","products_group"=>"Products Group","categories"=>"Categories","brands"=>"Brands","prods_bulk_upload"=>"Product bulk upload","products_group_bulk_upload"=>"Products group bulk upload","export_data"=>"Export Data","prod_mrp_update"=>"MRP Update","product_price_changelog"=>"Price Changelog");
	$menu['stock']=array("storage_locs"=>"Storage Locations","rackbins"=>"Rack & Bins","vendors"=>"Vendors","paflist"=>"PAF List","purchaseorders"=>"View POs","purchaseorder"=>"Create PO","apply_grn"=>"Stock Intake","stock_intake_list"=>"Stock Intakes Summary","stock_unavail_report"=>"Stock Unavailability Report","warehouse_summary"=>"Warehouse Summary");
	$menu['front']=array("menu"=>"Menu","deals"=>"Deals","deals_table"=>"Deals table","deals_bulk_upload"=>"Deals Bulk upload","freesamples"=>"Free Samples","variants"=>"Variants","cache_control"=>"Cache Control","activity"=>"Activity","vars"=>"Vars","deal_price_changelog"=>"Price Changelog","partner_deal_prices"=>"Bulk partner deal price update","auto_image_updater"=>"Auto Image updater");
	$menu["selling"]=array("orders"=>"Orders","order_summary"=>"Order Summary","partner_orders"=>"Partner Orders","partner_order_import"=>"Partner Order Import","offline"=>"Offline Orders","callcenter"=>"Recent Transaction","sales_analytics_graph"=>'Sales Graph');
	$menu['shipment']=array("batch_process"=>"Shipment batch process","pending_batch_process"=>"Pending Shipment batch process","outscan"=>"Outscan Order","generate_kfile"=>"Generate kfile","generate_manifesto"=>"Generate Manifesto","update_ship_kfile"=>"Update shipment kfile","courier"=>"Courier","pnh_shipment_sms_notify"=>"PNH Shipment SMS Notification","packed_list"=>"Packed Summary","outscan_list"=>"Outscan Summary");
	$menu['crm']=array("support"=>"Customer Support/Tickets","users"=>"Site Users","review"=>"Reviews","callcenter"=>"Recent transactions","stock_checker"=>"Stock Checker");
	$menu['accounting']=array("vouchers"=>"Vouchers","create_voucher"=>"Create Voucher","pending_pay_grns"=>"Ready for payment","pending_grns"=>"Unaccounted Stock Intakes","adminusers"=>"Admin Access","roles"=>"User Roles","clients"=>"Corporate Clients","client_orders"=>"Corporate Orders","client_invoices"=>"Corporate Invoices","pending_refunds_list"=>"Pending Refunds","partners"=>"Partners","deals_report"=>"Deals Report","pnh_investor_report"=>"PNH Sales Report","investor_report"=>"Investor Report");
	$menu['marketing']=array("featured_newsletter"=>"Newsletter","announcements"=>"Announcements","stats"=>"Stats","coupons"=>"Coupons","cashback_campaigns"=>"Cashback Campaigns","pointsys"=>"Loyalty Points","headtotoe"=>"Head to toe");
	$menu['pnh']=array("pnh_franchises"=>"Franchises","pnh_class"=>"Admin","pnh_deals"=>"Deals","pnh_members"=>"Members","list_employee"=>"Employees","pnh_special_margins"=>"Special Margins","pnh_offline_order"=>"Place Order","pnh_quotes"=>"Order Quotes","pnh_pending_receipts"=>"Pending Receipts","pnh_comp_details"=>"Company Details","pnh_catalogue"=>"Products Catalogue","pnh_special_margins"=>"Discounts","pnh_add_credits"=>"Add Credit","pnh_gen_statement"=>"Generate Account Statement",'pnh_sales_report'=>"PNH Sales Report","pnh_employee_sales_summary"=>"Employee Sales Summary");
	
	$submenu['list_employee']=array("list_employee"=>"Employees","add_employee"=>"Add Employees","assignment_histroy"=>"Assignment Histroy","roletree_view"=>"Role Tree View","calender"=>"Calender View","manage_routes"=>"Routes");
	$submenu['products']=array("addproduct"=>"Add Product");
	$submenu['categories']=array("addcat"=>"Add Category");
	$submenu['brands']=array("addbrand"=>"Add Brand");
	$submenu['vendors']=array("addvendor"=>"Add Vendor");
	$submenu['support']=array("addticket"=>"Add Ticket");
	
	$submenu['paflist'] = array("createpaf"=>"Create PAF","paflist"=>"List all PAF");
	
	
	$submenu['pnh_members']=array("pnh_addmember"=>"Add Member");
	$submenu['clients']=array("addclient"=>"Add Client");
	$submenu['client_orders']=array("addclientorder"=>"Add Client Order");
	$submenu['pnh_franchises']=array("pnh_addfranchise"=>"Add franchise");
	
	$submenu['menu']=array("addmenu"=>"Add Menu");
	$submenu['deals']=array("adddeal"=>"Add Deal");
	
	$submenu['orders']=array('orders/1'=>'Pending Orders','partial_shipment'=>'Partial Shipment Orders','disabled_but_possible_shipment'=>'Disabled But Possible');
	
	$submenu['stock_unavail_report']=array('stock_unavail_report/0/0/0/0'=>'Show All Orders','stock_unavail_report/0/0/0/1'=>'Show Snapittoday Orders','stock_unavail_report/0/0/0/2'=>'Show PNH Orders');
	
 
	
	$submenu['purchaseorder']=array("purchaseorder"=>"Vendorwise","po_product"=>"Productwise");
	$submenu['pnh_class']=array("pnh_class"=>"Class","pnh_less_margin_brands"=>"Less margin brands","pnh_sms_log"=>"SMS Log","pnh_device_type"=>"Device Types","pnh_loyalty_points"=>"Loyalty points","pnh_territories"=>"Territories","pnh_towns"=>"Towns","pnh_app_versions"=>"App Versions","pnh_order_import"=>"Import orders","pnh_member_card_batch"=>"MID card printing batch","pnh_version_price_change"=>"Version price changes","pnh_sms_campaign"=>"SMS Campaign");
	$submenu['pnh_special_margins']=array("pnh_special_margins"=>"Special Margins","pnh_sch_discounts"=>"List scheme discounts","pnh_bulk_sch_discount"=>"Add Scheme Discounts");
	$submenu['pnh_deals']=array("pnh_adddeal"=>"Add Deal","pnh_deals"=>"List Deals","pnh_deals_bulk_upload"=>"Deals Bulk upload","pnh_update_description"=>"Update description");
	
	$extras['front']=array("edit");
	$extras['marketing']=array("coupon");
	$extras['crm']=array("ticket","user");
	$extras['selling']=array("orders","transbystatus","batch","trans");
	$extras['stock']=array("apply_grn","viewpo");
	$extras['pnh']=array("pnh_franchise","pnh");
	
	$submenu['ticket']=array("addticket"=>"Add Ticket");
	
	if($this->uri->segment(2)!="dashboard")
	{
		$uri=substr(strstr(substr($this->uri->uri_string(),1),"/"),1);
		foreach($menu as $id=>$m)
		{
		foreach($m as $u=>$s)
			if(strstr($uri,$u)!==false)
			{
				$sub="{$id}_menu";
				break;
			}
		if(isset($extras[$id]))
			foreach($extras[$id] as $e)
				if(strstr($uri,$e)!==false)
				{
					$sub="{$id}_menu";
					break;
				}
		}
		echo '<script>submenu="'.$sub.'"</script>';
	}
	
	if($is_pnh_exec_only)
		foreach(array("prod","stock","front","selling","shipment","crm","accounting","marketing") as $i)
			unset($menu[$i]);
?>
<div id="hd" class="container">

	<div style="padding-left:20px ">
	<form style="display:none;" id="searchform" action="<?=site_url("admin/search")?>" method="post">
		<input type="hidden" name="q" id="searchkeyword">
	</form>

	<div class="logo_cont">
		<a href="<?=site_url("admin/dashboard")?>"><?php if($is_pnh_exec_only){?><img style="margin:20px 0px 0px 0px;width:120px;" src="<?=base_url()?>images/paynearhome.png"><?php }else{?><img style="margin:20px 0px 0px 0px;width:120px;" src="<?=base_url()?>images/paynearhome.png"><?php }?></a>
	</div>

<?php if($user){ ?>
	<div class="welcomeuser">
		<div class="username" align="right">Welcome <b><?php echo $user["username"];?></b>
		<a href="<?=site_url("admin/changepasswd")?>" style="color:#fff;font-size:75%;text-decoration: underline;">change password</a> 
		<a class="signout" href="<?=site_url("admin/logout")?>">Sign Out</a></div>
		<div style="clear:right;float:right;padding-top:5px;"><input type="text" id="searchbox" value="Search..." style="width:250px;"><input type="button" id="searchtrigh" value="Go!"></div>
	</div>
	<div style="float:right;margin-top:20px;margin-right:0px;">
		<img src="<?=IMAGES_URL?>phone.png" style="cursor:pointer;" onclick='$("#phone_booth").toggle()'>
	</div>
	<div style="float:right;margin-top:20px;margin-right:20px;">
		<a href="<?=site_url("admin/pnh_offline_order")?>"><img src="<?=IMAGES_URL?>place_order.png" style="cursor:pointer;"></a>
	</div>
	<script>
	$(function(){
		$("#phone_booth form").submit(function(){
			if(!is_required($(".pb_customer",$(this)).val()))
			{
				alert("Please enter Customer number");
				return false;
			}
			if(!is_required($(".pb_agent",$(this)).val()))
			{
				alert("Please enter Agent number");
				return false;
			}
			$(".loading",$(this)).show();
			$.post("<?=site_url("admin/makeacall")?>",$(this).serialize(),function(data){
				$("#phone_booth .loading").hide();
				if(data=="0")
					show_popup("Error in initiating call");
				else
				{
					$("#phone_booth").hide();
					show_popup("Call Initiated");
				}
			});
			return false;
		});
	});
	</script>
	
	
	<div id="phone_booth">
		<form>
			<div style="color:#ccc;" align="center">Prefix '0' for mobile numbers</div>
			<table>
				<tr><td>Customer Number : </td><td><input type="text" class="inp pb_customer" name="customer"></td></tr>
				<tr><td>Your number : </td><td><input type="text" class="inp pb_agent" name="agent" value="<?=$this->session->userdata("agent_mobile")?>"></td></tr>
				<tr><td><img src="<?=IMAGES_URL?>loader.gif" class="loading" style="display:none;"></td><td><input type="submit" value="Call Customer"><input type="button" value="Close" onclick='$("#phone_booth").hide()'></td></tr>
			</table>
		</form>
	</div>
	
	<div class="menu_cont">
	<ul class="menu">
		<li>
			<a href="<?=site_url("admin/dashboard")?>">Dashboard</a>
		</li>
	<?php foreach($menu as $id=>$m){?>
		<li id="<?=$id?>_menu">
			<a href="<?=site_url("admin/".key($m))?>"><?=$subs[$id]?></a>
			<ul>
				<?php foreach($m as $u=>$s){?>
				<li>
					<?php if(isset($submenu[$u])){?>
					<span>&raquo;</span>
					<ul class="submenu <?=(($u=="pnh_class"||$u=="list_employee"||$u=="pnh_franchises"||$u=="pnh_deals"||$u=="pnh_members"||$u=="pnh_special_margins")?"submenuright":"")?>">
						<?php foreach($submenu[$u] as $ur=>$sm){?>
							<li><a href="<?=site_url("admin/$ur")?>" <?=$this->uri->segment(2)==$u?"class='selected'":""?>><?=$sm?></a></li>
						<?php }?>
					</ul>
					<?php }?>
					<a href="<?=site_url("admin/$u")?>" <?=$this->uri->segment(2)==$u?"class='selected'":""?>><?=$s?></a>
				</li>
				<?php }?>
			</ul>
		</li>
	<?php }?>
		<li class="clear"></li>
	</ul>
	<div class="clear"></div>
	</div>
<?php }?>

	
	</div>
	<div class="clear"></div>
</div>
<style type="text/css">
#searchbox{
	color:#aaa;
	font-size:14px;
	}
</style>
<script type="text/javascript">
 
$(function(){
	$("#searchbox").focus(function(){
		sr=$(this);
		if(sr.val()=="Search...")
		{
			sr.css("color","#000");
			sr.val("");
		}
	});
	$("#searchbox").blur(function(){
		sr=$(this);
		if(sr.val().length==0)
		{
			sr.css("color","#aaa");
			sr.val("Search...");
		}
	});
	$("#searchbox").keypress(function(e){
		 
		if(e.which==13)
		{
			$("#searchkeyword").val($(this).val());
			$("#searchform").submit();
		}
	});
	$("#searchtrigh").click(function(){
		if($("#searchbox").val()=="Search...")
		{
			alert("inpput!!!");return;
		}
		$("#searchkeyword").val($("#searchbox").val());
		$("#searchform").submit();
	});


	$("#searchform").submit(function(){
		var srch_val = $.trim($("#searchbox").val());
			$("#searchbox").val(srch_val);
			$("#searchkeyword").val(srch_val);
	});
	
});
</script>
<?php 
$menu=$tmenu;
?>