<?php
foreach($reviews as $r){?>
<div class="review">
<table width="100%" cellpadding=0 cellspacing=0 border=0>
<tr>
<td width="140" valign="top">
	<div class="name"><?=$r['name']?></div>
	<div class="date"><?=date("d M y",$r['time'])?></div>
	<div class="rate"><?php for($i=1;$i<=$r['rating'];$i++){?><img src="<?=IMAGES_URL?>star.png"><?php }?><?php for(;$i<=5;$i++){?><img src="<?=IMAGES_URL?>unstar.png"><?php }?></div>
	<div class="flags">
	<?php if($r['first']){?>
		<img src="<?=IMAGES_URL?>firstreview.png">
	<?php }?>
	<?php if($r['buyer']){?>
		<img src="<?=IMAGES_URL?>certifiedbuyer.png">
	<?php }?>
	</div>
</td>
<td valign="top">
	<div class="head"><span>	<img src="<?=IMAGES_URL?>loader_gold.gif" class="load">
	<?=empty($r['title'])?"No Title":$r['title']?></span></div>
	<div class="cont">
		<?=$r['review']?>
	</div>
	<div class="thumbscont">
	<div class="thumbs">
		<input type="hidden" value="<?=md5($r['id']."sf9i35rjse9fisdf")?>">
		Was this review useful to you? <a href="javascript:void(0)" class="yes">Yes</a> / <a href="javascript:void(0)" class="no">No</a>
	</div>
	</div>
	<div class="useful">
		<b class="up"><?=$r['thumbs_up']?></b> of <b class="uppdown"><?=$r['thumbs_up']+$r['thumbs_down']?></b> users found this useful
	</div>
</td>
</tr>
</table>
</div>
<?php } if(empty($reviews)){?>
<h3>No reviews yet</h3>
<?php }?>