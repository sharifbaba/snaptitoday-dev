<?php
/*
 * Created on Jun 21, 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

 class Callcenter extends Controller{
 	
 	function __construct()
 	{
 		parent::__construct();
 		$this->load->model("callcm","dbm");
 	}
 	
 	function index()
 	{
 		$this->checkuser();
 		$data['page']="recent";
 		$data['trans']=$this->dbm->getrecenttrans();
 		$this->load->view("callcenter",$data);
 	}
 	
 	function checkuser()
 	{
 		$user=$this->session->userdata("callc_user");
 		if(!$user)
 		{
 			if($this->session->userdata("admin_user"))
 				return true;
 			redirect("callcenter/login");
 		}
 	}
 	
 	function logout()
 	{
 		$this->session->unset_userdata("callc_user");
 		redirect("callcenter");
 	}
 	
 	function trans($transid)
 	{
 		$this->checkuser();
 		$data['trans']=$trans=$this->dbm->gettrans($transid);
 		if(empty($trans))
 		{
 			$data['page']="echo";
 			$data['echo']="<h2>Transaction not found</h2><div style='padding:20px;'></div>";
 		}
 		else{
 			$data['page']="trans";
 			$data['pendings']=$this->dbm->getpendingorders($transid);
 			$data['orders']=$this->dbm->getorders($transid);
 		}
 		$this->load->view("callcenter",$data);
 	}
 	
 	function login()
 	{
 		if($_POST)
 		{
 			$r=$this->dbm->login();
 			if($r)
 				redirect("callcenter");
 			$data['error']=true;
 		}
 		$data['page']="login";
 		$this->load->view("callcenter",$data);
 	}
 	
 }