<?php 

class Erp extends Controller
{
	public $user_access=array();

	function __construct()
	{
		parent::__construct();

		$this->po_status=$this->config->item('po_status');
		$this->task_status = $this->config->item('task_status');
		$this->task_for=$this->config->item('task_for');
	}

	function rackbins()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|STOCK_INTAKE_ROLE|PURCHASE_ORDER_ROLE);
		$data['locs']=$locs=$this->db->query("select * from m_storage_location_info order by location_name")->result_array();
		if(empty($locs))
			show_error("no storage location available<br>please add one!");
		$data['rackbins']=$this->erpm->getrackbins();
		$data['page']="rackbins";
		$this->load->view("admin",$data);
	}

	function addrackbin()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|STOCK_INTAKE_ROLE|PURCHASE_ORDER_ROLE);
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']))
			$this->db->query("update m_rack_bin_info set rack_name=?,bin_name=? where id=? limit 1",array($_POST['rack'],$_POST['bin'],$_POST['id']));
		else
			$this->db->query("insert into m_rack_bin_info(location_id,rack_name,bin_name,created_on) values(?,?,?,now())",array($this->input->post("loc"),$this->input->post("rack"),$this->input->post("bin")));
		redirect("admin/rackbins");
	}

	function productsbytax($tax=false)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		if(empty($tax))
			show_404();
		$data['tax']=number_format($tax,2);
		$data['products']=$this->erpm->getproductsbytax($tax);
		$data['page']="products";
		$this->load->view("admin",$data);
	}

	function productsbybrand($bid=false)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		if(empty($bid))
			show_404();
		$data['brand']=$this->db->query("select name from king_brands where id=?",$bid)->row()->name;
		$data['products']=$this->erpm->getproductsbybrand($bid);
		$data['page']="products";
		$this->load->view("admin",$data);
	}

	function viewlinkeddeals($pid)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		$data['product']=$this->db->query("select product_name as p from m_product_info where product_id=?",$pid)->row()->p;
		$data['deals']=$this->erpm->getlinkeddealsforproduct($pid);
		$data['page']="linked_deals_for_products";
		$this->load->view("admin",$data);
	}

	function update_barcode()
	{
		$user=$this->auth();
		$pid=$this->input->post("pid");
		$barcode=$this->input->post("barcode");
		$this->db->query("update m_product_info set barcode=? where product_id=? limit 1",array($barcode,$pid));
		if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']))
		{
			$this->erpm->flash_msg("Barcode updated");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function mark_src_products()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		$pids=$this->input->post("pids");
		$action=$this->input->post("action");
		if(!empty($pids))
		{
			$pids=explode(",",$pids);
			$this->db->query("update m_product_info set is_sourceable=$action where product_id in ('".implode("','",$pids)."') limit ".count($pids));
			$this->erpm->flash_msg(count($pids)." products marked as ".($action=="0"?"NOT":"")." Sourceable");
			foreach($pids as $pid)
			{
				$inp=array("product_id"=>$pid,"is_sourceable"=>$action,"created_on"=>time(),"created_by"=>$user['userid']);
				$this->db->insert("products_src_changelog",$inp);
			}
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function add_products_group()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		if($_POST)
			$this->erpm->do_add_products_group();
		$data['page']="add_products_group";
		$this->load->view("admin",$data);
	}

	function createproductgroupscat()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		$cat=$this->input->post("cat");
		if(empty($cat)) show_error("Enter a valid category name");
		if($this->db->query("select 1 from products_group_category where name=?",$cat)->num_rows()!=0) show_error("$cat already exists");
		$this->db->insert("products_group_category",array("name"=>$cat,"created_on"=>time(),"created_by"=>$user['userid']));
		redirect($_SERVER['HTTP_REFERER']);
	}

	function products_group($catid=null)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		$sql="select g.*,c.name as category,count(distinct gp.product_id) as pids from products_group g left outer join products_group_category c on c.id=g.cat_id left outer join products_group_pids gp on gp.group_id=g.group_id";
		if($catid)
			$sql.=" where g.cat_id=$catid ";
		$sql.=" group by g.group_id order by g.group_id desc";
		if(!$catid)
			$sql.=" limit 100";
		if($catid)
			$data['pagetitle']="Products group of category:".$this->db->query("select name from products_group_category where id=?",$catid)->row()->name;
		else
			$data['pagetitle']="Recently created Products Group";
		$data['groups']=$this->db->query($sql)->result_array();
		$data['page']="products_group";
		$this->load->view("admin",$data);
	}

	function product_group($gid)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		$data['group']=$this->db->query("select * from products_group where group_id=?",$gid)->row_array();
		$data['prods']=$this->db->query("select gp.*,p.product_name from products_group_pids gp join m_product_info p on p.product_id=gp.product_id where gp.group_id=? group by gp.product_id order by p.product_name",$gid)->result_array();
		$data['page']="product_group";
		$this->load->view("admin",$data);
	}

	function products()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		$data['products']=$this->erpm->getproducts();
		$data['page']="products";
		$this->load->view("admin",$data);
	}

	function products_group_bulk_upload()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		if($_POST)
			$this->erpm->do_products_group_bulk_upload();
		$data['page']="products_group_bulk_upload";
		$this->load->view("admin",$data);
	}

	function pnh_deals_bulk_upload()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		if($_FILES)
			$this->erpm->do_pnh_deals_bulk_upload();
		$data['page']="pnh_deals_bulk_upload";
		$this->load->view("admin",$data);
	}

	function auto_image_updater()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		if($_POST)
		{
			$this->db->query("update cron_image_updater_lock set finish_status=0,is_locked=0,modified_by=?,modified_on=?,images_updated=0",array($user['userid'],time()));
			redirect("admin/auto_image_updater");
		}
		$data['page']="auto_image_updater";
		$this->load->view("admin",$data);
	}

	function deals_bulk_upload()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		if($_FILES)
			$this->erpm->do_deals_bulk_upload();
		$data['page']="deals_bulk_upload";
		$this->load->view("admin",$data);
	}

	function prods_bulk_upload()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		if($_FILES)
			$this->erpm->do_prods_bulk_upload();
		$data['page']="prods_bulk_upload";
		$this->load->view("admin",$data);
	}

	function view_variant($vid)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		$data['variant']=$this->db->query("select * from variant_info where variant_id=?",$vid)->row_array();
		$data['items']=$this->erpm->getdealitemsforvariant($vid);
		$data['page']="view_variant";
		$this->load->view("admin",$data);
	}

	function variants()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		$data['variants']=$this->erpm->getvariants();
		$data['page']="variants";
		$this->load->view("admin",$data);
	}

	function addvariant()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		if($_POST)
			$this->erpm->do_addvariant();
		$data['page']="addvariant";
		$this->load->view("admin",$data);
	}

	function vendor($vid=false)
	{
		if(!$vid)
			show_error("Vendor ID missing");
		$user=$this->auth(PURCHASE_ORDER_ROLE);
		$data['vendor']=$this->erpm->getvendor($vid);
		$data['contacts']=$this->erpm->getvendorcontacts($vid);
		$data['brands']=$this->erpm->getbrandsforvendor($vid);
		$data['pos']=$this->erpm->getposforvendor($vid);
		$data['page']="vendor";
		$this->load->view("admin",$data);
	}

	function editvendor($vid=false)
	{
		if(!$vid)
			show_404();
		$user=$this->auth(PURCHASE_ORDER_ROLE);
		if($_POST)
			$this->erpm->do_updatevendor($vid);
		$data['contacts']=$this->erpm->getvendorcontacts($vid);
		$data['brands']=$this->erpm->getbrandsforvendor($vid);
		$data['vendor']=$this->erpm->getvendor($vid);
		$data['page']="addvendor";
		$this->load->view("admin",$data);
	}

	function addvendor()
	{
		$user=$this->auth(PURCHASE_ORDER_ROLE);
		if($_POST)
		{
			$this->erpm->do_addvendor();
			redirect("admin/vendors");
		}
		$data['page']="addvendor";
		$this->load->view("admin",$data);
	}

	function closepo($poid)
	{
		$user=$this->auth(PURCHASE_ORDER_ROLE);
		$this->db->query("update t_po_info set po_status=3 where po_id=? limit 1",$poid);
		redirect("admin/viewpo/$poid");
	}

	function assignadmintoticket($tid,$admin)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		$adminname=$this->db->query("select name from king_admin where id=?",$admin)->row()->name;
		$this->db->query("update support_tickets set status=1,updated_on=now(),assigned_to=? where ticket_id=? limit 1",array($admin,$tid));
		$this->erpm->addnotesticket($tid,2,0,"Ticket assigned to $adminname");
		redirect("admin/ticket/$tid");
	}

	function mailcheck()
	{
		$this->load->library("imap");
		$luid=$this->db->query("select im_uid from auto_readmail_uid order by id desc limit 1")->row_array();
		if(empty($luid))
			die("no starting uid specified");

		$luid=$luid['im_uid'];
		$this->imap->login("care@snapittoday.com","snap123rty");
		$nuid=$this->imap->is_newmsg($luid);
		if(!$nuid)
			die("no new mail");
		$mails=array();
		for($i=$luid+1;$i<=$nuid;$i++)
			$mails[]=$this->imap->readmail($i);
			
		//		$mails=array(array("subject"=>"asdasdasdtn1319888060 SNPCFN51153sdqsdasdSNPCFN53153"));
			
		foreach($mails as $m)
		{
			$ticket=array();
			$userid=0;
			$ticket_no=0;
			$transid="";
			if(empty($m))
				continue;
			//			preg_match("/SNP[[:alpha:]][[:alpha:]][[:alpha:]][[:digit:]][[:digit:]][[:digit:]][[:digit:]][[:digit:]]/i",$m['subject'],$matches);
			preg_match("/(TK\d{10})/i",$m['subject'],$matches);
			if(empty($matches))
				preg_match("/(TK\d{10})/i",$m['msg'],$matches);
			if(!empty($matches))
			{
				$ticket_no=substr($matches[0],2);
				if($this->db->query("select count(1) as l from support_tickets where ticket_no=? limit 1",$ticket_no)->row()->l==0)
					$ticket_no=0;
			}
			if($ticket_no==0)
			{
				preg_match("/(SNP\w{3}\d{5})/i",$m['subject'],$matches);
				if(empty($matches))
					preg_match("/(SNP\w{3}\d{5})/i",$m['msg'],$matches);
				if(!empty($matches))
					$transid=$matches[0];
			}
			$customer=$this->db->query("select userid from king_users where email=?",$m['from'])->row_array();
			if(!empty($customer))
				$userid=$customer['userid'];
			$msg=nl2br("SUBJECT\n-----------------------------------------------------\n".$m['subject']."\n\n\n\nEMAIL CONTENT\n-----------------------------------------------------\n").$m['msg'];
			$no=rand(1000000000,9999999999);
			if($ticket_no==0)
			{
				$this->db->query("insert into support_tickets(ticket_no,user_id,email,transid,created_on) values(?,?,?,?,now())",array($no,$userid,$m['from'],$transid));
				$tid=$this->db->insert_id();
			}
			else
			{
				$tid=$this->db->query("select ticket_id as id from support_tickets where ticket_no=?",$ticket_no)->row()->id;
				$this->db->query("update support_tickets set status=1 where ticket_no=? and assigned_to!=0 limit 1",$ticket_no);
				$this->db->query("update support_tickets set status=0 where ticket_no=? and assigned_to=0 limit 1",$ticket_no);
			}
			$this->erpm->addnotesticket($tid,1,0,$msg,1);
			$this->db->query("insert into auto_readmail_log(ticket_id,subject,msg,from,created_on) values(?,?,?,?,now())",array($tid,$m['subject'],$msg,$m['from']));
			if($ticket_no!=0)
				$this->erpm->addnotesticket($tid,0,1,"Status reset after reply mail from customer");
		}
		$this->db->query("insert into auto_readmail_uid(im_uid,time) values(?,now())",$nuid);
	}

	function voucher($vid=false)
	{
		if(!$vid)
			show_404();
		$user=$this->auth(FINANCE_ROLE);
		$data['page']="view_voucher";
		$data['voucher']=$this->db->query("select v.*,a.name as created_by from t_voucher_info v join king_admin a on a.id=v.created_by where v.voucher_id=?",$vid)->row_array();
		$data['expense']=$this->db->query("select * from t_voucher_expense_link where voucher_id=?",$vid)->row_array();
		$data['doc_po']=$this->db->query("select * from t_voucher_document_link where voucher_id=? and ref_doc_type=2",$vid)->result_array();
		$data['doc_grn']=$this->db->query("select * from t_voucher_document_link where voucher_id=? and ref_doc_type=1",$vid)->result_array();
		$this->load->view("admin",$data);
	}

	function vouchers($s=false,$e=false)
	{
		$user=$this->auth(FINANCE_ROLE);
		if($s!=false && $e!=false && (strtotime($s)<=0 || strtotime($e)<=0))
			show_404();
		$data['vouchers']=$this->erpm->getvouchers_date_range($s,$e);
		$data['page']="batch_list";
		if($e)
			$data['pagetitle']="between $s and $e";
		$data['page']="vouchers";
		$this->load->view("admin",$data);
	}

	function create_voucher_exp()
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
			$this->erpm->do_voucher_exp();
		$data['page']="create_voucher_exp";
		$this->load->view("admin",$data);
	}

	function create_voucher($grn="")
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
			$this->erpm->do_voucher();
		$r=array();
		$grns=$this->db->query("select v.vendor_name,v.vendor_id,v.city_name,g.* from t_grn_info g join m_vendor_info v on v.vendor_id=g.vendor_id where g.payment_status=1")->result_array();
		foreach($grns as $g)
		{
			$v=$g['vendor_name'].", ".$g['city_name'];
			if(!isset($r[$v]))
				$r[$v]=array();
			$g['amount']=$this->db->query("select sum(purchase_inv_value) as s from t_grn_invoice_link where grn_id=?",$g['grn_id'])->row()->s;
			$r[$v][]=$g;
		}
		$data['grns']=$r;
		$data['page']="create_voucher";
		$this->load->view("admin",$data);
	}

	function jx_assignticketadmins()
	{
		$q=$this->input->post("q");
		foreach($this->db->query("select * from king_admin where name like ? order by name asc","%$q%")->result_array() as $a)
			echo "<a href='javascript:void(0)' onclick='assign_admin({$a['id']},\"{$a['name']}\")'>{$a['name']}</a>";
	}

	function ticket_frag($id)
	{
		echo $this->db->query("select m.msg from support_tickets_msg m where m.id=?",$id)->row()->msg;
	}

	function ticket($tid)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		if($_POST)
		{
			$action=$this->input->post("action");
			if($action==0)
				$this->erpm->do_addnotesticket($tid);
			elseif($action==1)
			$this->erpm->do_changestatusticket($tid);
			elseif($action==2)
			$this->erpm->do_changetypeticket($tid);
			elseif($action==3)
			$this->erpm->do_changepriorityticket($tid);
			redirect("admin/ticket/$tid");
		}
		$data['ticket']=$this->erpm->getticket($tid);
		$data['msgs']=$this->erpm->getticketmsgs($tid);
		$data['page']="viewticket";
		$this->load->view("admin",$data);
	}

	function addticket()
	{
		$user=$this->auth(CALLCENTER_ROLE);
		if($_POST)
			$this->erpm->do_ticket();
		$data['page']="addticket";
		$this->load->view("admin",$data);
	}

	function support($filter="all",$s=false,$e=false)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		if($s!=false && $e!=false && (strtotime($s)<=0 || strtotime($e)<=0))
			show_404();
		$data['tickets']=$this->erpm->gettickets($filter,$s,$e);
		if($e)
			$data['pagetitle']="between $s and $e";
		$data['page']="tickets";
		$data['filter']=$filter;
		$this->load->view("admin",$data);
	}

	function jx_loadforvoucher()
	{
		$user=$this->auth();
		$type=$_POST['type'];
		$vendor=$_POST['vendor'];
		if($type==1)
		{
			$grns=$this->db->query("select grn_id,created_on from t_grn_info where vendor_id=? and payment_status=1",$vendor)->result_array();
			foreach($grns as $g)
			{
				$rpos=$this->db->query("select distinct(po_id) as po_id from t_grn_product_link where grn_id=?",$g['grn_id'])->result_array();
				$pos=array();
				foreach($rpos as $p)
					$pos[]=$p['po_id'];
				$g['pos']=implode(",",$pos);
				echo "<a href='javascript:void(0)' onclick='addgrn(\"{$g['grn_id']}\",0,\"{$g['created_on']}\",\"{$g['pos']}\")'>GRN{$g['grn_id']}</a>";
			}
			if(empty($grns))
				echo "no accounted grns available for selected vendor";
		}else{
			$pos=$this->db->query("select po_id,created_on from t_po_info where vendor_id=? and po_status=0",$vendor)->result_array();
			foreach($pos as $g)
				echo "<a href='javascript:void(0)' onclick='addpo(\"{$g['po_id']}\",0,\"{$g['created_on']}\")'>PO{$g['po_id']}</a>";
			if(empty($pos))
				echo "no opened POs available for selected vendor";
		}
	}

	function account_grn($grn)
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
		{
			$this->erpm->do_account_grn();
			redirect("admin/pending_pay_grns");
		}
		$grn_data=$this->db->query("select * from t_grn_info where grn_id=?",$grn)->row_array();
		if($grn_data['payment_status']!=0)
			show_error("This grn is already accounted");
		$data['page']="account_grn";
		$data['items']=$this->db->query("select i.*,p.product_name from t_grn_product_link i join m_product_info p on p.product_id=i.product_id where i.grn_id=?",$grn)->result_array();
		$data['invoices']=$this->db->query("select * from t_grn_invoice_link where grn_id=?",$grn)->result_array();
		$this->load->view("admin",$data);
	}

	function jx_getbrandsforvendor_json()
	{
		$user=$this->auth();
		$vid=$this->input->post("vid");
		$brands=$this->db->query("select b.name,b.id from m_vendor_brand_link v join king_brands b on b.id=v.brand_id where v.vendor_id=? order by b.name asc",$vid)->result_array();
		echo json_encode($brands);
	}

	function jx_getproductsforbrand()
	{
		$user=$this->auth();
		$vid=$this->input->post("vid");
		$bid=$this->input->post("bid");
		if($vid)
		{
			$data=$this->db->query("select p.is_sourceable as src,0 as orders,b.brand_margin as margin,ifnull(sum(s.available_qty),0) as stock,p.product_id as id,p.product_name as product,p.mrp from m_product_info p join m_vendor_brand_link b on b.brand_id=p.brand_id and b.vendor_id=? left outer join t_stock_info s on s.product_id=p.product_id where p.brand_id=? group by p.product_id order by p.product_name asc",array($vid,$bid))->result_array();
			$pids=array();
			foreach($data as $d)
				$pids[]=$d['id'];
			if(!empty($pids))
			{
				$orders=$this->db->query("select ifnull(sum(o.quantity*l.qty),0) as s,l.product_id from m_product_deal_link l join king_orders o on o.itemid=l.itemid where l.product_id in ('".implode("','",$pids)."') and o.time>".(time()-(24*60*60*90))." group by l.product_id")->result_array();
				foreach($orders as $o)
					foreach($data as $i=>$d)
					{
						if($d['id']==$o['product_id'])
							$data[$i]['orders']=$o['s'];
					}
			}
			echo json_encode($data);
		}
		else
			echo json_encode($this->db->query("select p.is_sourceable as src,ifnull(sum(o.quantity),0) as orders,b.brand_margin as margin,ifnull(sum(s.available_qty),0) as stock,p.product_id as id,p.product_name as product,p.mrp from m_product_info p join m_vendor_brand_link b on b.brand_id=p.brand_id left outer join king_orders o on o.itemid in (select itemid from m_product_deal_link where product_id=p.product_id) and o.time > ".(time()-(24*90*60*60)).". left outer join t_stock_info s on s.product_id=p.product_id where p.brand_id=? group by p.product_id order by p.product_name asc",array($bid))->result_array());
	}

	function jx_show_vendor_details()
	{
		$user=$this->auth();
		$vid=$this->input->post("v");
		$vendor=$this->db->query("select * from m_vendor_info where vendor_id=?",$vid)->row_array();
		$p=$this->db->query("select * from m_vendor_contacts_info where vendor_id=? order by id asc limit 1",$vid)->row_array();
		$brands=$this->db->query("select b.name from m_vendor_brand_link v join king_brands b on b.id=v.brand_id where v.vendor_id=? order by b.name asc",$vid)->result_array();
		$list_of_pos=$this->db->query("select count(*) as total_pos from t_po_info where vendor_id=?",$vid)->row()->total_pos;
		$previous_pos=$this->db->query("SELECT vendor_id,total_value,po_status,created_on FROM t_po_info WHERE vendor_id=? ORDER BY created_on DESC LIMIT 1",$vid)->row_array();

		echo "<h3 style='margin:0px;'>{$vendor['vendor_name']}, {$vendor['city_name']}</h3>";
		echo "<div>{$p['contact_name']}, {$p['contact_designation']}, Mobile : {$p['mobile_no_1']}, {$p['mobile_no_2']}, {$p['email_id_1']}</div>";
		echo "<h4 style='margin:0px;margin-top:5px;'>Brands supported</h4>";
		foreach($brands as $b)
			echo $b['name'].", ";
		echo "<h4 style='margin:0px;margin-top:5px;'>Total Pos:$list_of_pos</h4>";
		echo "<h3 style='margin:0px;margin-top:5px;'>Previous Po Details</h3>";
		echo "<div>
		<b>Value :</b>{$previous_pos['total_value']}</br>
		<b>Status :</b>{$this->po_status[$previous_pos['po_status']]}</br>
		<b>Created On :</b>{$previous_pos['created_on']}</br>
		</div>";
	}
	/*
	 * SELECT a.vendor_id,a.total_value,a.po_status,a.created_on,
	b.po_id,b.product_id,b.order_qty
	FROM t_po_info a
	JOIN t_po_product_link b ON b.po_id=a.po_id
	WHERE a.vendor_id=3
	ORDER BY a.created_on DESC
	LIMIT 18
	*/
	function viewpo($poid)
	{
		$user=$this->auth(PURCHASE_ORDER_ROLE);
		$data['page']="viewpo";
		$data['po']=$this->db->query("select po.*,v.vendor_name from t_po_info po join m_vendor_info v on v.vendor_id=po.vendor_id where po_id=?",$poid)->row_array();
		$data['items']=$this->db->query("select i.*,p.product_name from t_po_product_link i join m_product_info p on p.product_id=i.product_id where i.po_id=?",$poid)->result_array();
		$data['grns']=$this->db->query("select g.*,v.vendor_name,0 as value from t_grn_product_link gp join t_grn_info g on g.grn_id=gp.grn_id join m_vendor_info v on v.vendor_id=g.vendor_id where gp.po_id=? group by g.grn_id",$poid)->result_array();
		$data['vouchers']=$this->db->query("select v.*,t.adjusted_amount from t_voucher_document_link t join t_voucher_info v on v.voucher_id=t.voucher_id where t.ref_doc_id=? and t.ref_doc_type=2",$poid)->result_array();
		$this->load->view("admin",$data);
	}

	function pending_pay_grns()
	{
		$user=$this->auth(FINANCE_ROLE);
		$data['grns']=$this->erpm->getpendingpaygrns();
		$data['page']="pending_pays";
		$this->load->view("admin",$data);
	}

	function pending_grns()
	{
		$user=$this->auth(FINANCE_ROLE);
		$data['grns']=$this->erpm->getpendinggrns();
		$data['page']="pending_grns";
		$this->load->view("admin",$data);
	}

	function purchaseorders($s=false,$e=false)
	{
		$user=$this->auth(PURCHASE_ORDER_ROLE|FINANCE_ROLE);
		if($s!=false && $e!=false && (strtotime($s)<=0 || strtotime($e)<=0))
			show_404();
		$data['pos']=$this->erpm->getpos_date_range($s,$e);
		if($e)
			$data['pagetitle']="between $s and $e";
		$data['page']="viewpos";
		$this->load->view("admin",$data);
	}

	function jx_getpos()
	{
		$user=$this->auth();
		$rs=$this->db->query("select * from t_po_info where vendor_id=? and po_status!=2 and po_status!=3",$this->input->post("v"))->result_array();
		foreach($rs as $r)
			echo '<a href="javascript:void(0)" onclick="loadpo('.$r['po_id'].')">'.$r['remarks'].' : PO'.$r['po_id'].' ('.$r['created_on'].')</a>';
	}

	function jx_searchbrands()
	{
		$user=$this->auth();
		$rs=$this->db->query("select name,id from king_brands where name like ? order by name asc limit 20","%{$_POST['q']}%")->result_array();
		foreach($rs as $r)
			echo '<a href="javascript:void(0)" onclick="addbrand(\''.$r['name'].'\',\''.$r['id'].'\')">'.$r['name'].'</a>';
	}

	function jx_getbrandmargin()
	{
		$user=$this->auth();
		$v=$_POST['v'];
		$b=$_POST['b'];
		echo $this->db->query("select brand_margin from m_vendor_brand_link where vendor_id=? and brand_id=?",array($v,$b))->row()->brand_margin;
	}

	function jx_grn_load_po()
	{
		$user=$this->auth();
		$poid=$this->input->post("p");
		$pois=$this->erpm->getpoitemsforgrn($poid);
		echo json_encode($pois);
	}

	function add_storage_loc()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|PURCHASE_ORDER_ROLE);
		if($_POST)
			$this->erpm->do_add_storage_loc();
		$data['page']="add_storage_loc";
		$this->load->view("admin",$data);
	}

	function storage_locs()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|PURCHASE_ORDER_ROLE);
		$data['locs']=$this->erpm->getstoragelocs();
		$data['page']="storage_locations";
		$this->load->view("admin",$data);
	}

	function viewgrn($grn=false)
	{
		if(!$grn)
			show_404();
		$user=$this->auth(FINANCE_ROLE|STOCK_INTAKE_ROLE);
		$data['grn']=$this->db->query("select g.*,v.vendor_name from t_grn_info g join m_vendor_info v on v.vendor_id=g.vendor_id where g.grn_id=?",$grn)->row_array();
		if(empty($data['grn']))
			show_error("Error while retrieving");
		$data['prods']=$this->db->query("select p.product_name,po.po_id,g.* from t_grn_product_link g join t_po_info po on po.po_id=g.po_id join m_product_info p on p.product_id=g.product_id where g.grn_id=?",$grn)->result_array();
		$data['invoices']=$this->db->query("select * from t_grn_invoice_link where grn_id=?",$grn)->result_array();
		$data['vouchers']=$this->db->query("select v.voucher_value,td.*,a.name as created_by from t_voucher_document_link td join t_voucher_info v on v.voucher_id=td.voucher_id join king_admin a on a.id=v.created_by where td.ref_doc_type=1 and td.ref_doc_id=?",$grn)->result_array();
		$data['page']="view_grn";
		$this->load->view("admin",$data);
	}

	function product_price_changelog($s='',$e='')
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|CALLCENTER_ROLE);
		if(empty($e))
		{
			$s=time()-(30*24*60*60);
			$e=time();
		}else{
			$s=strtotime($s);
			$e=strtotime($e)+(24*60*60);
		}
		$data['s']=$s;
		$data['e']=$e;
		$data['prods']=$this->db->query("select p.product_name,pc.*,a.name as created_by from product_price_changelog pc join m_product_info p on p.product_id=pc.product_id left outer join king_admin a on a.id=pc.created_by where pc.created_on between $s and $e order by pc.id desc")->result_array();
		$data['page']="product_price_changelog";
		$this->load->view("admin",$data);
	}

	function pnh_version_price_change($v=0,$export=0)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		if(!$v)
		{
			$v_raw=$this->db->query("select id from pnh_app_versions order by id desc limit 1")->row_array();
			if(empty($v_raw))
				show_error("No app versions defined");
			$v=$v_raw['id'];
		}
		$data['ver']=$ver=$this->db->query("select * from pnh_app_versions where id=?",$v)->row_array();
		$data['deals']=$deals=$this->erpm->pnh_getdealpricechange($v);
		$data['page']="pnh_version_price_change";
		if($export)
			$this->erpm->export_csv("version_price_change_version:{$ver['version_no']}",$deals);
		$this->load->view("admin",$data);
	}

	function deal_price_changelog($s='',$e='')
	{
		$user=$this->auth(DEAL_MANAGER_ROLE|CALLCENTER_ROLE);
		if(empty($e))
		{
			$s=time()-(30*24*60*60);
			$e=time();
		}else{
			$s=strtotime($s);
			$e=strtotime($e)+(24*60*60);
		}
		$data['s']=$s;
		$data['e']=$e;
		$data['deals']=$this->db->query("select p.name,pc.*,a.name as created_by from deal_price_changelog pc join king_dealitems p on p.id=pc.itemid left outer join king_admin a on a.id=pc.created_by where pc.created_on between $s and $e order by pc.id desc")->result_array();
		$data['page']="deal_price_changelog";
		$this->load->view("admin",$data);
	}

	function stock_intake_list($vid=0,$s=0,$e=0)
	{
		$this->auth(STOCK_INTAKE_ROLE);
		$pgt="";
		if($s==0)
		{
			$s=date("Y-m-d",mktime(0,0,0,date("n"),0));
			$e=date("Y-m-d");
			//			$pgt="Recent ";
		}
		//		else
		$pgt.="Stock Intakes between $s and $e";
		if($vid)
			$pgt.=" for Vendor:".$this->db->query("select vendor_name from m_vendor_info where vendor_id=?",$vid)->row()->vendor_name;
		$e="$e 23:59:59";
		$data['s']=$s;
		$data['e']=$e;
		$sql="select v.vendor_name,g.*,sum(p.invoice_qty) as invoiced,sum(p.received_qty) as received,count(p.product_id) as items,sum(i.purchase_inv_value) as invoice_value from t_grn_info g join m_vendor_info v on v.vendor_id=g.vendor_id join t_grn_product_link p on p.grn_id=g.grn_id left outer join t_grn_invoice_link i on i.grn_id=g.grn_id where 1";
		if($vid!=0)
			$sql.=" and g.vendor_id=$vid ";
		$sql.=" and g.created_on between '$s' and '$e' group by g.grn_id order by g.created_on desc";
		$data['grns']=$this->db->query($sql)->result_array();
		$data['page']="stock_intake_list";
		$data['pagetitle']=$pgt;
		$this->load->view("admin",$data);
	}

	function apply_grn($poid="")
	{
		$poid="";
		$user=$this->auth(STOCK_INTAKE_ROLE);
		if($_POST)
		{
			$this->erpm->do_grn();
			$this->session->set_flashdata("grn_done","yes");
			redirect("admin/apply_grn");
		}
		if(!empty($poid))
		{
			$data['po']=$this->erpm->getpo($poid);
			$data['po_items']=$this->erpm->getpoitemsforgrn($poid);
		}
		$r=array();
		$pos=$this->db->query("select v.vendor_name,v.city_name,t.* from t_po_info t join m_vendor_info v on v.vendor_id=t.vendor_id where t.po_status!=2 and t.po_status!=3 order by v.vendor_name asc",$this->input->post("v"))->result_array();
		foreach($pos as $po)
		{
			$v=$po['vendor_name'].", ".$po['city_name'];
			if(!isset($r[$v]))
			 $r[$v]=array();
			$r[$v][]=$po;
		}
		$data['pos']=$r;
		$data['page']="apply_grn";
		$this->load->view("admin",$data);
	}

	function po_product()
	{
		$user=$this->auth(PURCHASE_ORDER_ROLE);
		if($_POST)
			$this->erpm->do_po_prodwise();
		$data['page']="po_product";
		$this->load->view("admin",$data);
	}

	function purchaseorder()
	{
		$user=$this->auth(PURCHASE_ORDER_ROLE);
		if($_POST)
		{
			$this->erpm->createpo();
			redirect("admin/purchaseorders");
		}
		$data['vendors']=$this->erpm->getvendors();
		$data['page']="purchaseorder";
		$this->load->view("admin",$data);
	}

	function searchvendor()
	{
		$user=$this->auth();
		$res=$this->erpm->searchvendors($_POST['q']);
	}

	function jx_productdetails()
	{
		$user=$this->auth();
		$id=$this->input->post("id");
		echo json_encode($this->erpm->getproductdetails($id));
	}

	function jx_load_unavail_products()
	{
		$sql="select p.product_id,o.transid,p.purchase_cost,sum(s.available_qty) as available,b.id as brandid,b.name as brand,p.product_name,p.mrp,sum(o.quantity*l.qty) as qty from king_orders o join m_product_deal_link l on l.itemid=o.itemid join m_product_info p on p.product_id=l.product_id join king_brands b on b.id=p.brand_id left outer join t_stock_info s on s.product_id=l.product_id where o.status=0";
		$sql.=" group by l.product_id having sum(o.quantity*l.qty)>available or available is null order by p.product_name asc";
		$data['reports']=$reports=$this->db->query($sql)->result_array();
		foreach($reports as $i=>$r)
		{
			$vendors=array();
			$rvendors=$this->db->query("select v.vendor_name,v.vendor_id from m_vendor_info v join m_vendor_brand_link b on v.vendor_id=b.vendor_id where b.brand_id=?",$r['brandid'])->result_array();
			$vendors=array();
			foreach($rvendors as $v)
				$vendors[]=$v;
			$reports[$i]['vendors']=$vendors;
		}
		echo json_encode($reports);
	}

	function jx_searchproducts()
	{
		$user=$this->auth();
		$q=$this->input->post("q");
		$res=$this->erpm->searchproducts($q);
		foreach($res as $r)
			echo "<a href='javascript:void(0)' onclick='addproduct(\"{$r['product_id']}\",\"".htmlspecialchars($r['product_name'],ENT_QUOTES)."\",\"{$r['mrp']}\")'>{$r['product_name']}  <span style='color:red'>(stock:{$r['stock']})</span>  <span style='color:#ff9900'>(Mrp:Rs{$r['mrp']})</span></a>";
	}

	function jx_searchproductsfordeal()
	{
		$user=$this->auth();
		$q=$this->input->post("q");
		$res=$this->erpm->searchproductsfordeal($q);
		foreach($res as $r)
			echo "<a href='javascript:void(0)' onclick='addproduct".($_POST['type']=="group"?"g":"")."(\"{$r['product_id']}\",\"".htmlspecialchars($r['product_name'],ENT_QUOTES)."\",\"{$r['mrp']}\")'>{$r['product_name']}  <span style='color:red'>(stock:{$r['stock']})</span>  <span style='color:#ff9900'>(Mrp:Rs{$r['mrp']})</span></a>";
	}

	function jx_search_deals()
	{
		$user=$this->auth();
		$q=$this->input->post("q");
		$res=$this->db->query("select id,name from king_dealitems where name like ? order by name","%$q%")->result_array();
		foreach($res as $r)
			echo "<a href='javascript:void(0)' onclick='adddealitem(\"{$r['id']}\",\"".htmlspecialchars($r['name'])."\")'>{$r['name']}</a>";
	}

	function stock_unavail_report($partial=0,$s=0,$e=0,$is_pnh=0,$export=0)
	{
		$user=$this->auth(PURCHASE_ORDER_ROLE);
		$sql="select group_concat(distinct o.transid) as transid,p.purchase_cost,sum(s.available_qty) as available,b.id as brandid,b.name as brand,p.product_name,p.mrp,sum(o.quantity*l.qty) as qty from king_orders o join king_transactions t on t.transid=o.transid left outer join m_product_deal_link l on l.itemid=o.itemid left outer join products_group_orders og on og.order_id=o.id left outer join m_product_info p on p.product_id=ifnull(l.product_id,og.product_id) join king_brands b on b.id=p.brand_id join t_stock_info s on s.product_id=p.product_id where o.status=0";
		if($is_pnh==1)
			$sql.=" and t.is_pnh=1";
		elseif($is_pnh==2)
		$sql.=" and t.is_pnh=0";
		$data['from']=0;
		$data['to']=0;
		$data['is_pnh']=$is_pnh;
		$data['partial']=0;
		if($e!=0)
		{
			list($sy,$sm,$sd)=explode("-",$s);
			list($ey,$em,$ed)=explode("-",$e);
			$si=mktime(0,0,0,$sm,$sd,$sy);
			$ei=mktime(23,59,59,$em,$ed,$ey);
			$sql.=" and o.time between $si and $ei";
			$data['from']=$s;
			$data['to']=$e;
		}
		$sql.=" group by s.product_id having sum(o.quantity*ifnull(l.qty,1))>sum(s.available_qty) order by p.product_name,o.sno asc";
		$data['reports']=$reports=$this->db->query($sql)->result_array();
		foreach($reports as $i=>$r)
		{
			$vendors=array();
			$rvendors=$this->db->query("select v.vendor_name from m_vendor_info v join m_vendor_brand_link b on v.vendor_id=b.vendor_id where b.brand_id=?",$r['brandid'])->result_array();
			$vendors=array();
			foreach($rvendors as $v)
				$vendors[]=$v['vendor_name'];
			$reports[$i]['vendors']=implode(", ",array_unique($vendors));
		}
		$data['reports']=$reports;
		if($export)
		{
			ob_start();
			$f=fopen("php://output","w");
			fputcsv($f, array("Product Name","MRP","Purchase Cost","Required Quantity","Brand","Vendors","Order"));
			foreach($reports as $r)
				fputcsv($f, array($r['product_name'],$r['mrp'],$r['purchase_cost'],$r['qty']-$r['available'],$r['brand'],$r['vendors'],$r['transid']));
			fclose($f);
			$csv=ob_get_clean();
			ob_clean();
			header('Content-Description: File Transfer');
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename='.("stock_unavailability_".date("d_m_y").".csv"));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . strlen($csv));
			ob_clean();
			flush();
			echo $csv;
			exit;
		}
		$this->load->view("admin/body/stock_unavail_report",$data);
	}

	function partial_shipment()
	{
		$user=$this->auth(ORDER_BATCH_PROCESS_ROLE);
		$data['orders']=$this->erpm->getpartialshipments();
		$data['pagetitle']="Partial Shipment";
		$data['page']="orders";
		$data['partial_list']=true;
		$this->load->view("admin",$data);
	}

	function disabled_but_possible_shipment()
	{
		$user=$this->auth(ORDER_BATCH_PROCESS_ROLE);
		$data['orders']=$this->erpm->getdisabledbutpossibleshipments();
		$data['pagetitle']="Disabled but possible Shipments";
		$data['page']="orders";
		$data['partial_list']=true;
		$this->load->view("admin",$data);
	}

	function order_summary($s=false,$e=false)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		if(!$s)
			$e=$s=date("Y-m-d");
		$from=strtotime($s);
		$to=strtotime("23:59:59 $e");
		$r_orders=$this->db->query("select t.amount,p.brand_id,p.product_id,o.time,o.transid,i.name as deal,i.id as itemid,p.product_name,sum(s.available_qty) as stock,i.price from king_orders o join king_dealitems i on i.id=o.itemid join king_transactions t on t.transid=o.transid left outer join m_product_deal_link l on l.itemid=i.id left outer join products_group_orders po on po.order_id=o.id left outer join m_product_info p on p.product_id=ifnull(l.product_id,po.product_id) left outer join t_stock_info s on s.product_id=ifnull(p.product_id,po.product_id) where o.time between $from and $to group by o.transid,p.product_id order by o.time desc")->result_array();
		$order=array();
		$deal=array();
		foreach($r_orders as $o)
		{
			if(!isset($order[$o['transid']]))
				$order[$o['transid']]=array();
			if(!isset($deal[$o['transid']."-".$o['itemid']]))
			{
				$deal[$o['transid']."-".$o['itemid']]=array();
				$order[$o['transid']][]=$o;
			}
			$deal[$o['transid']."-".$o['itemid']][]=$o;
		}
		$data['s']=date("d/m/y",$from);
		$data['e']=date("g:ia d/m/y",$to);
		$data['orders']=$order;
		$data['deal']=$deal;
		$data['page']="order_summary";
		$this->load->view("admin",$data);
	}

	function orders($status=0,$s=false,$e=false)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		if($s!=false && $e!=false && (strtotime($s)<=0 || strtotime($e)<=0))
			show_404();
		$data['pagetitle']="";
		$data['orders']=$this->erpm->getordersbytransaction_date_range($status,$s,$e);
		if($status==1)
		{
			$data['pagetitle']="Pending ";
			$data['pending']=true;
		}
		if($e)
			$data['pagetitle'].="between $s and $e";
		$data['page']="orders";
		$this->load->view("admin",$data);
	}

	function changeshipaddr($transid=false)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		if(empty($transid))
			redirect("admin/orders");
		$this->erpm->do_changetranshipaddr($transid);
		redirect("admin/trans/$transid");
	}

	function changebilladdr($transid=false)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		if(empty($transid))
			redirect("admin/orders");
		$this->erpm->do_changetranbilladdr($transid);
		redirect("admin/trans/$transid");
	}

	function setprioritytrans($transid)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		$msg=$this->input->post("msg");
		$this->db->query("update king_transactions set priority=1,priority_note=? where transid=?",array($msg,$transid));
		$cmsg="High priority assigned<br>Msg : $msg";
		$this->erpm->do_trans_changelog($transid,$cmsg);
	}

	function cancel_orders()
	{
		$user=$this->auth(true);
		$r=0;
		if(empty($_POST))
			show_error("where is the milk for cookie?");
		if($this->input->post("refund"))
			$this->erpm->do_cancel_orders();
		foreach(array("transid","oids") as $i)
			$$i=$this->input->post("$i");
		foreach($this->db->query("select (i_price-i_coup_discount)*quantity as i_price from king_orders where id in ('".implode("','",$oids)."')")->result_array() as $o)
			$r+=$o['i_price'];
		$data['user']=$this->db->query("select u.name from king_orders o join king_users u on u.userid=o.userid where o.transid=?",$transid)->row()->name;
		$data['refund']=$r;
		$data['page']="cancel_order";
		$this->load->view("admin",$data);
	}

	function stock_correction()
	{
		$user=$this->auth(true);
		if(empty($_POST))
			die;
		foreach(array("pid","msg","corr","type") as $i)
			$$i=$this->input->post("$i");
		$p=$this->db->query("select * from m_product_info where product_id=?",$pid)->row_array();
		if($this->db->query("select 1 from t_stock_info where product_id=?",$pid)->num_rows()==0)
			show_error("No record for any stock for this product");
		$check_mrp=false;
		if($this->db->query("select 1 from t_stock_info where product_id=? and mrp=?",array($p['product_id'],$p['mrp']))->num_rows()==1)
			$check_mrp=true;
		$sql="update t_stock_info set available_qty=available_qty".($type==1?"+":"-")."? where product_id=?";
		if($check_mrp)
			$sql.=" and mrp=?";
		$sql.=" limit 1";
		$this->db->query($sql,array($corr,$pid,$p['mrp']));
		$this->erpm->do_stock_log($type,$corr,$pid,"",false,false);
		$this->db->query("update t_stock_update_log set msg=? where id=? limit 1",array($msg,$this->db->insert_id()));
		$this->erpm->flash_msg("Stock corrected");
		redirect("admin/product/$pid");
	}

	function cancel_invoice($invno="")
	{
		$user=$this->auth(INVOICE_PRINT_ROLE);
		$invoice=$this->db->query("select transid,order_id,invoice_no from king_invoice where invoice_no=? and invoice_status=1",$invno)->result_array();
		if(empty($invno) || empty($invoice))
			show_error("Invoice not found or Invoice already cancelled");
		$transid=$invoice[0]['transid'];
		$oids=array();
		foreach($invoice as $i)
			$oids[]=$i['order_id'];
		$orders=$this->db->query("select quantity as qty,itemid,id from king_orders where id in ('".implode("','",$oids)."')")->result_array();
		foreach($orders as $o)
		{
			$pls=$this->db->query("select qty,pl.product_id,p.mrp from m_product_deal_link pl join m_product_info p on p.product_id=pl.product_id where itemid=?",$o['itemid'])->result_array();
			$pls2=$this->db->query("select pl.qty,p.product_id,p.mrp from king_orders o join products_group_orders pgo on pgo.order_id=o.id join m_product_group_deal_link pl join m_product_info p on p.product_id=pgo.product_id where o.id=?",$o['id'])->result_array();
			$pls=array_merge($pls,$pls2);
			foreach($pls as $p)
			{
				$check_mrp=false;
				if($this->db->query("select 1 from t_stock_info where product_id=? and mrp=?",array($p['product_id'],$p['mrp']))->num_rows()==1)
					$check_mrp=true;
				$sql="update t_stock_info set available_qty=available_qty+? where product_id=?";
				if($check_mrp)
					$sql.=" and mrp=?";
				$sql.=" limit 1";
				$this->db->query($sql,array($p['qty']*$o['qty'],$p['product_id'],$p['mrp']));
				$this->erpm->do_stock_log(1,$p['qty']*$o['qty'],$p['product_id'],$this->db->query("select id from king_invoice where order_id=?",$o['id'])->row()->id,false,true);
			}
			$this->db->query("update t_imei_no set status=0 where order_id=?",$o['id']);
		}
		$this->db->query("update king_orders set status=0 where id in ('".implode("','",$oids)."') and transid=?",$transid);
		$this->db->query("update king_invoice set invoice_status=0 where invoice_no=?",$invno);
		$this->db->query("update proforma_invoices set invoice_status=0 where p_invoice_no=?",$this->db->query("select p_invoice_no as i from shipment_batch_process_invoice_link where invoice_no=?",$invno)->row()->i);
		$this->erpm->do_trans_changelog($transid,"Invoice no $invno cancelled");
		$this->session->set_flashdata("erp_pop_info","Invoice cancelled");
		$bid=$this->db->query("select batch_id from shipment_batch_process_invoice_link where invoice_no=?",$invno)->row()->batch_id;
		$c=$this->db->query("select count(1) as l from shipment_batch_process_invoice_link where packed=1 and shipped=1 and shipped=1 and batch_id=$bid")->row()->l;
		if($this->db->query("select p_invoice_no as n from shipment_batch_process_invoice_link where invoice_no=?",$invno)->row()->n!=0)
			$c+=$this->db->query("select count(1) as l from shipment_batch_process_invoice_link bi join proforma_invoices i on i.p_invoice_no=bi.p_invoice_no where bi.batch_id=$bid and bi.packed=0 and i.invoice_status=0")->row()->l;
		if($this->db->query("select count(1) as l from shipment_batch_process_invoice_link where batch_id=?",$bid)->row()->l<=$c)
			//		if($this->db->query("select count(1) as l from shipment_batch_process_invoice_link bi join proforma_invoices i on i.p_invoice_no=bi.p_invoice_no where bi.batch_id=? and bi.packed=0 and i.invoice_status=1",$bid)->row()->l==0)
			$this->db->query("update shipment_batch_process set status=2 where batch_id=? limit 1",$bid);
		redirect("admin/trans/$transid");
	}

	function mark_c_refund($rid="",$from_pending=false)
	{
		$user=$this->auth(true);
		$this->db->query("update t_refund_info set status=1,modified_on=?,modified_by=? where refund_id=?",array(time(),$user['userid'],$rid));
		$r=$this->db->query("select r.amount,t.is_pnh,r.transid,t.franchise_id from t_refund_info r join king_transactions t on t.transid=r.transid where r.refund_id=?",$rid)->row_array();
		if($r['is_pnh']!=0)
		{
			$this->erpm->pnh_fran_account_stat($r['franchise_id'],0,$r['amount'],"Refund - Order {$r['transid']} cancelled");
			$this->erpm->do_trans_changelog($r['transid'],"Amount of Rs {$r['amount']} credited back to franchise");
			$this->erpm->pnh_sendsms($this->db->query("select login_mobile1 as m from pnh_m_franchise_info where franchise_id=?",$r['franchise_id'])->row()->m,"Refund of Rs {$r['amount']} against Order: {$r['transid']} is credited to your account",$r['franchise_id']);
		}
		if(!$from_pending)
			redirect("admin/trans/".$this->db->query("select transid from t_refund_info where refund_id=?",$rid)->row()->transid);
		redirect("admin/pending_refunds_list");
	}

	function change_qy_order($transid)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		$refund=$this->input->post("nc_refund");
		$oid=$this->input->post("nc_oid");
		$qty=$this->input->post("nc_qty");
		$n_oid=random_string("numeric",10);
		$order=$this->db->query("select * from king_orders where id=?",$oid)->row_array();
		$prod=$this->db->query("select name from king_dealitems where id=?",$order['itemid'])->row_array();
		$n_qty=$order['quantity']-$qty;
		$inp=array($n_oid,$transid,$order['userid'],$order['itemid'],$order['vendorid'],$order['brandid'],$n_qty,3,$order['i_orgprice'],$order['i_price'],$order['i_nlc'],$order['i_phc'],$order['i_tax'],$order['i_discount'],$order['i_coup_discount'],$order['i_discount_applied_on'],$order['time'],time());
		$this->db->query("insert into king_orders(id,transid,userid,itemid,vendorid,brandid,quantity,status,i_orgprice,i_price,i_nlc,i_phc,i_tax,i_discount,i_coup_discount,i_discount_applied_on,time,actiontime) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",$inp);
		$this->db->query("update king_orders set quantity=? where id=? limit 1",array($qty,$oid));
		$this->erpm->do_trans_changelog($transid,"Order '{$prod['name']}' quantity changed from {$order['quantity']} to {$qty}");
		$this->db->query("insert into t_refund_info(transid,amount,status,created_on,created_by) values(?,?,?,?,?)",array($transid,$refund,0,time(),$user['userid']));
		$rid=$this->db->insert_id();
		$this->db->query("insert into t_refund_order_item_link(refund_id,order_id,qty) values(?,?,?)",array($rid,$n_oid,$n_qty));
		redirect("admin/trans/{$transid}");
	}

	function endisable_for_batch($transid)
	{
		$user=$this->auth(true);
		$flag=1;
		if($this->db->query("select batch_enabled from king_transactions where transid=?",$transid)->row()->batch_enabled==1)
			$flag=0;
		$this->db->query("update king_transactions set batch_enabled=$flag where transid=? limit 1",$transid);
		$this->erpm->do_trans_changelog($transid,"Order ".($flag?"ENABLED":"DISABLED")." for batch process");
		redirect("admin/trans/$transid");
	}

	function bulk_endisable_for_batch()
	{
		$this->auth(ORDER_BATCH_PROCESS_ROLE);
		foreach(array("enable","disable","trans") as $i)
			$$i=$this->input->post("$i");
		if(empty($trans))
			show_error("No Orders selected");
		$flag=1;
		if($disable)
			$flag=0;
		$filter=$this->db->query("select transid,batch_enabled from king_transactions where transid in ('".implode("','",$trans)."') and batch_enabled!=$flag")->result_array();
		$trans=array();
		foreach($filter as $f)
			$trans[]=$f['transid'];
		foreach($trans as $transid)
		{
			$this->db->query("update king_transactions set batch_enabled=$flag where transid=? limit 1",$transid);
			$this->erpm->do_trans_changelog($transid,"Order ".($flag?"ENABLED":"DISABLED")." for batch process");
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function resend_mails($transid)
	{
		if($this->input->post("order"))
			$this->resend_order_confirmation_mail($transid);
		else
			$this->resend_shipment_mail($transid);
	}

	private function resend_shipment_mail($transid)
	{
		$user=$this->auth();
		$email=$this->input->post("email");
		$mdata['prods']=$os=$this->db->query("select inv.transid,i.name,o.quantity,o.medium,o.shipid,o.id from king_invoice inv join king_orders o on o.id=inv.order_id join king_dealitems i on i.id=o.itemid where inv.transid=? order by i.name asc",$transid)->result_array();
		if(empty($os))
			show_error("No shipments were made for this order");
		$mdata['transid']=$transid=$os[0]['transid'];
		$mdata['medium']=$os[0]['medium'];
		$mdata['trackingid']=$os[0]['shipid'];
		$oid=$os[0]['id'];
		$partial=false;
		if($this->db->query("select count(1) as l from king_orders where status!=2 and transid=?",$transid)->row()->l!=0)
			$partial=true;
		$mdata['partial']=$partial;
		$payload=$this->db->query("select u.name,o.bill_email,o.ship_email from king_orders o join king_users u on u.userid=o.userid where o.id=?",$oid)->row_array();
		$mdata['name']=$payload['name'];
		$msg=$this->load->view("mails/shipment",$mdata,true);
		$this->vkm->email(array($email,$payload['bill_email'],$payload['ship_email']),"Your order is shipped",$msg,true);
		$this->session->set_flashdata("erp_pop_info","Shipment mail resent successfully");
		redirect("admin/trans/$transid");
	}

	private function resend_order_confirmation_mail($transid)
	{
		$user=$this->auth();
		$email=$this->input->post("email");

		$orders=$this->db->query("select * from king_orders where transid=?",$transid)->result_array();

		$umail='<table width="100%" border=1 cellspacing=0 cellpadding=7 style="font-size:inherit;margin-top:15px;"><tr><th>Product</th><th>Unit Price</th><th>Quantity</th><th>Price</th></tr>';
		$mrp_total=0;
		$m_items=array();
		$total_discount = 0;
		foreach($orders as $order)
		{
			$m_item=$this->db->query("select orgprice,price,name from king_dealitems where id=?",$order['itemid'])->row_array();
			$itemname=$m_item['name'];
			$t=$m_item['price']*$order['quantity'];
			$m=$m_item['orgprice']*$order['quantity'];
			$total_discount+=($order['i_coup_discount']+$order['i_discount'])*$order['quantity'];
			$umail.="<tr><td>{$itemname}</td><td>{$m_item['orgprice']}</td><td>{$order['quantity']}</td><td>{$m}</td></tr>";
			$t_tot+=$t;
			$mrp_total+=$m;
			$m_items[]=$m_item;
		}
		$m_trans=$this->db->query("select * from king_transactions where transid=?",$transid)->row_array();
		$t_amount=$this->db->query("select amount from king_transactions where transid=?",$transid)->row()->amount;
		$umail.="<tr><td colspan=3 align='right'>Discount</td><td>".floor($total_discount)."</td></tr>";
		if($giftwrap_order)
			$umail.="<tr><td colspan=3 align='right'>GiftWrap Charges</td><td>".GIFTWRAP_CHARGES."</td></tr>";
		$umail.="<tr><td colspan=3 align='right'>Handling/Shipping/COD charges</td><td>".($m_trans['cod']+$m_trans['ship'])."</td></tr>";
		$umail.="<tr><td colspan=3 align='right'>Total</td><td>Rs {$t_amount}</td></tr>";
		$umail.="</table>";

		$msg=$this->load->view("mails/order",array("transid"=>$transid,"umail"=>$umail),true);
		$this->vkm->email(array($order['ship_email'],$order['bill_email'],$email),"Your order details : $transid",$msg);
		$this->session->set_flashdata("erp_pop_info","Order confirmation mail resent successfully");
		redirect("admin/trans/$transid");
	}

	function reship_order()
	{
		$user=$this->auth(true);
		if(empty($_POST))
			show_error("No milk for cookie");
		$this->erpm->do_reship_order();
	}

	function trans($transid="")
	{
		if(empty($transid))
			show_404();
		$user=$this->auth(CALLCENTER_ROLE|FINANCE_ROLE|PNH_EXECUTIVE_ROLE);
		if($_POST)
			$this->erpm->do_addtransmsg($transid);
		$data['tran']=$tran=$this->erpm->gettransaction($transid);
		if(empty($tran))
			show_error("Transaction not found");
		$data['orders']=$this->erpm->getordersfortransid($transid);
		if(empty($data['orders']))
			redirect("admin/callcenter/trans/{$transid}");
		$data['batch']=$this->erpm->getbatchesstatusfortransid($transid);
		$data['tickets']=$this->erpm->getticketsfortrans($transid);
		$data['changelog']=$this->erpm->gettranschangelog($transid);
		$data['freesamples']=$this->erpm->getfreesamplesfortransaction($transid);
		$data['refunds']=$this->db->query("select * from t_refund_info where transid=?",$transid)->result_array();
		$data['page']="transaction_orders";
		$this->load->view("admin",$data);
	}

	function pending_refunds_list()
	{
		$user=$this->auth(true);
		$data['refunds']=$this->db->query("select * from t_refund_info where status=0 order by refund_id asc")->result_array();
		$data['page']="pending_refunds_list";
		$this->load->view("admin",$data);
	}

	function getvendorproducts()
	{
		$vid=$this->input->post("v");
		$q=$this->input->post("q");
		$res=$this->erpm->searchvendorproducts($vid,$q);
		foreach($res as $r)
			echo "<a href='javascript:void(0)' onclick='addproduct(\"{$r['product_id']}\",\"".htmlspecialchars($r['product_name'],ENT_QUOTES)."\",\"{$r['mrp']}\",\"{$r['margin']}\")'>{$r['product_name']} <span style='color:red'>(stock: {$r['stock']})</span> <span style='color:#ff9900;'>(Mrp:Rs {$r['mrp']})</span></a>";
	}

	function pnh_investor_report($fid=0)
	{
		$user=$this->auth(INVESTOR_ROLE);
		$data['report']=$this->erpm->getpnhreport($fid);
		$data['page']="pnh_investor_report";
		$this->load->view("admin",$data);
	}

	function investor_report()
	{
		$user=$this->auth(INVESTOR_ROLE);
		$data['report']=$this->erpm->getinvestorreport();
		$data['page']="investor_report";
		$this->load->view("admin",$data);
	}

	function changepassword()
	{
		$user=$this->auth();
		if($_POST)
			$this->erpm->do_changepwd();
		$data['page']="changepwd";
		$this->load->view("admin",$data);
	}

	function searchproducts()
	{
		$user=$this->auth();
		$q=$this->db->query("select p.*,sum(s.available_qty) as stock from m_product_info p left outer join t_stock_info s on s.product_id=p.product_id where p.product_name like ? order by p.product_name group by p.product_id","%{$_POST['q']}%")->result_array();
		foreach($q as $r)
			echo "<a href='javascript:void(0)' onclick='addproduct(\"{$r['product_id']}\",\"".htmlspecialchars($r['product_name'],ENT_QUOTES)."\",\"{$r['mrp']}\",\"{$r['vat']}\")'>{$r['product_name']}  <span style='color:red'>(stock:{$r['stock']})</span>  <span style='color:#ff9900;'>(Mrp: Rs{$r['mrp']})</span></a>";
	}

	function add_batch_process()
	{
		$user=$this->auth(ORDER_BATCH_PROCESS_ROLE);
		if(!$_POST)
			redirect("batch_process");
		$bid=$this->erpm->do_shipment_batch_process();
		if(!$bid)
			show_error("INSUFFICIENT STOCK TO PROCESS ANY ORDER");
		redirect("admin/batch/$bid");
	}

	function add_courier()
	{
		$user=$this->auth(OUTSCAN_ROLE|INVOICE_PRINT_ROLE);
		if($_POST)
			$this->erpm->do_addcourier();
		$data['page']="add_courier";
		$this->load->view("admin",$data);
	}

	function edit_pincodes($cid=false)
	{
		$user=$this->auth(OUTSCAN_ROLE|INVOICE_PRINT_ROLE);
		if(!$cid)
			show_404();
		if($_POST)
		{
			$this->erpm->do_updatepincodesforcourier($_POST['courier_id'],$_POST['pincodes']);
			redirect("admin/courier");
		}
		$data['courier_id']=$cid;
		$data['pincodes']=$this->erpm->getpincodesforcourier($cid);
		$data['page']="edit_pincodes";
		$this->load->view("admin",$data);
	}

	function courier()
	{
		$user=$this->auth(OUTSCAN_ROLE|INVOICE_PRINT_ROLE|ORDER_BATCH_PROCESS_ROLE);
		$data['couriers']=$this->erpm->getcouriers();
		$data['page']="couriers";
		$this->load->view("admin",$data);
	}

	function update_awb($courier_id)
	{
		$user=$this->auth(OUTSCAN_ROLE|INVOICE_PRINT_ROLE);
		if($_POST)
		{
			$this->erpm->do_updateawb($courier_id);
			redirect("admin/courier");
		}
		$data['cid']=$courier_id;
		$data['awb']=$this->db->query("select * from m_courier_awb_series where courier_id=?",$courier_id)->row_array();
		$data['page']="update_awb";
		$this->load->view("admin",$data);
	}

	function update_ship_kfile()
	{
		$user=$this->auth(KFILE_ROLE);
		if($_POST)
			$this->erpm->do_update_ship_kfile();
		$data['page']="update_kfile";
		$this->load->view("admin",$data);
	}

	function edit_partner($pid)
	{
		$this->add_partner($pid);
	}

	function add_partner($pid=false)
	{
		$user=$this->auth(true);
		if($pid)
			$data['partner']=$this->db->query("select * from partner_info where id=?",$pid)->row_array();
		if($_POST)
		{
			//			$this->erpm->debug_post();
			foreach(array("name","trans_prefix","trans_mode") as $i)
				$inp[$i]=$this->input->post($i);
			if(strlen($inp['trans_prefix'])!=3)
				show_error("Transaction prefix should be 3 characters to be exact");
			$inp['trans_prefix']=strtoupper($inp['trans_prefix']);
			if(!$this->input->post("pid"))
			{
				$inp['created_by']=$user['userid'];
				$this->db->insert("partner_info",$inp);
				$this->erpm->flash_msg("New partner added");
			}else{
				$inp['modified_by']=$user['userid'];
				$this->db->update("partner_info",$inp,array("id"=>$this->input->post("pid")));
				$this->erpm->flash_msg("Partner details updated");
			}
			redirect("admin/partners");
		}
		$data['page']="add_partner";
		$this->load->view("admin",$data);
	}

	function partners()
	{
		$user=$this->auth(true);
		$data['partners']=$this->db->query("select * from partner_info order by name asc")->result_array();
		$data['page']="partners";
		$this->load->view("admin",$data);
	}

	function view_partner_orders($log_id)
	{
		$user=$this->auth(PARTNER_ORDERS_ROLE);
		$data['orders']=$this->db->query("select l.*,o.status from partner_order_items l join king_orders o on o.transid=l.transid where l.log_id=? group by l.transid",$log_id)->result_array();
		$data['log_id']=$log_id;
		$data['page']="partner_order_items";
		$this->load->view("admin",$data);
	}

	function warehouse_summary($type=0,$id=0)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		if($type==1)
		{
			$data['products']=$this->db->query("select p.product_id,p.product_name,sum(s.available_qty) as stock,sum(s.available_qty*s.mrp) as stock_value,p.mrp from m_product_info p join t_stock_info s on s.product_id=p.product_id and s.available_qty>0 where p.brand_id=$id group by p.product_id having sum(s.available_qty)!=0")->result_array();
			$data['pagetitle']="Products in Stock for brand: ".$this->db->query("select name from king_brands where id=?",$id)->row()->name;
		}
		$data['page']="warehouse_summary";
		$this->load->view("admin",$data);
	}

	function partner_orders()
	{
		$user=$this->auth(PARTNER_ORDERS_ROLE);
		$data['logs']=$this->erpm->getpartnerorderlogs();
		$data['page']="partner_orders";
		$this->load->view("admin",$data);
	}

	function partner_order_import()
	{
		$user=$this->auth(PARTNER_ORDERS_ROLE);
		if($_POST)
			$this->erpm->do_partner_order_import();
		$data['page']="partner_order_import";
		$this->load->view("admin",$data);
	}

	function generate_kfile()
	{
		$user=$this->auth(KFILE_ROLE);
		if($_POST)
			$this->erpm->do_generate_kfile();
		$data['outscans']=$this->erpm->getoutscansforkfile();
		$data['page']="generate_kfile";
		$this->load->view("admin",$data);
	}

	function packed_list($s=0,$e=0)
	{
		$user=$this->auth(OUTSCAN_ROLE|PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		if($s==0)
		{
			$data['pagetitle']="Today's Packed List";
			$s=$e=date("Y-m-d");
		}else $data['pagetitle']="Packed List between $s and $e";
		$s=date("Y-m-d H:i:s",strtotime($s));
		$e=date("Y-m-d H:i:s",strtotime($e." 23:59:59"));
		$data['packed']=$this->db->query("select b.shipped,p.transid,b.batch_id,b.p_invoice_no,b.invoice_no,b.awb,convert_tz(b.packed_on,'-08:00','+05:30') as packed_on,a.name as packed_by from shipment_batch_process_invoice_link b left outer join king_invoice p on p.invoice_no=b.invoice_no join king_admin a on a.id=b.packed_by where b.packed=1 and convert_tz(b.packed_on,'-08:00','+05:30') between '$s' and '$e' group by b.p_invoice_no order by b.packed_on desc")->result_array();
		$data['page']="packed_list";
		$this->load->view("admin",$data);
	}

	function outscan_list($s=0,$e=0)
	{
		$user=$this->auth(OUTSCAN_ROLE|PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		if($s==0)
		{
			$data['pagetitle']="Today's Outscan/Shipped List";
			$s=$e=date("Y-m-d");
		}else $data['pagetitle']="Outscan/Shipped List between $s and $e";
		$s=date("Y-m-d H:i:s",strtotime($s));
		$e=date("Y-m-d H:i:s",strtotime($e." 23:59:59"));
		$data['outscans']=$this->db->query("select p.transid,b.batch_id,b.p_invoice_no,b.invoice_no,b.awb,convert_tz(b.shipped_on,'-08:00','+05:30') as shipped_on,a.name as shipped_by from shipment_batch_process_invoice_link b left outer join king_invoice p on p.invoice_no=b.invoice_no join king_admin a on a.id=b.shipped_by where b.shipped=1 and convert_tz(b.shipped_on,'-08:00','+05:30') between '$s' and '$e' group by b.p_invoice_no order by b.shipped_on desc")->result_array();
		$data['page']="outscan_list";
		$this->load->view("admin",$data);
	}

	function outscan()
	{
		$user=$this->auth(OUTSCAN_ROLE);
		if($_POST)
			$this->erpm->do_outscan($_POST['awn']);
		$data['page']="outscan";
		$this->load->view("admin",$data);
	}

	function cancel_proforma_invoice($p_invoice)
	{
		$invoice=$this->db->query("select transid,order_id,p_invoice_no,p_invoice_no as invoice_no from proforma_invoices where p_invoice_no=? and invoice_status=1",$p_invoice)->result_array();
		if(empty($invoice))
			show_error("Proforma Invoice not found or Invoice already cancelled");
		$transid=$invoice[0]['transid'];
		$oids=array();
		foreach($invoice as $i)
			$oids[]=$i['order_id'];
		$orders=$this->db->query("select quantity as qty,itemid,id from king_orders where id in ('".implode("','",$oids)."')")->result_array();
		foreach($orders as $o)
		{
			$pls=$this->db->query("select qty,pl.product_id,p.mrp from m_product_deal_link pl join m_product_info p on p.product_id=pl.product_id where itemid=?",$o['itemid'])->result_array();
			$pls2=$this->db->query("select pl.qty,p.product_id,p.mrp from products_group_orders pgo join m_product_group_deal_link pl join m_product_info p on p.product_id=pgo.product_id where pgo.order_id=?",$o['id'])->result_array();
			$pls=array_merge($pls,$pls2);
			foreach($pls as $p)
			{
				$check_mrp=false;
				if($this->db->query("select 1 from t_stock_info where product_id=? and mrp=?",array($p['product_id'],$p['mrp']))->num_rows()==1)
					$check_mrp=true;
				$sql="update t_stock_info set available_qty=available_qty+? where product_id=?";
				if($check_mrp)
					$sql.=" and mrp=?";
				$sql.=" limit 1";
				$this->db->query($sql,array($p['qty']*$o['qty'],$p['product_id'],$p['mrp']));
				$this->erpm->do_stock_log(1,$p['qty']*$o['qty'],$p['product_id'],$this->db->query("select id from proforma_invoices where order_id=?",$o['id'])->row()->id,false,true);
			}
		}
		$this->db->query("update king_orders set status=0 where id in ('".implode("','",$oids)."') and transid=?",$transid);
		$this->db->query("update proforma_invoices set invoice_status=0 where p_invoice_no=?",$p_invoice);
		$this->erpm->do_trans_changelog($transid,"Proforma Invoice no $p_invoice cancelled");
		$this->session->set_flashdata("erp_pop_info","Proforma Invoice cancelled");
		$bid=$this->db->query("select batch_id from shipment_batch_process_invoice_link where p_invoice_no=?",$p_invoice)->row()->batch_id;
		if($this->db->query("select count(1) as l from shipment_batch_process_invoice_link where batch_id=?",$bid)->row()->l<=$this->db->query("select count(1) as l from shipment_batch_process_invoice_link where packed=1 and batch_id=?",$bid)->row()->l+$this->db->query("select count(1) as l from shipment_batch_process_invoice_link bi join proforma_invoices i on i.p_invoice_no=bi.p_invoice_no where bi.batch_id=? and bi.packed=0 and i.invoice_status=0",$bid)->row()->l)
			//		if($this->db->query("select count(1) as l from shipment_batch_process_invoice_link bi join proforma_invoices i on i.p_invoice_no=bi.p_invoice_no where bi.batch_id=? and bi.packed=0 and i.invoice_status=1",$bid)->row()->l==0)
			$this->db->query("update shipment_batch_process set status=2 where batch_id=? limit 1",$bid);
		redirect("admin/proforma_invoice/$p_invoice");
	}

	function proforma_invoice($p_invoice)
	{
		$user=$this->auth(INVOICE_PRINT_ROLE|ORDER_BATCH_PROCESS_ROLE);
		$data['batch']=$this->db->query("select * from shipment_batch_process_invoice_link where p_invoice_no=?",$p_invoice)->row_array();
		$data['invoice']=$this->db->query("select * from proforma_invoices where p_invoice_no=?",$p_invoice)->row_array();
		$data['orders']=$this->db->query("select o.id,i.id as itemid,i.name as product,o.quantity,o.transid from proforma_invoices p join king_orders o on o.id=p.order_id join king_dealitems i on i.id=o.itemid where p.p_invoice_no=?",$p_invoice)->result_array();
		$data['page']="proforma_invoice";
		$this->load->view("admin",$data);
	}

	function pack_invoice($inv_no="")
	{
		$user=$this->auth(INVOICE_PRINT_ROLE);
		$data['invoice']=$this->erpm->getinvoiceforpacking($inv_no);
		if($_POST)
			$this->erpm->do_pack();
		if(empty($inv_no))
			show_404();
		$data['page']="pack_invoice";
		$this->load->view("admin",$data);
	}

	function batch($bid)
	{
		$user=$this->auth(ORDER_BATCH_PROCESS_ROLE|OUTSCAN_ROLE|INVOICE_PRINT_ROLE);
		$data['batch']=$this->erpm->getbatch($bid);
		$data['invoices']=$this->erpm->getbatchinvoices($bid);
		$data['page']="batch";
		$this->load->view("admin",$data);
	}

	function stock_procure_list()
	{
		$user=$this->auth(INVOICE_PRINT_ROLE);
		$data['prods']=$this->erpm->getprodproclistfortransids($_POST['tids']);
		$this->load->view("admin/body/product_proc_list_transids",$data);
	}

	function product_proc_list_for_batch($bid)
	{
		$user=$this->auth(INVOICE_PRINT_ROLE);
		$data['prods']=$this->erpm->getprodproclist($bid);
		$this->load->view("admin/body/product_proc_list",$data);
	}

	function batch_process($s=false,$e=false)
	{
		$user=$this->auth(ORDER_BATCH_PROCESS_ROLE|INVOICE_PRINT_ROLE);
		if($s!=false && $e!=false && (strtotime($s)<=0 || strtotime($e)<=0))
			show_404();
		$data['batchs']=$this->erpm->getbatchs_date_range($s,$e);
		$data['page']="batch_list";
		if($e)
			$data['pagetitle']="between $s and $e";
		$this->load->view("admin",$data);
	}

	function pending_batch_process()
	{
		$user=$this->auth(ORDER_BATCH_PROCESS_ROLE|INVOICE_PRINT_ROLE);
		$data['batchs']=$this->erpm->getpendingbatchs();
		$data['page']="batch_list";
		$this->load->view("admin",$data);
	}

	function pnh_reverse_receipt($rid)
	{
		$user=$this->auth(true);
		$r=$this->db->query("select * from pnh_t_receipt_info where receipt_id=?",$rid)->row_array();
		$this->db->query("update pnh_t_receipt_info set status=3 where receipt_id=? limit 1",$rid);
		$_POST=array("type"=>1,"amount"=>$r['receipt_amount'],"desc"=>"Reversal of receipt $rid","internal"=>true,"sms"=>false);
		$this->pnh_acc_stat_c($r['franchise_id']);
		redirect($_SERVER['HTTP_REFERER']);
	}

	function pnh_acc_stat_c($fid)
	{
		$user=$this->auth(true);
		if(!$_POST)
			die;
		$mob=$this->db->query("select login_mobile1 as m from pnh_m_franchise_info where franchise_id=?",$fid)->row()->m;
		foreach(array("type","amount","desc","sms") as $i)
			$$i=$this->input->post($i);
		$this->erpm->pnh_fran_account_stat($fid,$type,$amount,$desc);
		if($sms)
			$this->erpm->pnh_sendsms($mob,"Amount of Rs $amount has been ".($type==0?"credited to":"debited from")." your franchise account against '$desc'",$fid);
		$this->erpm->flash_msg("Acount statement corrected");
		if(!isset($_POST['internal']))
			redirect("admin/pnh_franchise/$fid");
	}

	function product($pid=false)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		$data['product']=$this->db->query("select ifnull(sum(s.available_qty),0) as stock,p.*,b.name as brand from m_product_info p left outer join t_stock_info s on s.product_id=p.product_id join king_brands b on b.id=p.brand_id where p.product_id=?",$pid)->row_array();
		$data['log']=$this->db->query("select u.name as username,l.*,pi.p_invoice_no,ci.invoice_no as c_invoice_no,i.invoice_no from t_stock_update_log l left outer join king_invoice i on i.id=l.invoice_id left outer join t_client_invoice_info ci on ci.invoice_id=l.corp_invoice_id left outer join proforma_invoices pi on pi.id=l.p_invoice_id left outer join king_admin u on u.id=l.created_by where l.product_id=? order by l.id desc",$pid)->result_array();
		$data['page']="viewproduct";
		$this->load->view("admin",$data);
	}

	function editproduct($pid=false)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		if($_POST){
			foreach(array('pname',"pdesc","psize","puom","pmrp","pvat","pcost","pbarcode","pisoffer","pissrc","pbrand","prackbin","pmoq","prorder","prqty","premarks","pissno") as $i)
				$inp[]=$this->input->post($i);
			$inp[]=$pid;
			$this->db->query("update m_product_info set product_name=?,short_desc=?,size=?,uom=?,mrp=?,vat=?,purchase_cost=?,barcode=?,is_offer=?,is_sourceable=?,brand_id=?,default_rackbin_id=?,moq=?,reorder_level=?,reorder_qty=?,remarks=?,modified_on=now(),is_serial_required=? where product_id=? limit 1",$inp);
			$t_inp=array("product_id"=>$pid,"is_sourceable"=>$this->input->post("pissrc"),"created_on"=>time(),"created_by"=>$user['userid']);
			$this->db->insert("products_src_changelog",$t_inp);
			redirect("admin/product/$pid");
		}
		$data['prod']=$this->db->query("select * from m_product_info where product_id=?",$pid)->row_array();
		$data['page']="addproduct";
		$this->load->view("admin",$data);
	}

	function addproduct()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE);
		if($_POST)
		{
			$inp=array("P".rand(10000,99999));
			foreach(array('pname',"pdesc","psize","puom","pmrp","pvat","pcost","pbarcode","pisoffer","pissrc","pbrand","prackbin","pmoq","prorder","prqty","premarks","pissno") as $i)
				$inp[]=$this->input->post($i);
			$this->db->query("insert into m_product_info(product_code,product_name,short_desc,size,uom,mrp,vat,purchase_cost,barcode,is_offer,is_sourceable,brand_id,default_rackbin_id,moq,reorder_level,reorder_qty,remarks,is_serial_required,created_on)
					values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())",$inp);
			$pid=$this->db->insert_id();
			$rackbin=0;$location=0;
			$raw_rackbin=$this->db->query("select l.location_id as default_location_id,l.id as default_rack_bin_id from m_rack_bin_brand_link b join m_rack_bin_info l on l.id=b.rack_bin_id where b.brandid=?",$this->input->post("pbrand"))->row_array();
			if(!empty($raw_rackbin))
			{
				$rackbin=$raw_rackbin['default_rack_bin_id'];
				$location=$raw_rackbin['default_location_id'];
			}
			$this->db->query("insert into t_stock_info(product_id,location_id,rack_bin_id,mrp,available_qty) values(?,?,?,?,0)",array($pid,$location,$rackbin,$pmrp));
			redirect("admin/products");
		}


		$data['page']="addproduct";
		$this->load->view("admin",$data);
	}

	function createblankstockrows()
	{
		$sql="select p.product_id,p.mrp,r.default_location_id as loc,r.default_rack_bin_id as rack from m_product_info p left outer join m_brand_location_link r on r.brand_id=p.brand_id left outer join t_stock_info s on s.product_id=p.product_id group by p.product_id having count(s.product_id)=0";
		$prods=$this->db->query($sql)->result_array();
		print_r($prods);
		foreach($prods as $p)
			$this->db->query("insert into t_stock_info(product_id,location_id,rack_bin_id,mrp,available_qty) values(?,?,?,?,0)",array($p['product_id'],$p['loc'],$p['rack'],$p['mrp']));
	}

	function resetadminuserpass($uid=false)
	{
		$user=$this->auth(true);
		$admin=$this->db->query("select * from king_admin where id=?",$uid)->row_array();
		if(!$uid || empty($admin))
			show_error("UID is Missing or not available in database");
		$email=$admin['email'];
		$name=$admin['name'];
		$username=$admin['username'];
		$password=randomChars(6);
		$this->db->query("update king_admin set password=md5(?) where id=? limit 1",array($password,$uid));
		$this->vkm->email($email,"Your Snapittoday ERP account update","Hi $name,<br><br>Your ERP account password has been changed<br><br>Username : $username<br>Password : $password<br><br>ERP Team Snapittoday");
		$this->session->set_flashdata("erp_pop_info","Password reset for $name");
		redirect("admin/adminusers");
	}


	function editadminuser($uid)
	{
		$user=$this->auth(true);
		if($_POST)
			$this->erpm->do_updateadminuser($uid);
		$data['auser']=$this->db->query("select * from king_admin where id=?",$uid)->row_array();
		$data['roles']=$this->db->query("select * from user_access_roles order by user_role asc")->result_array();
		$data['page']="addadminuser";
		$this->load->view("admin",$data);
	}

	function addadminuser()
	{
		$user=$this->auth(true);
		if($_POST)
			$this->erpm->do_addadminuser();
		$data['roles']=$this->db->query("select * from user_access_roles order by id asc")->result_array();
		$data['page']="addadminuser";
		$this->load->view("admin",$data);
	}

	function adminusers()
	{
		$user=$this->auth(true);
		$data['page']="adminusers";
		$data['users']=$this->db->query("select * from king_admin order by name asc")->result_array();
		$data['roles']=$this->db->query("select * from user_access_roles order by id asc")->result_array();
		$this->load->view("admin",$data);
	}

	function roles()
	{
		$user=$this->auth(ADMINISTRATOR_ROLE);
		$data['roles']=$this->db->query("select * from user_access_roles order by id asc")->result_array();
		$data['page']="roles";
		$this->load->view("admin",$data);
	}

	function addbrand()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE|PRODUCT_MANAGER_ROLE);
		//print_r($user);
		//exit;
		if($_POST)
			$this->erpm->do_addbrand();
		$data['page']="editbrand";
		$data['brand']=array("name"=>"");
		$data['rbs']=array();
		$data['rackbins']=$this->db->query("select * from m_rack_bin_info order by rack_name asc")->result_array();
		$this->load->view("admin",$data);
	}

	function viewbrand($bid=false)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE|PRODUCT_MANAGER_ROLE);
		$data['page']="viewbrand";
		$data['brand']=$this->db->query("select * from king_brands where id=?",$bid)->row_array();
		$data['rbs']=$this->db->query("select r.* from m_rack_bin_brand_link rb join m_rack_bin_info r on r.id=rb.rack_bin_id where rb.brandid=?",$bid)->result_array();
		$data['products']=$this->erpm->getproductsforbrand($bid);
		$data['deals']=$this->erpm->getdealsforbrand($bid);
		$data['vendors']=$this->erpm->getvendorsforbrand($bid);
		$this->load->view("admin",$data);
	}

	function editbrand($bid=false)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE|PRODUCT_MANAGER_ROLE);
		if(empty($bid))
			show_404();
		if($_POST)
			$this->erpm->do_editbrand($bid);
		$data['rackbins']=$this->db->query("select * from m_rack_bin_info order by rack_name asc")->result_array();
		$data['rbs']=$this->erpm->getrackbinsforbrand($bid);
		$data['page']="editbrand";
		$data['brand']=$this->db->query("select * from king_brands where id=?",$bid)->row_array();
		$this->load->view("admin",$data);
	}

	function viewcat($cat=false)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		if(empty($cat))
			show_404();
		$data['cat']=$this->db->query("select c.*,m.name as main from king_categories c left outer join king_categories m on m.id=c.type where c.id=?",$cat)->row_array();
		$data['deals']=$this->db->query("select i.*,c.name as category,c.id as catid from king_categories c join king_deals d on d.catid=c.id join king_dealitems i on i.dealid=d.dealid where c.id=? or c.type=?",array($cat,$cat))->result_array();
		$data['page']="viewcat";
		$this->load->view("admin",$data);
	}

	function addcat()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		if($_POST)
			$this->erpm->do_addnewcat();
		$data['page']="addcategory";
		$this->load->view("admin",$data);
	}

	function editcat($catid=false)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		if(empty($catid))
			show_404();
		if($_POST)
			$this->erpm->do_updatecat($catid);
		$data['cat']=$this->db->query("select * from king_categories where id=?",$catid)->row_array();
		$data['page']="addcategory";
		$this->load->view("admin",$data);
	}

	function dashboard()
	{
		$user=$this->auth();
		$data['page']="dashboard";
		$this->load->view("admin",$data);
	}

	function export_data()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		if($_POST)
			$this->erpm->do_export_data();
		$data['page']="export_data";
		$this->load->view("admin",$data);
	}

	function jx_pub_deal()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		if(!$_POST)
			die;
		$this->db->query("update king_deals set publish=? where dealid=? limit 1",array($this->input->post("pub"),$this->input->post("did")));
	}

	function jx_live_deal()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		if(!$_POST)
			die;
		$this->db->query("update king_dealitems set live=? where id=? limit 1",array($this->input->post("live"),$this->input->post("id")));
	}

	function deals_bulk_image_update($bid)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		$data['items']=$this->db->query("select i.name,i.dealid,d.publish,i.live,b.* from deals_bulk_upload_items b join king_dealitems i on i.id=b.item_id join king_deals d on d.dealid=i.dealid where b.bulk_id=?",$bid)->result_array();
		$data['page']="deals_bulk_image_update";
		$this->load->view("admin",$data);
	}

	function bu_img_update()
	{
		$user=$this->auth();
		$i=$this->input->post("i");
		$iid=$this->input->post("itemid");
		$imgname=randomChars(15);
		if (isset ( $_FILES ['pic'] ) && $_FILES ['pic'] ['error'] == 0)
		{
			$this->load->library("thumbnail");
			$img=$_FILES['pic']['tmp_name'];
			if($this->thumbnail->check($img))
			{
				$this->thumbnail->create(array("source"=>$img,"dest"=>"images/items/300/$imgname.jpg","width"=>300));
				$this->thumbnail->create(array("source"=>$img,"dest"=>"images/items/small/$imgname.jpg","width"=>200));
				$this->thumbnail->create(array("source"=>$img,"dest"=>"images/items/thumbs/$imgname.jpg","width"=>50,"max_height"=>50));
				$this->thumbnail->create(array("source"=>$img,"dest"=>"images/items/$imgname.jpg","width"=>400));
				$this->thumbnail->create(array("source"=>$img,"dest"=>"images/items/big/$imgname.jpg","width"=>1000));
				$did=$this->db->query("select dealid from king_dealitems where id=?",$iid)->row()->dealid;
				$this->db->query("update king_dealitems set pic=? where id=? limit 1",array($imgname,$iid));
				$this->db->query("update king_deals set pic=? where dealid=? limit 1",array($imgname,$did));
				$this->db->query("update deals_bulk_upload_items set is_image_updated=1,updated_on=".time().",updated_by={$user['userid']} where item_id=?",$iid);
				$bid=$this->db->query("select bulk_id from deals_bulk_upload_items where item_id=?",$iid)->row()->bulk_id;
				if($this->db->query("select 1 from deals_bulk_upload_items where bulk_id=? and is_image_updated=0",$bid)->num_rows()==0)
					$this->db->query("update deals_bulk_upload set is_all_image_updated=1 where id=? limit 1",$bid);
				$err=0;
			}
			else $err=1;
		}
		else
			$err=1;
		echo "<script>parent.updatedimg($i,$err)</script>";
	}

	function dealsbymenu_table($mid)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		$data['deals']=$this->db->query("select d.pic,b.name as brand,c.name as category,m1.name as menu1,m2.name as menu2,i.id,d.dealid,i.name,i.orgprice as mrp,i.price,d.publish,i.live from king_deals d join king_dealitems i on i.dealid=d.dealid join king_categories c on c.id=d.catid join king_brands b on b.id=d.brandid join king_menu m1 on m1.id=d.menuid left outer join king_menu m2 on m2.id=d.menuid2 where d.menuid=? or d.menuid2=? order by i.name asc",array($mid,$mid))->result_array();
		$data['pagetitle']="menu : ".$this->db->query("select name from king_menu where id=?",$mid)->row()->name;
		$data['page']="deals_table";
		$this->load->view("admin",$data);
	}

	function dealsbycategory_table($cid)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		$data['deals']=$this->db->query("select d.pic,b.name as brand,c.name as category,m1.name as menu1,m2.name as menu2,i.id,d.dealid,i.name,i.orgprice as mrp,i.price,d.publish,i.live from king_deals d join king_dealitems i on i.dealid=d.dealid join king_categories c on c.id=d.catid join king_brands b on b.id=d.brandid join king_menu m1 on m1.id=d.menuid left outer join king_menu m2 on m2.id=d.menuid2 where d.catid=? order by i.name asc",array($cid))->result_array();
		$data['pagetitle']="Category : ".$this->db->query("select name from king_categories where id=?",$cid)->row()->name;
		$data['page']="deals_table";
		$this->load->view("admin",$data);
	}

	function dealsbybrand_table($cid)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		$data['deals']=$this->db->query("select d.pic,b.name as brand,c.name as category,m1.name as menu1,m2.name as menu2,i.id,d.dealid,i.name,i.orgprice as mrp,i.price,d.publish,i.live from king_deals d join king_dealitems i on i.dealid=d.dealid join king_categories c on c.id=d.catid join king_brands b on b.id=d.brandid join king_menu m1 on m1.id=d.menuid left outer join king_menu m2 on m2.id=d.menuid2 where d.brandid=? order by i.name asc",array($cid))->result_array();
		$data['pagetitle']="Brand : ".$this->db->query("select name from king_brands where id=?",$cid)->row()->name;
		$data['page']="deals_table";
		$this->load->view("admin",$data);
	}

	function deals_table()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		$data['deals']=$this->db->query("select d.pic,b.name as brand,c.name as category,m1.name as menu1,m2.name as menu2,i.id,d.dealid,i.name,i.orgprice as mrp,i.price,d.publish,i.live from king_deals d join king_dealitems i on i.dealid=d.dealid join king_categories c on c.id=d.catid join king_brands b on b.id=d.brandid join king_menu m1 on m1.id=d.menuid left outer join king_menu m2 on m2.id=d.menuid2 order by d.sno desc limit 40")->result_array();
		$data['page']="deals_table";
		$this->load->view("admin",$data);
	}

	function categories()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		$cats=$this->db->query("select * from king_categories order by name asc")->result_array();

		$as=$alphas=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
		foreach($as as $alpha)
			$ret[$alpha]=array();
		foreach($cats as $c)
		{
			$alpha=strtolower($c['name']{
				0});
				if(!isset($ret[$alpha]))
				{
					$ret[$alpha]=array();
					$alphas[]=$alpha;
				}
				$ret[$alpha][]=$c;
		}
		for($i=0;$i<5;$i++)
			$r[$i]=array();
			
		$i=0;
		foreach($ret as $a=>$rt)
		{
			$r[$i][$a]=$rt;
			$i++;
			if($i>4)
				$i=0;
		}
		$data['count']=count($cats);
		$data['alphas']=$alphas;
		$data['categories']=$r;
		$data['page']="categories";
		$this->load->view("admin",$data);
	}

	function brands()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE|PRODUCT_MANAGER_ROLE);
		$brands=$this->db->query("select * from king_brands order by name asc")->result_array();
		$as=$alphas=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");

		foreach($as as $alpha)
			$ret[$alpha]=array();

		foreach($brands as $c)
		{
			$alpha=strtolower($c['name']{
				0});

				if(!isset($ret[$alpha]))
				{
					$ret[$alpha]=array();

					$alphas[]=$alpha;
				}
				$ret[$alpha][]=$c;
		}
		for($i=0;$i<5;$i++)
			$r[$i]=array();
			
		$i=0;
		foreach($ret as $a=>$rt)
		{
			$r[$i][$a]=$rt;
			$i++;
			if($i>4)
				$i=0;
		}
		$data['count']=count($brands);
		$data['alphas']=$alphas;
		$data['brands']=$r;
		$data['rbs']=$this->erpm->getrackkbinsforbrands();
		$data['page']="brands";
		$this->load->view("admin",$data);
	}

	function vendorsbybrand($bid=false)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE|PURCHASE_ORDER_ROLE);
		if(!$bid)
			show_404();
		$data['page']="erpvendors";
		$data['brand']=$this->db->query("select name from king_brands where id=?",$bid)->row()->name;
		$data['vendors']=$this->erpm->getvendorsforbrand($bid);
		$this->load->view("admin",$data);
	}

	function vendors()
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE|PURCHASE_ORDER_ROLE);
		$data['page']="erpvendors";
		$data['vendors']=$this->erpm->getvendors();
		$this->load->view("admin",$data);
	}

	function changeusertemp($userid,$temp)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		$this->db->query("update king_users set temperament=? where userid=?",array($temp,$userid));
		redirect("admin/user/$userid");
	}

	function client_invoices($s=false,$e=false)
	{
		$user=$this->auth(FINANCE_ROLE);
		if($e)
			$data['pagetitle']="between $s and $e";
		$data['invoices']=$this->erpm->getclientinvoices($s,$e);
		$data['page']="client_invoices";
		$this->load->view("admin",$data);
	}

	function print_client_invoice($inv)
	{
		$user=$this->auth(FINANCE_ROLE);
		$data['invoice']=$this->erpm->getclientinvoice($inv);
		$data['orders']=$this->erpm->getclientinvoiceforprint($inv);
		$this->load->view("admin/body/client_invoice_print",$data);
	}

	function pack_client_invoice($inv)
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
		{
			$this->db->query("update t_client_invoice_info set invoice_status=1 where invoice_id=?",$inv);
			redirect("admin/client_invoice/$inv");
		}
		$data['invoice']=$this->db->query("select p.barcode,p.product_name,t.*,t.invoice_qty as qty from t_client_invoice_product_info t join m_product_info p on p.product_id=t.product_id where t.invoice_id=?",$inv)->result_array();
		$data['page']="pack_client_invoice";
		$this->load->view("admin",$data);
	}

	function payment_client_invoice($inv=false)
	{
		if(!$inv)
			show_404();
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
			$this->erpm->do_payment_client($inv);
		$data['page']="payment_client";
		$this->load->view("admin",$data);
	}

	function client_invoice($iid=false)
	{
		$user=$this->auth(FINANCE_ROLE);
		if(!$iid)
			show_404();
		$data['payments']=$this->db->query("select * from t_client_invoice_payment where invoice_id=?",$iid)->result_array();
		$data['invoice']=$this->db->query("select i.*,a.name as created_by from t_client_invoice_info i join king_admin a on a.id=i.created_by where i.invoice_id=?",$iid)->row_array();
		$data['items']=$this->db->query("select p.product_name,i.* from t_client_invoice_product_info i join m_product_info p on p.product_id=i.product_id where i.invoice_id=?",$iid)->result_array();
		$data['page']="client_invoice";
		$this->load->view("admin",$data);
	}

	function createclientinvoice()
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
			$this->erpm->do_clientinvoice();
		$data['clients']=$this->erpm->getclients();
		$data['page']="createclientinvoice";
		$this->load->view("admin",$data);
	}

	function jx_listclientordersforinvoice()
	{
		$user=$this->auth();
		if(!$_POST)
			die;
		$cid=$this->input->post("cid");
		foreach($this->db->query("select * from t_client_order_info where client_id=? and (order_status=0 || order_status=1)",$cid)->result_array() as $o)
			echo '<a href="javascript:void(0);" onclick="loadorder(\''.$o['order_id'].'\')">'."ORD{$o['order_id']}, {$o['order_reference_no']}".'</a>';
	}

	function jx_loadclientordersforinvoice()
	{
		$user=$this->auth();
		if(!$_POST)
			die;
		$oid=$this->input->post("oid");
		$items=$this->erpm->getclientordersforinvoice($oid);
		echo json_encode($items);
	}

	function client_order($oid=false)
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
			$this->erpm->do_closeclientorder($oid);
		$data['order']=$this->erpm->getclientorder($oid);
		$data['invoices']=$this->erpm->getinvoicesforclientorder($oid);
		$data['page']="client_order";
		$this->load->view("admin",$data);
	}

	function client_orders($s=false,$e=false)
	{
		$user=$this->auth(FINANCE_ROLE);
		$data['orders']=$this->erpm->getclientorders();
		$data['page']="client_orders";
		$this->load->view("admin",$data);
	}

	function client_orders_by_client($cid=0)
	{
		$user=$this->auth(FINANCE_ROLE);
		$data['orders']=$this->erpm->getclientordersbyclient($cid);
		$data['byclient']=true;
		$data['pagetitle']="for ".$this->db->query("select client_name as c from m_client_info where client_id=?",$cid)->row()->c;
		$data['page']="client_orders";
		$this->load->view("admin",$data);
	}


	function clients()
	{
		$user=$this->auth(FINANCE_ROLE);
		$data['clients']=$this->erpm->getclients();
		$data['page']="clients";
		$this->load->view("admin",$data);
	}

	function addclientorder($cid=false)
	{
		$user=$this->auth(FINANCE_ROLE);
		if($cid==false)
			show_404();
		if($_POST)
			$this->erpm->do_addclientorder($cid);
		$data['page']="addclientorder";
		$this->load->view("admin",$data);
	}

	function editclient($cid)
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
			$this->erpm->do_updateclient($cid);
		$data['client']=$this->db->query("select * from m_client_info where client_id=?",$cid)->row_array();
		$data['contacts']=$this->db->query("select * from m_client_contacts_info where client_id=?",$cid)->result_array();
		$data['page']="addclient";
		$this->load->view("admin",$data);
	}

	function addclient()
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
			$this->erpm->do_addclient();
		$data['page']="addclient";
		$this->load->view("admin",$data);
	}

	function stock_checker()
	{
		$user=$this->auth();
		if($_POST)
		{
			$data['deal']=$this->db->query("select name,id from king_dealitems where id=?",$_POST['id'])->row_array();
			$avail=$this->erpm->do_stock_check(array($data['deal']['id']));
			$data['status']=1;
			if(empty($avail))
				$data['status']=0;
		}
		$data['page']="stock_checker";
		$this->load->view("admin",$data);
	}

	function update_partner_deal_prices($itemid)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		if($_POST)
		{
			foreach(array("partner_id","customer_price","partner_price") as $i)
				$$i=$this->input->post($i);
			foreach($partner_id as $i=>$pid)
			{
				$inp=array("partner_id"=>$pid,"itemid"=>$itemid,"customer_price"=>$customer_price[$i],"partner_price"=>$partner_price[$i]);
				if($this->db->query("select 1 from partner_deal_prices where itemid=? and partner_id=?",array($itemid,$pid))->num_rows()==0)
				{
					$inp['created_on']=time();$inp['created_by']=$user['userid'];
					$this->db->insert("partner_deal_prices",$inp);
				}else{
					$inp['modified_on']=time();
					$inp['modified_by']=$user['userid'];
					$this->db->where("partner_id",$inp['partner_id']);
					$this->db->where("itemid",$inp['itemid']);
					$this->db->update("partner_deal_prices",$inp);
				}
			}
			$this->erpm->flash_msg("Partner prices updated");
			redirect("admin/update_partner_deal_prices/$itemid");
		}
		$data['itemid']=$itemid;
		$data['page']="update_partner_deal_prices";
		$this->load->view("admin",$data);
	}

	function partner_deal_prices()
	{
		$user=$this->auth(true);
		if($_POST)
		{
			$f=fopen($_FILES['pfile']['tmp_name'],"r");
			$head=fgetcsv($f);
			$out=array($head);
			$c=0;
			$template=array("partner_id","itemid","customer_price","partner_price");
			while(($data=fgetcsv($f))!==false)
			{
				foreach($template as $i=>$c)
					$inp[$c]=$data[$i];
				if($this->db->query("select 1 from partner_deal_prices where itemid=? and partner_id=?",array($data[1],$data[0]))->num_rows()==0)
				{
					$inp['created_on']=time();$inp['created_by']=$user['userid'];
					$this->db->insert("partner_deal_prices",$inp);
				}else{
					$inp['modified_on']=time();
					$inp['modified_by']=$user['userid'];
					$this->db->where("partner_id",$inp['partner_id']);
					$this->db->where("itemid",$inp['itemid']);
					$this->db->update("partner_deal_prices",$inp);
				}
				$c++;
			}
			$this->erpm->flash_msg("$c deal prices updated");
		}
		$data['page']="partner_deal_prices";
		$this->load->view("admin",$data);
	}


	function search()
	{
		$user=$this->auth();
		if(strlen($this->input->post("q"))<3)
			show_error("Please enter atleast 3 characters to search");
		$eq=$this->input->post("q");
		$q="%".$this->input->post("q")."%";
		$data['categories']=$this->db->query("select * from king_categories where name like ?",$q)->result_array();
		$data['brands']=$this->db->query("select * from king_brands where name like ?",$q)->result_array();
		$data['deals']=$this->db->query("select name,dealid,id from king_dealitems where is_pnh=0 and name like ?",$q)->result_array();
		$data['products']=$prods=$this->db->query("select product_name,product_id from m_product_info where product_name like ?",$q)->result_array();
		foreach($prods as $i=>$p)
			$prods[$i]['stock']=$this->db->query("select sum(available_qty) as s from t_stock_info where product_id=?",$p['product_id'])->row()->s;
		$data['products']=$prods;
		$data['invoices']=$this->db->query("select * from king_invoice where invoice_no like ?",$q)->result_array();
		$data['orders']=$this->db->query("select transid from king_transactions where transid like ?",$q)->result_array();
		$data['clients']=$this->db->query("select client_id,client_name from m_client_info where client_name like ?",$q)->result_array();
		$data['users']=$this->db->query("select * from king_users where email like ? or name like ? or (mobile=? and mobile<>0) order by name asc",array($q,$q,$this->input->post("q")))->result_array();
		$data['tickets']=$this->db->query("select * from support_tickets where ticket_no like ? or concat('TK',ticket_no) like ?",array($q,$q))->result_array();
		$data['vendors']=$this->db->query("select * from m_vendor_info where vendor_name like ? or vendor_code like ?",array($q,$q))->result_array();
		$data['awbs']=$this->db->query("select b.*,o.transid from shipment_batch_process_invoice_link b join king_invoice o on o.invoice_no=b.invoice_no where b.awb=? limit 1",$this->input->post("q"))->result_array();
		$data['pnh_deals']=$this->db->query("select name,id,pnh_id from king_dealitems where is_pnh=1 and (name like ? or (pnh_id=? and pnh_id!='0'))",array($q,$this->input->post("q")))->result_array();
		$data['pnh_franchises']=$this->db->query("select franchise_id,pnh_franchise_id,franchise_name from pnh_m_franchise_info where pnh_franchise_id=? or franchise_name like ? or login_mobile1=? or login_mobile2=?",array($eq,$q,$eq,$eq))->result_array();
		$data['pnh_members']=$this->db->query("select concat(first_name,' ',last_name) as name,pnh_member_id,user_id from pnh_member_info where pnh_member_id=? or concat(first_name,' ',last_name) like ?",array($eq,$q))->result_array();
		$data['page']="search_results";
		$this->load->view("admin",$data);
	}

	function pnh_jx_checkstock_order()
	{
		$fid=$this->input->post("fid");
		$mid=$this->input->post("mid");
		$pids=explode(",",$this->input->post("pids"));
		$qty=explode(",",$this->input->post("qty"));
		$iids=$this->db->query("select id,pnh_id from king_dealitems where is_pnh=1 and pnh_id in('".implode("','",$pids)."')")->result_array();
		$itemids=array();
		$order_det=array();
		$e=0;
		foreach($pids as $pid)
			foreach($iids as $id)
			if($id['pnh_id']==$pid)
			$itemids[]=$id['id'];
		$avail=$this->erpm->do_stock_check($itemids,$qty);
		$un="";
		$attr=$this->input->post('attr');
		$attr_data=array();
		if($attr)
		{
			$attrs=explode("&",$attr);
			foreach($attrs as $attr)
			{
				list($pp,$v)=explode("=",$attr);
				list($p,$a)=explode("_",$pp);
				if(!isset($attr_data[$p]))
					$attr_data[$p]=array();
				$attr_data[$p][$a]=$v;
			}
		}
		foreach($pids as $pid)
		{
			if(!isset($attr_data[$pid]))
				continue;
			$prods=array();
			$i=0;
			foreach($attr_data[$pid] as $a=>$v)
			{
				if($i==0)
				{
					$pr=$this->db->query("select product_id from products_group_pids where attribute_name_id=? and attribute_value_id=?",array($a,$v))->result_array();
					foreach($pr as $p)
						$prods[]=$p['product_id'];
				}else{
					$c_prods=$prods;
					$prods=array();
					$pr=$this->db->query("select product_id from products_group_pids where attribute_name_id=? and attribute_value_id=?",array($a,$v))->result_array();
					foreach($pr as $p)
						if(in_array($p['product_id'],$c_prods))
						$prods[]=$p['product_id'];
				}
				$i++;
				if(empty($prods))
				{
					$e=1;
					$un.="{$pid} is not available for selected combination";
					break;
				}
			}
		}
		if($e==0)
		{
			foreach($itemids as $i=>$itemid)
				if(!in_array($itemid,$avail))
		 	$un.="{$pids[$i]} is out of stock\n";
		 $e=0;
		 if(strlen($un)!=0)
		 	$e=1;
		}
		$total=$d_total=$bal=$abal=0;
		$pc="";
		if($e==0 && $this->db->query("select 1 from pnh_member_info where pnh_member_id=?",$mid)->num_rows()==0 && $this->db->query("select 1 from pnh_m_allotted_mid where franchise_id=? and ? between mid_start and mid_end",array($fid,$mid))->num_rows()==0)
		{
			$e=1;$un="MID : $mid is not allotted to this franchise";
		}
		if($e==0)
		{
			$iids=array();
			$fran=$this->db->query("select * from pnh_m_franchise_info where franchise_id=?",$fid)->row_array();
			$margin=$this->db->query("select margin,combo_margin from pnh_m_class_info where id=?",$fran['class_id'])->row_array();
			if($fran['sch_discount_start']<time() && $fran['sch_discount_end']>time() && $fran['is_sch_enabled'])
				$margin['margin']+=$fran['sch_discount'];
			foreach($pids as $i=>$iid)
			{
				$prod=$this->db->query("select i.*,d.publish from king_dealitems i join king_deals d on d.dealid=i.dealid where i.is_pnh=1 and i.pnh_id=?",$iid)->row_array();
				$items[$i]['name']=$prod['name'];
				$items[$i]['tax']=$prod['tax'];
				$items[$i]['mrp']=$prod['orgprice'];
				$items[$i]['price']=$prod['price'];
				$items[$i]['itemid']=$prod['id'];
				$margin=$this->erpm->get_pnh_margin($fran['franchise_id'],$iid);
				$items[$i]['base_margin']=$margin['base_margin'];
				$items[$i]['sch_margin']=$margin['sch_margin'];
				if($prod['is_combo']=="1")
				{
					$items[$i]['discount']=$items[$i]['price']/100*$margin['combo_margin'];
					$items[$i]['base_margin']=$margin['combo_margin'];
				}
				else
					$items[$i]['discount']=$items[$i]['price']/100*$margin['margin'];
				$total+=$items[$i]['price']*$qty[$i];
				$items[$i]['qty']=$qty[$i];
				$d_total+=($items[$i]['price']-$items[$i]['discount'])*$qty[$i];
				$items[$i]['final_price']=($items[$i]['price']-$items[$i]['discount']);
				$iids[]=$prod['id'];
			}
			$bal=$fran['current_balance'];
			$abal=$fran['current_balance']-$d_total;
			if($fran['credit_limit']+$fran['current_balance']<$d_total)
			{
				$e=1;$un="Insufficient balance! Balance in your account Rs {$fran['current_balance']}\nTotal order amount : Rs $d_total";
			}
			$pc_data['deals']=$this->erpm->pnh_getdealpricechanges($fran['app_version'],$iids);
			$pc_data['total']=$total;
			$pc_data['mid']=$mid;
			$pc_data['items']=$items;
			$pc=$this->load->view("admin/body/pc_offline_frag",$pc_data,true);
		}
		die(json_encode(array("e"=>$e,"msg"=>$un,"total"=>$total,"d_total"=>$d_total,"com"=>$total-$d_total,"bal"=>$bal,"abal"=>$abal,"pc"=>$pc)));
	}

	function pnh_fran_ver_change($fid,$v)
	{
		$user=$this->auth(true);
		$this->db->query("update pnh_m_franchise_info set app_version=? where franchise_id=? limit 1",array($v,$fid));
		$this->erpm->flash_msg("Version changed for franchise");
		redirect("admin/pnh_franchise/$fid");
	}

	function jx_pnh_prod_suggestion()
	{
		$user=$this->auth();
		$pid=$_POST['pid'];
		$fid=$_POST['fid'];
		$prods=array();
		$cat_brand=$this->db->query("select d.catid,d.brandid from king_dealitems i join king_deals d on d.dealid=i.dealid where i.pnh_id=?",$pid)->row_array();
		$catid=$cat_brand['catid'];
		$brandid=$cat_brand['brandid'];
		foreach($this->db->query("select i.is_combo,i.orgprice as mrp,i.price,i.name,i.pnh_id,d.catid,d.brandid from king_deals d join king_dealitems i on i.dealid=d.dealid where i.is_pnh=1 and d.publish=1 and (d.brandid=? or d.catid=?) and i.pnh_id!=$pid and i.live=1 order by length(concat(d.catid,d.brandid)) desc limit 40",array($brandid,$catid))->result_array() as $p)
		{
			$pid=$p['pnh_id'];
			$margin=$this->erpm->get_pnh_margin($fid,$pid);
			if($p['is_combo']=="1")
				$p['discount']=$p['price']/100*$margin['combo_margin'];
			else
				$p['discount']=$p['price']/100*$margin['margin'];
			$p['margin']=$p['discount']/$p['price']*100;
			$prods[]=$p;
		}
		$data['prods']=$prods;
		$this->load->view("admin/body/pnh_prod_suggest_frag",$data);
	}

	function pnh_place_quote()
	{
		$user=$this->auth(CALLCENTER_ROLE);
		foreach(array("pid","quote","fid","qty") as $i)
			$$i=$this->input->post($i);
		if(empty($pid))
			show_error("Quote is empty. There is no products");
		$inp=array("franchise_id"=>$fid,"created_on"=>time(),"created_by"=>$user['userid']);
		$this->db->insert("pnh_quotes",$inp);
		$qid=$this->db->insert_id();
		foreach($pid as $i=>$p)
		{
			$inp=array("quote_id"=>$qid,"pnh_id"=>$p,"qty"=>$qty[$i],"dp_price"=>$quote[$i]);
			$this->db->insert("pnh_quotes_deal_link",$inp);
		}
		echo site_url("admin/pnh_quote/$qid");
	}

	function pnh_update_quote($qid)
	{
		$user=$this->auth(true);
		if($_POST)
		{
			foreach(array("id","final") as $i)
				$$i=$this->input->post($i);
			$_C_POST=$_POST;
			foreach($id as $j=>$i)
			{
				if($final[$j]==0)
					continue;
				if(isset($_C_POST['up_sm'.$i]))
				{
					$_POST=array("special_margin"=>$final[$j],"from"=>date("Y-m-d"),"to"=>date("Y-m-d"),"type"=>1,'internal'=>1);
					$itemid=$this->db->query("select i.id from pnh_quotes_deal_link l join king_dealitems i on i.is_pnh=1 and i.pnh_id=l.pnh_id where l.id=?",$i)->row()->id;
					$this->pnh_special_margin_deal($itemid);
				}
				$this->db->query("update  pnh_quotes_deal_link set final_price=?,status=1,price_updated_by=?,updated_on=? where id=? limit 1",array($final[$j],$user['userid'],time(),$i));
			}
			$this->db->query("update pnh_quotes set updated_by=?,updated_on=? where quote_id=? limit 1",array($user['userid'],time(),$qid));
			redirect("admin/pnh_quote/$qid");
		}
	}

	function pnh_quotes($fid=0,$s=false,$e=false)
	{
		$user=$this->auth(OFFLINE_ORDER_ROLE);
		$from=$to=0;
		if($s)
		{
			$from=strtotime($s);
			$to=strtotime("23:59:59 $e");
		}
		$sql="select q.*,u.name as updated_by,c.name as created_by,f.franchise_name from pnh_quotes q join pnh_m_franchise_info f on f.franchise_id=q.franchise_id join king_admin c on c.id=q.created_by left outer join king_admin u on u.id=q.updated_by where 1 ";
		if($fid)
			$sql.=" and q.franchise_id=? ";
		if($from)
			$sql.=" and q.created_on between $from and $to ";
		$sql.=" order by q.created_on desc,q.updated_on desc";
		$title="Order Quotes";
		if($fid)
			$title.=" for '".$this->db->query("select franchise_name from pnh_m_franchise_info where franchise_id=?",$fid)->row()->franchise_name."'";
		if($from)
			$title.=" between $s and $e";
		$data['pagetitle']=$title;
		$data['quotes']=$this->db->query($sql,$fid)->result_array();
		$data['page']="pnh_quotes";
		$this->load->view("admin",$data);
	}

	function pnh_quote($qid)
	{
		$user=$this->auth(CALLCENTER_ROLE);
		if($_POST)
		{
			$id=$_POST['id'];
			$transid=$_POST['transid'];
			$this->db->query("update pnh_quotes_deal_link set transid=?,order_status=1,updated_on=?,updated_by=? where id=? limit 1",array($transid,time(),$user['userid'],$id));
			die;
		}
		$data['quote']=$this->db->query("select q.*,f.franchise_id,f.franchise_name,u.name as admin,q.quote_id from pnh_quotes q join pnh_m_franchise_info f on f.franchise_id=q.franchise_id join king_admin u on u.id=q.created_by where q.quote_id=?",$qid)->row_array();
		$data['deals']=$this->db->query("select i.name,i.orgprice as mrp,i.price,q.pnh_id,q.* from pnh_quotes_deal_link q join king_dealitems i on i.pnh_id=q.pnh_id and i.is_pnh=1 where q.quote_id=?",$qid)->result_array();
		$data['page']="pnh_quote";
		$this->load->view("admin",$data);
	}

	function pnh_offline_order()
	{
		$user=$this->auth(CALLCENTER_ROLE);
		if($_POST)
			$this->erpm->do_pnh_offline_order();
		$data['page']="pnh_offline_order";
		$this->load->view("admin",$data);
	}

	function pnh_jx_getfranbalance()
	{
		$user=$this->auth();
		$fran=$this->db->query("select current_balance as balance,credit_limit as credit from pnh_m_franchise_info where franchise_id=?",$_POST['id'])->row_array();
		echo json_encode($fran);
	}

	function pnh_jx_loadfranchisebyid()
	{
		$user=$this->auth();
		$fid=$_POST['fid'];
		$fran=$this->db->query("select * from pnh_m_franchise_info where pnh_franchise_id=?",$fid)->row_array();
		$sec_q=array("What was your childhood nickname?","In what city were you born?","What is the name of the company of your first job?","In what year was your father born?","What was the name of your elementary / primary school?","What is your mother's maiden name?"," What is your oldest sibling's name?"," Who was your childhood hero?");
		if(empty($fran))
			die("<h3>No franchisee available for given id</h3>");
		if($fran['is_suspended']==1)
			die("<h3>This franchise account is suspended");
		echo "<h4><a target='_blank' href='".site_url("admin/pnh_franchise/".$fran['franchise_id'])."'>{$fran['franchise_name']}</a></h4>";
		echo '<table border=1 cellpadding="5"><tr><th>Login Details</th><th>Authenticate</th><th>Details</th><th></th></tr>';
		echo "<tr><td>Login Mobile1 : <span class='ff_mob'>{$fran['login_mobile1']}</span><img src='".IMAGES_URL."phone.png' class='phone_small' onclick='makeacall(\"0{$fran['login_mobile1']}\")'></td><td>Security Question : ".($fran['security_question']=="-1"?$fran['security_custom_question']:$sec_q[$fran['security_question']])."  Answer : <b>{$fran['security_answer']}</b></td><td>FID : {$fran['pnh_franchise_id']}</td><td>Territory : ".$this->db->query("select territory_name as name from pnh_m_territory_info where id=?",$fran['territory_id'])->row()->name."</td></tr>";
		echo "<tr><td>Login Mobile2 : {$fran['login_mobile2']}<img src='".IMAGES_URL."phone.png' class='phone_small' onclick='makeacall(\"0{$fran['login_mobile2']}\")'></td><td>Security Question2 : ".($fran['security_question2']=="-1"?$fran['security_custom_question2']:$sec_q[$fran['security_question2']])."  Answer : <b>{$fran['security_answer2']}</b></td><td>Balance : Rs {$fran['current_balance']}</td><td>Town : ".$this->db->query("select town_name as name from pnh_towns where id=?",$fran['town_id'])->row()->name."</td></tr>";
		echo "<tr><td>Login Email : {$fran['email_id']}</td><td></td><td>Credit Limit : Rs {$fran['credit_limit']}</td></tr>";
		echo '</table>';
		echo "<div id='auth_cont'><input type='button' value='Authenticate' onclick='select_fran({$fran['franchise_id']})'></div>";
	}

	function pnh_jx_loadfranchisebymobile()
	{
		$user=$this->auth();
		$mobile=$_POST['mobile'];
		$fran=$this->db->query("select * from pnh_m_franchise_info where (login_mobile1=? and login_mobile1<>0) or (login_mobile2=? and login_mobile2!=0)",array($mobile,$mobile))->row_array();
		$sec_q=array("What was your childhood nickname?","In what city were you born?","What is the name of the company of your first job?","In what year was your father born?","What was the name of your elementary / primary school?","What is your mother's maiden name?"," What is your oldest sibling's name?"," Who was your childhood hero?");
		if(empty($fran))
			die("<h3>No franchisee available for given id</h3>");
		if($fran['is_suspended']==1)
			die("<h3>This franchise account is suspended");
		echo "<h3><a target='_blank' href='".site_url("admin/pnh_franchise/".$fran['franchise_id'])."'>{$fran['franchise_name']}</a></h3>";
		echo '<table border=1 cellpadding="5"><tr><th>Login Details</th><th>Authenticate</th><th>Details</th></tr>';
		echo "<tr><td>Login Mobile1 : <span class='ff_mob'>{$fran['login_mobile1']}</span><img src='".IMAGES_URL."phone.png' class='phone_small' onclick='makeacall(\"0{$fran['login_mobile1']}\")'></td><td>Security Question : ".($fran['security_question']=="-1"?$fran['security_custom_question']:$sec_q[$fran['security_question']])."  Answer : <b>{$fran['security_answer']}</b></td><td>FID : {$fran['pnh_franchise_id']}</td><td>Territory : ".$this->db->query("select territory_name as name from pnh_m_territory_info where id=?",$fran['territory_id'])->row()->name."</td></tr>";
		echo "<tr><td>Login Mobile2 : {$fran['login_mobile2']}<img src='".IMAGES_URL."phone.png' class='phone_small' onclick='makeacall(\"0{$fran['login_mobile2']}\")'></td><td>Security Question2 : ".($fran['security_question2']=="-1"?$fran['security_custom_question2']:$sec_q[$fran['security_question2']])."  Answer : <b>{$fran['security_answer2']}</b></td><td>Balance : Rs {$fran['current_balance']}</td><td>Town : ".$this->db->query("select town_name as name from pnh_towns where id=?",$fran['town_id'])->row()->name."</td></tr>";
		echo "<tr><td>Login Email : {$fran['email_id']}</td><td></td><td>Credit Limit : Rs {$fran['credit_limit']}</td></tr>";
		echo '</table>';
		echo "<div id='auth_cont'><input type='button' value='Authenticate' onclick='select_fran({$fran['franchise_id']})'></div>";
	}

	function jx_checkloginmob()
	{
		$mobile=$this->input->post("mob");
		$fran=$this->db->query("select * from pnh_m_franchise_info where (login_mobile1=? and login_mobile1<>0) or (login_mobile2=? and login_mobile2!=0)",array($mobile,$mobile))->row_array();
		if(empty($fran))
			echo "1";
		else
			echo "0";
	}

	function pnh_jx_loadpnhprodbybarcode()
	{
		$user=$this->auth();
		$barcode=$_POST['barcode'];
		$ret=$this->db->query("select i.pnh_id as pid from m_product_info p join m_product_deal_link l on l.product_id=p.product_id join king_dealitems i on i.id=l.itemid and i.is_pnh=1 where p.barcode=?",$barcode)->row_array();
		if(empty($ret))
			echo json_encode(array("pid"=>0));
		else
			echo json_encode(array("pid"=>$ret['pid']));
	}

	function pnh_jx_show_schemes()
	{
		$fid=$_POST['fid'];
		$msg='<table width="100%" cellpadding=5 cellspacing=0>';
		$msg.='<thead><tr><th>Brand</th><th>Category</th><th>Discount</th></tr></thead>';
		$msg.='<tbody>';
		$disc=$this->db->query("select s.*,a.name as admin,b.name as brand,c.name as category from pnh_sch_discount_brands s left outer join king_brands b on b.id=s.brandid left outer join king_categories c on c.id=s.catid join king_admin a on a.id=s.created_by where s.franchise_id=? and ? between valid_from and valid_to group by brandid order by id desc",array($fid,time()))->result_array();
		foreach($disc as $s){
			$msg.="<tr><td>".(empty($s['brand'])?"All brands":$s['brand'])."</td>";
			$msg.="<td>".(empty($s['category'])?"All categories":$s['category'])."</td>";
			$msg.="<td>".($s['discount'])."%</td>";
			$msg.="</tr>";
		}
		if(empty($disc))
			$msg.="<tr><Td colspan='100%'>no schemes</td></tr>";
		$msg.="</tbody></table>";
		echo $msg;
	}

	function pnh_jx_loadpnhprod()
	{
		$user=$this->auth();
		$fid=$_POST['fid'];
		$pid=$_POST['pid'];
		$prod=$this->db->query("select i.id,i.is_combo,i.pnh_id as pid,i.live,i.orgprice as mrp,i.price,i.name from king_dealitems i join king_deals d on d.dealid=i.dealid and d.publish=1 where pnh_id=? and is_pnh=1",$pid)->row_array();
		if(!empty($prod))
		{
			$stock=$this->erpm->do_stock_check(array($prod['id'])); if(empty($stock)) $prod['live']=0; else $prod['live']=1;
			$margin=$this->erpm->get_pnh_margin($fid,$pid);
			if($prod['is_combo'])
				$prod['margin']=$margin['combo_margin'];
			else
				$prod['margin']=$margin['margin'];
			$attr="";
			foreach($this->db->query("select group_id from m_product_group_deal_link where itemid=?",$prod['id'])->result_array() as $g)
			{
				$group=$this->db->query("select group_id,group_name from products_group where group_id=?",$g['group_id'])->row_array();
				$attr.="<div style='padding:5px;'>{$group['group_name']}";
				$anames=$this->db->query("select attribute_name_id,attribute_name from products_group_attributes where group_id=?",$g['group_id'])->result_array();
				foreach($anames as $a)
				{
					$attr.="<br>{$a['attribute_name']} :<select class='attr' name='{$pid}_{$a['attribute_name_id']}'>";
					$avalues=$this->db->query("select * from products_group_attribute_values where attribute_name_id=?",$a['attribute_name_id'])->result_array();
					foreach($avalues as $v)
						$attr.="<option value='{$v['attribute_value_id']}'>{$v['attribute_value']}</option>";
					$attr.='</select>';
				}
				$attr.="</div>";
			}
			$prod['lcost']=round($prod['price']-($prod['price']/100*$prod['margin']),2);
			$prod['attr']=$attr;
			unset($prod['is_combo']);
		}
		echo json_encode($prod);
	}

	function pnh_expire_scheme_discount($id)
	{
		$user=$this->auth(true);
		$this->db->query("update pnh_sch_discount_brands set valid_to=? where id=? limit 1",time()-20);
		redirect($_SERVER['HTTP_REFERER']);
	}

	function pnh_special_margin_deal($itemid)
	{
		$user=$this->auth(true);
		foreach(array("special_margin","from","to","type") as $i)
			$$i=$this->input->post($i);
		if($type)
		{
			$offer=$this->db->query("select price from king_dealitems where id=?",$itemid)->row()->price;
			if($special_margin>$offer)
				show_error("Special margin price can't be greater than offer price (Rs $offer)");
			$special_margin=round(($offer-$special_margin)/$offer*100,2);
		}
		$from=strtotime($from);
		$to=strtotime("23:59:59 $to");
		$inp=array("special_margin"=>$special_margin,"itemid"=>$itemid,"from"=>$from,"to"=>$to,"created_on"=>time(),"created_by"=>$user['userid']);
		$this->db->insert("pnh_special_margin_deals",$inp);
		$this->erpm->flash_msg("Special margin updated");
		if(!isset($_POST['internal']))
			redirect("admin/pnh_deal/$itemid");
	}

	function pnh_pub_unpub_deals()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		foreach(array("action","itemids") as $i)
			$$i=$this->input->post($i);
		$ids=explode(",",$itemids);
		foreach(explode(",",$itemids) as $id)
			$this->db->query("update king_deals set publish=$action where dealid=? limit 1",$this->db->query("select dealid from king_dealitems where id=?",$id)->row()->dealid);
		$this->erpm->flash_msg(count($ids)." deals ".($action=="0"?"Unp":"P")."ublished");
		redirect($_SERVER['HTTP_REFERER']);
	}

	function pnh_editdeal($itemid)
	{
		$this->pnh_adddeal($itemid);
	}

	function pnh_adddeal($itemid=false)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		if($_POST && !$itemid)
			$this->erpm->do_pnh_adddeal();
		else if($_POST)
			$this->erpm->do_pnh_updatedeal($itemid);
		if($itemid)
			$data['deal']=$this->db->query("select d.*,i.*,d.description,d.keywords,d.tagline from king_dealitems i join king_deals d on d.dealid=i.dealid where i.id=?",$itemid)->row_array();
		$data['page']="pnh_adddeal";
		$this->load->view("admin",$data);
	}

	function pnh_edit_fran($fid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		if($_POST)
			$this->erpm->do_pnh_updatefranchise($fid);
		$data['fran']=$this->db->query("select * from pnh_m_franchise_info where franchise_id=?",$fid)->row_array();
		$data['page']="pnh_addfranchise";
		$this->load->view("admin",$data);
	}

	function pnh_assign_exec($fid)
	{
		$user=$this->auth(true);
		if($_POST)
		{
			$admins=array_unique($this->input->post("admins"));
			$this->db->query("delete from pnh_franchise_owners where franchise_id=?",$fid);
			foreach($admins as $a)
				$this->db->query("insert into pnh_franchise_owners(admin,franchise_id,created_by,created_on) values(?,?,?,?)",array($a,$fid,$user['userid'],time()));
			$this->erpm->flash_msg("Executives assigned to franchise");
			redirect("admin/pnh_franchise/$fid");
		}
		$data['admins']=$this->db->query("select * from king_admin order by name asc")->result_array();
		$data['exec']=$this->db->query("select admin from pnh_franchise_owners where franchise_id=?",$fid)->result_array();
		$data['page']="pnh_assign_exec";
		$this->load->view("admin",$data);
	}

	function pnh_addfranchise()
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		if($_POST)
			$this->erpm->do_pnh_addfranchise();
		$data['page']="pnh_addfranchise";
		$this->load->view("admin",$data);
	}

	function pnh_app_versions()
	{
		$user=$this->auth(true);
		if($_POST)
		{
			$no=$this->input->post("no");
			$no=$no+1-1;
			if(empty($no) || $no==0)
				show_error("Invalid version number  : Not an integer");
			if($this->db->query("select 1 from pnh_app_versions")->num_rows()!=0 && $this->db->query("select 1 from pnh_app_versions where version_no>=?",$no)->num_rows()!=0)
				show_error("Invalid version number : Version number should be in ascending order");
			$inp=array("version_no"=>$no,"version_date"=>time(),"created_by"=>$user['userid']);
			$this->db->insert("pnh_app_versions",$inp);
			$this->erpm->flash_msg("App version added");
			redirect("admin/pnh_app_versions");
		}
		$data['page']="pnh_app_versions";
		$this->load->view("admin",$data);
	}

	function pnh_addtown()
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		if($_POST)
			$this->erpm->do_pnh_addtown();
		$data['page']="pnh_addtown";
		$this->load->view("admin",$data);
	}

	function pnh_towns($tid=false)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		$data['towns']=$this->erpm->pnh_gettowns($tid);
		if($tid!=false)
			$data['terry']=$this->db->query("select territory_name from pnh_m_territory_info where id=?",$tid)->row()->territory_name;
		$data['page']="pnh_towns";
		$this->load->view("admin",$data);
	}

	function pnh_jx_searchdeals()
	{
		$q=$_POST['q'];
		foreach($this->db->query("select i.name,i.pnh_id,i.orgprice as mrp,i.price,i.store_price from king_dealitems i where i.is_pnh=1 and i.name like ?","%$q%")->result_array() as $d)
			echo "<a href=\"javascript:void(0)\" onclick='add_deal_callb(\"{$d['name']}\",\"{$d['pnh_id']}\",\"{$d['mrp']}\",\"{$d['price']}\",\"{$d['store_price']}\")'>{$d['name']}</a>";
	}

	function pnh_jx_loadtown()
	{
		$tid=$_POST['tid'];
		echo "<select name='town'>";
		foreach($this->db->query("select id,town_name from pnh_towns where territory_id=?",$tid)->result_array() as $t)
			echo '<option value="'.$t['id'].'">'.$t['town_name'].'</option>';
		echo "</select>";
	}

	function pnh_print_franchisesbyterritory($tid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE|CALLCENTER_ROLE);
		$data['frans']=$this->erpm->pnh_getfranchisesbyterry($tid);
		$data['pagetitle']="Franchises of territory:".$this->db->query("select territory_name as n from pnh_m_territory_info where id=?",$tid)->row()->n;
		$this->load->view("admin/body/print_franchisesbyterritory",$data);
	}

	function pnh_franchisesbyterritory($tid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE|CALLCENTER_ROLE);
		$data['frans']=$this->erpm->pnh_getfranchisesbyterry($tid);
		$data['pagetitle']="Franchises of territory:".$this->db->query("select territory_name as n from pnh_m_territory_info where id=?",$tid)->row()->n;
		$data['page']="pnh_franchises";
		$this->load->view("admin",$data);
	}

	function pnh_franchisesbytown($tid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE|CALLCENTER_ROLE);
		$data['frans']=$this->erpm->pnh_getfranchisesbytown($tid);
		$data['pagetitle']="Franchises of town:".$this->db->query("select town_name as n from pnh_towns where id=?",$tid)->row()->n;
		$data['page']="pnh_franchises";
		$this->load->view("admin",$data);
	}

	function pnh_franchises()
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE|CALLCENTER_ROLE);
		$data['frans']=$this->erpm->pnh_getfranchises();
		$data['page']="pnh_franchises";
		$this->load->view("admin",$data);
	}

	function pnh_activate_receipt($rid=false)
	{
		$this->auth(FINANCE_ROLE);
		if(!$rid)
			show_error("Input kissing");
		$this->erpm->do_pnh_activate_receipt($rid);
	}

	function pnh_cancel_receipt($rid=false)
	{
		$this->auth(FINANCE_ROLE);
		if(!$rid)
			show_error("Input kissing");
		$this->erpm->do_pnh_cancel_receipt($rid);
	}

	function pnh_disenable_sch($fid,$en)
	{
		$this->auth(PNH_EXECUTIVE_ROLE|FINANCE_ROLE);
		$this->db->query("update pnh_m_franchise_info set is_sch_enabled=? where franchise_id=? limit 1",array($en,$fid));
		redirect("admin/pnh_franchise/$fid#sch_hist");
	}

	function pnh_pending_receipts()
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_POST)
		{
			if($_POST['type']=="act")
				$this->pnh_activate_receipt($_POST['rid']);
			else
				$this->pnh_cancel_receipt($_POST['rid']);
		}
		$data['receipts']=$this->db->query("select r.*,f.franchise_name,a.name as admin from pnh_t_receipt_info r join pnh_m_franchise_info f on f.franchise_id=r.franchise_id left outer join king_admin a on a.id=r.created_by where r.status=0")->result_array();
		$data['page']="pnh_pending_receipts";
		$this->load->view("admin",$data);
	}

	function pnh_download_stat($fid)
	{
		$this->auth(PNH_EXECUTIVE_ROLE|FINANCE_ROLE);
		if($_POST)
			$this->erpm->do_pnh_download_stat($fid);
	}

	function pnh_sms_log($fid=null,$s_from=0,$s_to=0)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE|CALLCENTER_ROLE);
		if($fid==0)
			$fid=null;
		if(!$this->erpm->auth(CALLCENTER_ROLE,true))
			$this->erpm->is_franchise_auth($fid);
		$from=$to=0;
		if($s_from)
		{
			$from=strtotime($s_from);
			$to=strtotime($s_to);
		}
		$data['page']="pnh_sms_log";
		$sql="select concat(f.franchise_name,', ',f.city) as franchise,l.franchise_id,l.sender as from,l.msg as input,o.msg as reply,l.created_on,o.created_on as reply_on from pnh_sms_log l join pnh_m_franchise_info f on f.franchise_id=l.franchise_id left outer join pnh_sms_log o on o.reply_for=l.id where ";
		if($fid)
			$sql.=" l.franchise_id=? and ";
		else $sql.="1 and ";
		$sql.=($from?"l.created_on between $from and $to and ":"")."l.reply_for=0 order by l.id desc".(($fid||$from)?"":" limit 40");
		$data['log']=$this->db->query($sql,$fid)->result_array();
		$data['fid']=$fid;
		$sql="select l.*,concat(f.franchise_name,', ',f.city) as franchise from pnh_sms_log_sent l join pnh_m_franchise_info f on f.franchise_id=l.franchise_id where ";
		if($fid)
			$sql.="l.franchise_id=? and ";
		else $sql.="1 and ";
		$sql.=($from?" l.sent_on between $from and $to ":"1 ")." order by l.id desc".(($fid||$from)?"":" limit 40");
		$data['erp']=$this->db->query($sql,$fid)->result_array();
		$this->load->view("admin",$data);
	}

	function pnh_comp_details()
	{
		$user=$this->auth();
		$data['page']="pnh_comp_details";
		$data['dets']=$this->db->query("select * from pnh_comp_details")->row_array();
		$this->load->view("admin",$data);
	}

	function pnh_franchise($fid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE|CALLCENTER_ROLE);
		if(!$this->erpm->auth(CALLCENTER_ROLE,true))
			$this->erpm->is_franchise_auth($fid);
		$data['fran']=$this->erpm->pnh_getfranchise($fid);
		//		if(!$this->auth(true,true) && $user['userid']!=$fran[''])
		$data['receipts']=$this->db->query("select r.*,a.name as admin,act.name as act_by from pnh_t_receipt_info r left outer join king_admin a on a.id=r.created_by left outer join king_admin act on act.id=r.activated_by where franchise_id=? group by r.receipt_id",$fid)->result_array();
		$data['devices']=$this->db->query("select dm.created_on,di.id,di.device_sl_no,d.device_name from pnh_m_device_info di join pnh_m_device_type d on d.id=di.device_type_id join pnh_t_device_movement_info dm on dm.device_id=di.id where di.issued_to=?",$fid)->result_array();
		$data['page']="pnh_franchise";
		$this->load->view("admin",$data);
	}

	function pnh_special_margins()
	{
		$user=$this->auth(OFFLINE_ORDER_ROLE);
		$data['deals']=$this->db->query("select i.orgprice,i.price,s.from,s.to,i.pnh_id,i.name,i.id,s.special_margin from king_dealitems i join pnh_special_margin_deals s on s.itemid=i.id where ? between s.from and s.to",time())->result_array();
		$data['page']="pnh_special_margins";
		$this->load->view("admin",$data);
	}

	function pnh_sch_discounts($fid=0,$s=0,$e=0)
	{
		$user=$this->auth(true);
		$from=$to=0;
		if($s)
		{
			$from=strtotime($s);
			$to=strtotime("23:59:59 $e");
		}
		$sql="select s.*,f.franchise_name,a.name as created_by from pnh_sch_discount_track s join pnh_m_franchise_info f on f.franchise_id=s.franchise_id left outer join king_admin a on a.id=s.created_by where 1 ";
		$title="Scheme discounts added";
		if($fid)
			$sql.=" and s.franchise_id=?";
		if($from)
			$sql.=" and s.created_on between $from and $to";
		if($fid)
			$title.=" for ".$this->db->query("select franchise_name from pnh_m_franchise_info where franchise_id=?",$fid)->row()->franchise_name;
		if($from)
			$title.=" between $s and $e";
		$data['pagetitle']=$title;
		$data['discs']=$this->db->query($sql,$fid)->result_array();
		$data['page']="pnh_sch_discounts";
		$this->load->view("admin",$data);
	}

	function pnh_bulk_sch_discount()
	{
		$user=$this->auth(true);
		if($_POST)
		{
			foreach(array("discount","start","end","reason","brand","cat","fids") as $i)
				$$i=$this->input->post($i);
			if(empty($fids))
				show_error("No Franchises selected");
			$start=strtotime($start);
			$end=strtotime($end." 23:59:59");
			foreach($fids as $fid)
			{
				$inp=array("franchise_id"=>$fid,"catid"=>$cat,"brandid"=>$brand,"sch_discount"=>$discount,"sch_discount_start"=>$start,"sch_discount_end"=>$end,'reason'=>$reason,"created_by"=>$user['userid'],"created_on"=>time());
				$this->db->insert("pnh_sch_discount_track",$inp);
				if($brand==0 && $cat==0)
					$this->db->query("update pnh_m_franchise_info set sch_discount=?,sch_discount_start=?,sch_discount_end=? where franchise_id=?",array($discount,$start,$end,$fid));
				else
				{
					$inp=array("franchise_id"=>$fid,"discount"=>$discount,"valid_from"=>$start,"valid_to"=>$end,"brandid"=>$brand,"created_on"=>time(),"created_by"=>$user['userid'],"catid"=>$cat);
					$this->db->insert("pnh_sch_discount_brands",$inp);
				}
			}
			$this->erpm->flash_msg("Scheme discount added");
			redirect("admin/pnh_bulk_sch_discount");
		}
		$data['page']="pnh_bulk_sch_discount";
		$this->load->view("admin",$data);
	}

	function pnh_give_sch_discount($fid)
	{
		$user=$this->auth(true);
		foreach(array("discount","start","end","reason","brand","cat") as $i)
			$$i=$this->input->post($i);
		$start=strtotime($start);
		$end=strtotime($end." 23:59:59");
		$inp=array("franchise_id"=>$fid,"catid"=>$cat,"brandid"=>$brand,"sch_discount"=>$discount,"sch_discount_start"=>$start,"sch_discount_end"=>$end,'reason'=>$reason,"created_by"=>$user['userid'],"created_on"=>time());
		$this->db->insert("pnh_sch_discount_track",$inp);
		if($brand==0 && $cat==0)
			$this->db->query("update pnh_m_franchise_info set sch_discount=?,sch_discount_start=?,sch_discount_end=? where franchise_id=?",array($discount,$start,$end,$fid));
		else
		{
			$inp=array("franchise_id"=>$fid,"discount"=>$discount,"valid_from"=>$start,"valid_to"=>$end,"brandid"=>$brand,"created_on"=>time(),"created_by"=>$user['userid'],"catid"=>$cat);
			$this->db->insert("pnh_sch_discount_brands",$inp);
		}
		$this->erpm->flash_msg("Scheme discount added");
		redirect("admin/pnh_franchise/$fid#sch_hist");
	}

	function pnh_loyalty_points()
	{
		$user=$this->auth(true);
		if($_POST)
		{
			foreach(array("amount","points") as $i)
				$$i=$this->input->post("$i");
			$this->db->query("truncate pnh_loyalty_points");
			foreach($amount as $i=>$a)
				if(!empty($a))
				$this->db->query("insert into pnh_loyalty_points(amount,points) values(?,?)",array($a,$points[$i]));
			redirect("admin/pnh_loyalty_points");
		}
		$data['page']="pnh_loyalty_points";
		$this->load->view("admin",$data);
	}

	function pnh_members($fid=0)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE|CALLCENTER_ROLE);
		$data['page']="pnh_members";
		if($fid)
			$data['users']=$this->db->query("select u.*,count(o.transid) as orders,f.franchise_name as fran from pnh_member_info u left outer join king_orders o on o.userid=u.user_id join pnh_m_franchise_info f on f.franchise_id=u.franchise_id where u.franchise_id=? group by u.user_id order by u.first_name desc",$fid)->result_array();
		else
			$data['users']=$this->db->query("select u.*,count(o.transid) as orders,f.franchise_name as fran from pnh_member_info u left outer join king_orders o on o.userid=u.user_id join pnh_m_franchise_info f on f.franchise_id=u.franchise_id group by u.user_id order by u.id desc limit 20")->result_array();
		if($fid)
			$data['pagetitle']=" of '".$this->db->query("select concat(franchise_name,', ',city) as name from pnh_m_franchise_info where franchise_id=?",$fid)->row()->name."'";
		$this->load->view("admin",$data);
	}

	function pnh_viewmember($uid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE|CALLCENTER_ROLE);
		$data['page']="pnh_viewmember";
		$data['member']=$this->db->query("select u.*,count(o.transid) as orders,f.franchise_name as fran from pnh_member_info u left outer join king_orders o on o.userid=u.user_id left outer join pnh_m_franchise_info f on f.franchise_id=u.franchise_id where u.user_id=?",$uid)->row_array();
		$this->load->view("admin",$data);
	}

	function pnh_allot_mid($fid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		$start=$this->input->post("start");
		$end=$this->input->post("end");
		if($this->db->query("select 1 from pnh_m_allotted_mid where (? between mid_start and mid_end) or (? between mid_start and mid_end)",array($start,$end))->num_rows()!=0)
			show_error("Range already allotted to another franchise");
		$this->db->query("insert into pnh_m_allotted_mid(franchise_id,mid_start,mid_end,created_on,created_by) values(?,?,?,?,?)",array($fid,$start,$end,time(),$user['userid']));
		redirect("admin/pnh_franchise/$fid");
	}

	function pnh_franchise_bank_details($fid)
	{
		$this->auth(PNH_EXECUTIVE_ROLE|FINANCE_ROLE);
		if($_POST)
			$this->erpm->do_pnh_add_fran_bank_details($fid);
	}

	function pnh_give_credit()
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		$fid=$this->input->post("fid");
		if($this->input->post("reduce"))
			$_POST['limit']=-$_POST['limit'];
		if(empty($_POST['limit']))
			show_error("Invalid credit limit");
		$f=$this->db->query("select credit_limit from pnh_m_franchise_info where franchise_id=?",$this->input->post("fid"))->row_array();
		$inp=array($this->input->post("fid"),$this->input->post("limit"),$this->input->post("limit")+$f['credit_limit'],$this->input->post("reason"),$user['userid'],$user['userid'],time());
		$this->db->query("insert into pnh_t_credit_info(franchise_id,credit_added,new_credit_limit,reason,credit_given_by,created_by,created_on) values(?,?,?,?,?,?,?)",$inp);
		$this->db->query("update pnh_m_franchise_info set credit_limit=? where franchise_id=?",array($this->input->post("limit")+$f['credit_limit'],$fid));
		redirect("admin/pnh_franchise/$fid");
	}

	function pnh_topup($fid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		if($_POST)
			$this->erpm->do_pnh_topup($fid);
		$data['fid']=$fid;
		$data['page']="pnh_topup";
		$this->load->view("admin",$data);
	}

	function pnh_removefdevice($did,$fid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		$this->db->query("update pnh_m_device_info set issued_to=0 where id=?",$did);
		$this->db->query("insert into pnh_t_device_movement_info(device_id,issued_to,created_by,created_on) values(?,0,?,?)",array($did,$user['userid'],time()));
		redirect("admin/pnh_manage_devices/$fid");
	}

	function pnh_manage_devices($fid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		if($_POST)
			$this->erpm->do_pnh_manage_devices($fid);
		$data['fid']=$fid;
		$data['devs']=$this->db->query("select di.id,di.device_sl_no,d.device_name from pnh_m_device_info di join pnh_m_device_type d on d.id=di.device_type_id where di.issued_to=?",$fid)->result_array();
		$data['page']="pnh_manage_devices";
		$this->load->view("admin",$data);
	}

	function pnh_deals()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE|CALLCENTER_ROLE);
		$data['deals']=$this->erpm->pnh_getdeals();
		$data['page']="pnh_deals";
		$this->load->view("admin",$data);
	}

	function pnh_dealsbycat($catid,$type=0)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE|CALLCENTER_ROLE);
		$data['deals']=$this->erpm->pnh_getdealsbycat($catid,$type);
		$data['page']="pnh_deals";
		$data['brand']=false;
		$data['pagetitle']="PNH Deals by category : ".$this->db->query("select name from king_categories where id=?",$catid)->row()->name;
		$this->load->view("admin",$data);
	}

	function pnh_dealsbybrand($brandid,$type=0)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE|CALLCENTER_ROLE);
		$data['deals']=$this->erpm->pnh_getdealsbybrand($brandid,$type);
		$data['brand']=true;
		$data['page']="pnh_deals";
		$data['pagetitle']="PNH Deals by brand : ".$this->db->query("select name from king_brands where id=?",$brandid)->row()->name;
		$this->load->view("admin",$data);
	}

	function prod_mrp_update($bid=false)
	{
		$user=$this->auth(PRODUCT_MANAGER_ROLE|DEAL_MANAGER_ROLE);
		if($_POST)
		{
			$c=0;
			foreach($this->input->post("pid") as $i=>$pid)
			{
				$mrp=$_POST['mrp'][$i];
				if(empty($mrp))
					continue;
				$c++;
				$pc_prod=$this->db->query("select * from m_product_info where product_id=? and mrp!=?",array($pid,$mrp))->row_array();
				if(!empty($pc_prod))
				{
					$inp=array("product_id"=>$pid,"new_mrp"=>$mrp,"old_mrp"=>$pc_prod['mrp'],"reference_grn"=>0,"created_by"=>$user['userid'],"created_on"=>time());
					$this->db->insert("product_price_changelog",$inp);
					$this->db->query("update m_product_info set mrp=? where product_id=? limit 1",array($mrp,$pid));
					foreach($this->db->query("select product_id from products_group_pids where group_id in (select group_id from products_group_pids where product_id=$pid) and product_id!=$pid")->result_array() as $pg)
					{
						$inp=array("product_id"=>$pg['product_id'],"new_mrp"=>$mrp,"old_mrp"=>$this->db->query("select mrp from m_product_info where product_id=?",$pg['product_id'])->row()->mrp,"reference_grn"=>0,"created_by"=>$user['userid'],"created_on"=>time());
						$this->db->insert("product_price_changelog",$inp);
						$this->db->query("update m_product_info set mrp=? where product_id=? limit 1",array($mrp,$pg['product_id']));
					}
					$r_itemids=$this->db->query("select itemid from m_product_deal_link where product_id=?",$pid)->result_array();
					$r_itemids2=$this->db->query("select l.itemid from products_group_pids p join m_product_group_deal_link l on l.group_id=p.group_id where p.product_id=?",$pid)->result_array();
					$r_itemids=array_unique(array_merge($r_itemids,$r_itemids2));
					foreach($r_itemids as $d)
					{
						$itemid=$d['itemid'];
						$item=$this->db->query("select orgprice,price from king_dealitems where id=?",$itemid)->row_array();
						$o_price=$item['price'];$o_mrp=$item['orgprice'];
						$n_mrp=$this->db->query("select ifnull(sum(p.mrp*l.qty),0) as mrp from m_product_deal_link l join m_product_info p on p.product_id=l.product_id where l.itemid=?",$itemid)->row()->mrp+$this->db->query("select ifnull(sum((select avg(mrp) from m_product_group_deal_link l join products_group_pids pg on pg.group_id=l.group_id join m_product_info p on p.product_id=pg.product_id where l.itemid=$itemid)*(select qty from m_product_group_deal_link where itemid=$itemid)),0) as mrp")->row()->mrp;
						$n_price=$item['price']/$o_mrp*$n_mrp;
						$inp=array("itemid"=>$itemid,"old_mrp"=>$o_mrp,"new_mrp"=>$n_mrp,"old_price"=>$o_price,"new_price"=>$n_price,"created_by"=>$user['userid'],"created_on"=>time(),"reference_grn"=>0);
						$r=$this->db->insert("deal_price_changelog",$inp);
						$this->db->query("update king_dealitems set orgprice=?,price=? where id=? limit 1",array($n_mrp,$n_price,$itemid));
						if($this->db->query("select is_pnh as b from king_dealitems where id=?",$itemid)->row()->b)
						{
							$o_s_price=$this->db->query("select store_price from king_dealitems where id=?",$itemid)->row()->store_price;
							$n_s_price=$o_s_price/$o_mrp*$n_mrp;
							$this->db->query("update king_dealitems set store_price=? where id=? limit 1",array($n_s_price,$itemid));
							$o_n_price=$this->db->query("select nyp_price as p from king_dealitems where id=?",$itemid)->row()->p;
							$n_n_price=$o_n_price/$o_mrp*$n_mrp;
							$this->db->query("update king_dealitems set nyp_price=? where id=? limit 1",array($n_n_price,$itemid));
						}
						foreach($this->db->query("select * from partner_deal_prices where itemid=?",$itemid)->result_array() as $r)
						{
							$o_c_price=$r['customer_price'];
							$n_c_price=$o_c_price/$o_mrp*$n_mrp;
							$o_p_price=$r['partner_price'];
							$n_p_price=$o_p_price/$o_mrp*$n_mrp;
							$this->db->query("update partner_deal_prices set customer_price=?,partner_price=? where itemid=? and partner_id=?",array($n_c_price,$n_p_price,$itemid,$r['partner_id']));
						}
					}
				}
			}
			$this->erpm->flash_msg("MRPs of $c products updated");
			redirect("admin/prod_mrp_update/$bid");
		}
		if($bid)
			$data['prods']=$this->db->query("select product_name as name,mrp,product_id from m_product_info where brand_id=? order by name asc",$bid)->result_array();
		$data['page']="prod_mrp_update";
		$this->load->view("admin",$data);
	}

	function pnh_pub_deal($itemid,$pub)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		$this->db->query("update king_deals set publish=? where dealid=?",array(!$pub,$this->db->query("select dealid from king_dealitems where id=?",$itemid)->row()->dealid));
		$this->session->set_flashdata("erp_pop_info","Deal status changed");
		redirect($_SERVER['HTTP_REFERER']);
	}

	function pnh_class()
	{
		$user=$this->auth(true);
		if($_POST && $this->input->post("new"))
			$this->erpm->do_pnh_add_class();
		elseif($_POST)
		$this->erpm->do_pnh_update_class();
		$data['class']=$this->db->query("select * from pnh_m_class_info order by class_name asc")->result_array();
		$data['page']="pnh_class";
		$this->load->view("admin",$data);
	}

	function pnh_territories()
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		if($_POST && !$this->input->post("edit"))
			$this->erpm->do_pnh_add_territory();
		elseif($_POST)
		$this->erpm->do_pnh_update_territory();
		$data['terrys']=$this->db->query("select * from pnh_m_territory_info order by territory_name asc")->result_array();
		$data['page']="pnh_territories";
		$this->load->view("admin",$data);
	}

	function pnh_device_type()
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		if($_POST && !$this->input->post("edit"))
			$this->erpm->do_pnh_add_device_type();
		elseif($_POST)
		$this->erpm->do_pnh_update_device_type();
		$data['devs']=$this->db->query("select * from pnh_m_device_type order by device_name asc")->result_array();
		$data['page']="pnh_device_type";
		$this->load->view("admin",$data);
	}

	function jx_pnh_getmid()
	{
		$user=$this->auth();
		$mid=$this->input->post("mid");
		if(strlen($mid)!=8 || $mid{0}!=2)
			die("Invalid MID $mid");
		$mem=$this->db->query("select * from pnh_member_info where pnh_member_id=?",$mid)->row_array();
		if(empty($mem))
		{
			$fran=$this->db->query("select f.franchise_id,f.franchise_name as name from pnh_m_allotted_mid a join pnh_m_franchise_info f on f.franchise_id=a.franchise_id where ? between a.mid_start and a.mid_end",$mid)->row_array();
			if(empty($fran))
				die("$mid is not allotted to any franchise");
			if(!isset($_POST['more']))
				die("$mid is assigned to Franchise: <a href='".site_url("admin/pnh_franchise/{$fran['franchise_id']}")."' target='_blank'>{$fran['name']}</a>");
			die("$mid is assigned to Franchise: <a href='".site_url("admin/pnh_franchise/{$fran['franchise_id']}")."' target='_blank'>{$fran['name']}</a> <span style='color:green;margin-left:10px;font-size:120%;float:right;'>NEW MEMBER!</span><div>Member Name : <input type='text' class='inp' name='m_name'> &nbsp; &nbsp;&nbsp; Mobile No : <input type='text' class='inp' maxlength=10 name='m_mobile'></div>");
		}
		else
		{
			$mem['first_name']=($mem['first_name']==""&&$mem['last_name']=="")?"No Name":$mem['first_name'];
			$msg="<div style='font-size:120%;'>";
			$order=$this->db->query("select count(1) as n,sum(amount) as t from king_transactions where transid in (select transid from king_orders where userid=?)",$mem['user_id'])->row_array();
			$msg.="Member Name : <a href='".site_url("admin/pnh_viewmember/{$mem['user_id']}")."' target='_blank'>{$mem['first_name']} {$mem['last_name']}</a> &nbsp;&nbsp;&nbsp; Total Orders : {$order['n']} &nbsp;&nbsp;&nbsp; Total Amount : Rs {$order['t']} &nbsp;&nbsp;&nbsp; Loyalty points : {$mem['points']}";
			$msg.="<div class='clear'></div></div>";
			die($msg);
		}
	}

	function pnh_order_import()
	{
		$user=$this->auth(FINANCE_ROLE);
		if($_FILES)
			$this->erpm->do_pnh_order_import();
		$data['page']="pnh_order_import";
		$this->load->view("admin",$data);
	}

	function pnh_upload_images($fid)
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE);
		if($_FILES)
			$this->erpm->do_pnh_upload_images($fid);
		$data['page']="pnh_upload_images";
		$this->load->view("admin",$data);
	}

	function jx_pnh_checkmememail()
	{
		$e=$this->input->post("email");
		if($this->db->query("select 1 from pnh_member_info where email=?",$e)->num_rows()!=0)
			die("0");
		else
			die("1");
	}

	function jx_pnh_checkmemmob()
	{
		$e=$this->input->post("mob");
		$m=$this->input->post("mid");
		if($this->db->query("select 1 from pnh_member_info where mobile=? and pnh_member_id!=?",array($e,$m))->num_rows()!=0)
			die("0");
		else
			die("1");
	}

	function pnh_addmember()
	{
		$user=$this->auth(PNH_EXECUTIVE_ROLE|CALLCENTER_ROLE);
		if($_POST)
			$this->erpm->do_pnh_addmember();

		$data['page']="pnh_add_member";
		$this->load->view("admin",$data);
	}

	function pnh_cash_bill($transid)
	{
		$user=$this->auth();
		$data['bill']=$b=$this->db->query("select * from pnh_cash_bill where transid=?",$transid)->row_array();
		if(empty($b))
			show_error("Cash Bill not found");
		$data['fran']=$this->db->query("select * from pnh_m_franchise_info where franchise_id=?",$b['franchise_id'])->row_array();
		$data['member']=$this->db->query("select * from pnh_member_info where user_id=?",$b['user_id'])->row_array();
		$data['orders']=$this->db->query("select o.*,i.name as product from king_orders o join king_dealitems i on i.id=o.itemid where o.transid=? and o.status!=2",$transid)->result_array();
		$data['page']="pnh_cash_bill";
		$this->load->view("admin/body/pnh_cash_bill",$data);
	}

	function create_cash_bill()
	{
		$trans=$this->db->query("select * from king_transactions where franchise_id!=0")->result_array();
		foreach($trans as $t)
		{
			$transid=$t['transid'];
			$userid=$this->db->query("select userid from king_orders where transid=?",$transid)->row()->userid;
			$franid=$t['franchise_id'];
			$billno=10001;
			$nbill=$this->db->query("select bill_no from pnh_cash_bill where franchise_id=? order by bill_no desc limit 1",$franid)->row_array();
			if(!empty($nbill))
				$billno=$nbill['bill_no']+1;
			$inp=array("bill_no"=>$billno,"franchise_id"=>$franid,"transid"=>$transid,"user_id"=>$userid,"status"=>1);
			$this->db->insert("pnh_cash_bill",$inp);
		}
	}

	function pnh_member_card_batch()
	{
		$user=$this->auth(true);
		if($_POST)
		{
			$n=$this->input->post("n");
			$data=$this->db->query("select pnh_member_id as mid,if(salute=0,'Mr',if(salute=1,'Mrs','Ms')) as salute,first_name,last_name from pnh_member_info where is_card_printed=0 and created_on!=0 order by id asc limit $n")->result_array();
			if(empty($data))
				show_error("No pending member cards to be generated");
			$this->db->query("update pnh_member_info set is_card_printed=1 where is_card_printed=0 and created_on!=0 order by id asc limit $n");
			$this->erpm->export_csv("member_card_results",$data,true);
		}
		$data['page']="pnh_member_card_batch";
		$this->load->view("admin",$data);
	}

	function pnh_deal_extra_images($id)
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		if($_FILES)
			$this->erpm->do_pnh_deal_extra_images($id);
		$data['page']="pnh_deal_extra_images";
		$this->load->view("admin",$data);
	}

	function pnh_deal($id)
	{
		$user=$this->auth();
		$data['deal']=$this->db->query("select i.gender_attr,d.catid,d.brandid,d.tagline,i.nyp_price,i.pnh_id,i.id,b.name as brand,c.name as category,i.name,i.pic,i.orgprice,i.price,i.store_price,d.description,d.publish from king_dealitems i join king_deals d on d.dealid=i.dealid join king_brands b on b.id=d.brandid join king_categories c on c.id=d.catid where i.id=? or i.pnh_id=?",array($id,$id))->row_array();
		$data['prods']=$this->db->query("select p.product_id,p.product_name,l.qty from m_product_deal_link l join m_product_info p on p.product_id=l.product_id where l.itemid=?",$id)->result_array();
		$data['page']="pnh_deal";
		$this->load->view("admin",$data);
	}

	function pnh_update_description()
	{
		$user=$this->auth(true);
		if($_FILES)
		{
			$f=@fopen($_FILES['csv']['tmp_name'],"r");
			$head=fgetcsv($f);
			$template=array("Item ID","description");
			if(empty($head) || count($head)!=count($template))
				show_error("Invalid template structure".count($head));
			$payload=array();
			while(($data=fgetcsv($f))!=false)
			{
				if(count($data)!=2)
					show_error("Invalid template structure");
				$deal=$this->db->query("select dealid from king_dealitems where id=? and is_pnh=1",$data[0])->row_array();
				if(empty($deal))
					show_error("Invalid Item ID {$data[0]} or its not a PNH deal");
				$inp=array("dealid"=>$deal['dealid'],"description"=>$data[1]);
				$payload[]=$inp;
			}
			foreach($payload as $p)
				$this->db->query("update king_deals set description=? where dealid=? limit 1",array($p['description'],$p['dealid']));
			$this->erpm->flash_msg(count($payload)." deals updated");
			redirect("admin/pnh_update_description");
		}
		$data['page']="pnh_update_description";
		$this->load->view("admin",$data);
	}

	function pnh_sms_campaign()
	{
		$user=$this->auth(true);
		if($_POST)
		{
			foreach(array("fids","send_to","msg") as $i)
				$$i=$this->input->post($i);
			if(empty($fids))
				show_error("No franchises were selected");
			if(empty($msg))
				show_error("No message to send");
			foreach($this->db->query("select login_mobile1,franchise_id,login_mobile2 from pnh_m_franchise_info where franchise_id in ('".implode("','",$fids)."')")->result_array() as $f)
			{
				$this->erpm->pnh_sendsms($f['login_mobile1'],$msg,$f['franchise_id']);
				if($send_to==1 && !empty($f['login_mobile2']))
					$this->erpm->pnh_sendsms($f['login_mobile2'],$msg,$f['franchise_id']);
			}
			$this->erpm->flash_msg("SMS sent");
			redirect("admin/pnh_sms_campaign");
		}
		$data['frans']=$this->erpm->pnh_getfranchises();
		$data['page']="pnh_sms_campaign";
		$this->load->view("admin",$data);
	}

	function pnh_catalogue()
	{
		$user=$this->auth(true);
		$data=$this->db->query("select group_concat(p.product_name order by l.id asc separator '||') as prods_name, group_concat(l.qty order by l.id asc separator '||') as prods_qty,i.name,i.orgprice as mrp,i.price,i.pnh_id,b.name as brand,d.brandid from king_deals d join king_dealitems i on i.dealid=d.dealid join king_brands b on b.id=d.brandid join m_product_deal_link l on l.itemid=i.id join m_product_info p on p.product_id=l.product_id where d.publish=1 and i.is_pnh=1 group by i.id order by b.name asc,i.name asc")->result_array();
		$payload=array();
		foreach($data as $d)
		{
			if(!isset($payload[$d['brandid']]))
				$payload[$d['brandid']]=array();
			$payload[$d['brandid']][]=$d;
		}
		$this->load->library("pdf");
		$this->pdf->doc_type("Product Catalogue");
		$this->pdf->AliasNbPages();
		$this->pdf->AddPage();
		$this->pdf->Image("images/paynearhome.jpg",78,110);
		$this->pdf->SetFont('Arial','B',30);
		$this->pdf->Cell(0,150,"Product Catalogue",0,1,"C");
		$this->pdf->SetFont('Arial','B',15);
		$this->pdf->SetY(155);
		$this->pdf->Cell(0,0,date("d/m/y"),0,0,"C");
		$this->pdf->SetFont('Arial','',10);
		$this->pdf->SetY(-35);
		$this->pdf->Cell(0,0,"To place an order",0,0,"R");
		$this->pdf->SetFont('Arial','B',10);
		$this->pdf->SetY(-30);
		$this->pdf->Cell(0,0,"Call 1800 200 1996",0,0,"R");
		$this->pdf->SetY(-25);
		$this->pdf->Cell(0,0,"hello@paynearhome.in",0,0,"R");
		foreach($payload as $pl)
		{
			$this->pdf->AddPage();
			$this->pdf->SetFont('Arial','B',17);
			$this->pdf->SetFillColor(200,220,255);
			$this->pdf->Cell(0,10,$pl[0]['brand'],0,1,'L',true);
			$this->pdf->SetDrawColor(200,200,200);
			$this->pdf->Cell(0,1,"","B",1);
			foreach($pl as $p)
			{
				$this->pdf->SetFont('Arial','B',9);
				$this->pdf->ln(2);
				$this->pdf->MultiCell(120,3,ucfirst($p['name']),0,'L');
				$this->pdf->SetX(135);
				$this->pdf->SetFont('Arial','B',8);
				$this->pdf->Cell(40,-2,"MRP : Rs ".$p['mrp']);
				$this->pdf->Cell(20,-2,"Offer : Rs ".$p['price'],0,1);
				$this->pdf->Cell(0,4,"",0,1);
				$prods=explode("||",$p['prods_name']);
				$qtys=explode("||",$p['prods_qty']);
				$this->pdf->SetFont('Arial','B',10);
				$this->pdf->Cell(60,5,$p['pnh_id']);
				$this->pdf->SetFont('Arial','',7);
				foreach($prods as $i=>$prod)
					$this->pdf->Cell(0,4,"{$prod}     x{$qtys[$i]}",0,1,'R');
				$this->pdf->Cell(0,4,"","B",1);
			}
		}
		$this->pdf->Output("products_catalogue_".date("d-m-y").".pdf","D");
	}

	function jx_deals_report_prod()
	{
		$p=$_POST['p'];
		$data['prods']=$this->db->query("$p")->result_array();
		$this->load->view("admin/body/deals_report_frag_prod",$data);
	}

	function pnh_less_margin_brands()
	{
		$user=$this->auth(true);
		if($_POST)
		{
			$this->db->query("truncate table pnh_less_margin_brands");
			foreach($this->input->post("bids") as $b)
				$this->db->query("insert into pnh_less_margin_brands(brandid,created_on,created_by) values(?,?,?)",array($b,time(),$user['userid']));
			$this->erpm->flash_msg("Less margin brands marked");
			redirect("admin/pnh_less_margin_brands");
		}
		$data['page']="pnh_less_margin_brands";
		$this->load->view("admin",$data);
	}

	function pnh_unsuspend_fran($fid)
	{
		$user=$this->auth(true);
		$fran=$this->db->query("select * from pnh_m_franchise_info where franchise_id=?",$fid)->row_array();
		if(empty($fran))
			show_error("No franchise found");
		$this->db->query("update pnh_m_franchise_info set is_suspended=0,suspended_on=".time().",suspended_by={$user['userid']} where franchise_id=? limit 1",$fid);
		$this->erpm->flash_msg("Franchise unsuspended");
		$this->erpm->send_admin_note("Franchise account : {$fran['franchise_name']} ({$fran['pnh_franchise_id']}) was unsuspended on ".date("g:ia d/m/y")." by {$user['username']}","Franchise account unsuspension");
		redirect("admin/pnh_franchise/$fid");
	}

	function pnh_suspend_fran($fid)
	{
		$user=$this->auth(true);
		$fran=$this->db->query("select * from pnh_m_franchise_info where franchise_id=?",$fid)->row_array();
		if(empty($fran))
			show_error("No franchise found");
		$this->db->query("update pnh_m_franchise_info set is_suspended=1,suspended_on=".time().",suspended_by={$user['userid']} where franchise_id=? limit 1",$fid);
		$this->erpm->flash_msg("Franchise suspended");
		$this->erpm->send_admin_note("Franchise account : {$fran['franchise_name']} ({$fran['pnh_franchise_id']}) was suspended on ".date("g:ia d/m/y")." by {$user['username']}","Franchise account suspension");
		redirect("admin/pnh_franchise/$fid");
	}

	function jx_deals_report()
	{
		$p=$_POST['p'];
		$data['deals']=$this->db->query("$p")->result_array();
		$this->load->view("admin/body/deals_report_frag",$data);
	}

	function clear_dealsrep_cache()
	{
		$this->load->library("pettakam",array("repo"=>"cache","ext"=>"pkm_snp"));
		$this->pettakam->clear("deals_report");
		redirect("admin/deals_report");
	}

	function deals_report()
	{
		$user=$this->auth(DEAL_MANAGER_ROLE);
		$this->load->library("pettakam",array("repo"=>"cache","ext"=>"pkm_snp"));
		$data['page']="deals_report";
		$this->load->view("admin",$data);
	}

	function pnh_shipment_sms_notify($fid=false)
	{
		$user=$this->auth(true);
		if($_POST)
		{
			if($this->input->post("start"))
			{
				$fid=$this->input->post("fid");
				$start=$this->input->post("start");
				$end=$this->input->post("end");
			}else
			{
				$fid=$this->input->post("fid");
				$transids=$this->input->post("transid");
				$temp=$this->input->post("template");
				$fran=$this->db->query("select * from pnh_m_franchise_info where franchise_id=?",$fid)->row_array();
				foreach($transids as $t)
				{
					$tran=$this->db->query("select o.userid from king_orders o where o.transid=? limit 1",$t)->row_array();
					$m=$this->db->query("select * from pnh_member_info where user_id=?",$tran['userid']);
					if(empty($m['mobile']))
						continue;
					$temp=str_ireplace("%transid%", $t,$temp);
					$temp=str_ireplace("%mname%", $m['first_name']." ".$m['last_name'] ,$temp);
					$temp=str_ireplace("%fname%", $fran['franchise_name'],$temp);
					$this->erpm->pnh_send_sms($m['mobile'],$temp);
				}
			}
		}
		if($fid)
		{
			$start=strtotime($start);
			$end=strtotime($end);
			$data['orders']=$this->db->query("select t.transid,m.first_name,m.last_name,m.mobile as member_mobile,f.franchise_name,f.franchise_id,o.actiontime from king_transactions t join pnh_m_franchise_info f on f.franchise_id=t.franchise_id join king_orders o on o.transid=t.transid join pnh_member_info m on m.user_id=o.userid where t.franchise_id=? and t.init between ? and ? group by t.transid order by o.actiontime desc",array($fid,$start,$end))->result_array();
			$data['fran']=$this->db->query("select * from pnh_m_franchise_info where franchise_id=?",$fid)->row_array();
		}
		$data['page']="pnh_shipment_sms_notify";
		$this->load->view("admin",$data);
	}

	function changepasswd()
	{
		$user=$this->auth();
		if($_POST)
		{
			$p=$this->input->post("p");
			$cp=$this->input->post("cp");
			if($p!=$cp)
				show_error("Passwords are not same");
			if(strlen($p)<6)
				show_error("Password should be atleast 6 characters length");
			$this->db->query("update king_admin set password=?  where id=? limit 1",array(md5($p),$user['userid']));
			$this->erpm->flash_msg("Password changed");
			redirect("admin/dashboard");
		}
		$data['page']="changepasswd";
		$this->load->view("admin",$data);
	}

	function pnh_update_call_log($lid)
	{
		$this->auth();
		$this->db->query("update pnh_call_log set msg=? where id=? limit 1",array($this->input->post("msg"),$lid));
		redirect($_SERVER['HTTP_REFERER']);
	}

	function makeacall()
	{
		$user=$this->auth(CALLCENTER_ROLE,true);
		if($user==false)
			die("0");
		foreach(array("agent","customer") as $i)
			$$i=$this->input->post("$i");
		$franmob=substr($customer,1);
		$fran=$this->db->query("select franchise_id from pnh_m_franchise_info where (login_mobile1=$franmob && login_mobile1!='') or (login_mobile2=$franmob && login_mobile2!='')")->row_array();
		if(!empty($fran))
			$this->db->insert("pnh_call_log",array("franchise_id"=>$fran['franchise_id'],"created_by"=>$user['userid'],"created_on"=>time()));
		$post_data = array(
				'From' => "$agent",
				'To' => "$customer",
				'CallerId' => "09243404342",
				'CallType' => "trans"
		);
		$this->session->set_userdata("agent_mobile",$agent);

		$exotel_sid = "snapittoday"; // Your Exotel SID
		$exotel_token = "491140e9fbe5c507177228cf26cf2f09356e042c"; // Your exotel token
			
		$url = "https://".$exotel_sid.":".$exotel_token."@twilix.exotel.in/v1/Accounts/".$exotel_sid."/Calls/connect";
			
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FAILONERROR, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
		$http_result = curl_exec($ch);
		$error = curl_error($ch);
		$http_code = curl_getinfo($ch ,CURLINFO_HTTP_CODE);
		curl_close($ch);
		die("1");
	}

	/**
	 * PNH Employee Module
	 * @author roopashree@localcircle.in
	 */

	/**
	 * function to check if employee is already registered
	 *
	 * @return boolean
	 */
//-----------------------------------------EMPLOYEE ADD --------------------------------------------------------------------------
	function _validate_newemployee()
	{
		$contact_no=$this->input->post('contact_no');
		$email=$this->input->post('email_id');

		$sql="select count(*) as status from m_employee_info where (contact_no=? or email=?) ";

		$is_avail = $this->db->query($sql,array($contact_no,$email))->row()->status;

		if($is_avail > 0)
		{
			$this->form_validation->set_message('_validate_newemployee','Employee already registered');
			return false;
		}
		return true;
	}
	function _check_validbuex()
	{
		$town=$this->input->post('town');
		$role_id=$this->input->post('role_id');
		
		if(!isset($town) && $role_id==5)
		{
			$this->form_validation->set_message('_check_validbex','Towns need to be Assigned');
			return false;
		}
		return true;
	}
	

	/**
	 * function to create thumbnail
	 */
	function _createThumbnail()
	{
		$user=$this->auth(PNH_EMPLOYEE);
		$config['image_library']= 'gd';
		$config['source_image']= './resources/employee_assets/image/';
		$config['create_thumb']= TRUE;
		$config['maintain_ratio']= TRUE;
		$config['width']= 75;
		$config['height']=75;

		$this->image_lib->initialize($config);

		$this->image_lib->resize();

		if(!$this->image_lib->resize())
			echo $this->image_lib->display_errors();
	}
	/**
	 * Add Employee
	 */
	
	function add_employee()
	{
		$user=$this->auth();
		$role_id=$this->get_jobrolebyuid($user['userid']);
		if($role_id<=3)
		{
			$access_roles = $this->erpm->get_emp_access_roles();
			$roles_list=$this->erpm->getRolesList();
			$data['access_roles']=$access_roles;
			$data['roles_list']=$roles_list;
			$data['page']='add_emp';
			$this->load->view("admin",$data);
		}
	
	}

	/**
	 * process add_employee employee form details
	 *
	 */
	function process_addemployee()
	{
		
		$user=$this->auth();
		$role_id=$this->get_jobrolebyuid($user['userid']);
		if($role_id<=3)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('emp_name','Name','required');
			$this->form_validation->set_rules('father_name','father_name');
			$this->form_validation->set_rules('mother_name','mother_name');
			$this->form_validation->set_rules('edu','Education');
			$this->form_validation->set_rules('dob','D.O.B');
			$this->form_validation->set_rules('gender','Gender','required');
			$this->form_validation->set_rules('postcode','PostCode','required');
			$this->form_validation->set_rules('city','City','required');
		//	$this->form_validation->set_rules('email_id','Email','trim|required|max_length[128]|valid_email|is_unique');
			$this->form_validation->set_rules('address','address','required');
			$this->form_validation->set_rules('contact_no','Phone Number','required');
			$this->form_validation->set_rules('role_id','Job Title','required');
			$this->form_validation->set_rules('assigned_under_id','Assigned Under','required');
			
	
			if($this->form_validation->run() == false)
			{
				$this->add_employee();
			}
			else
			{
	
				$config['upload_path'] = 'resources/employee_assets/image';
				$config['allowed_types'] ='jpg|jpeg|png';
				$config['max_size']	= '2000';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
	
				$this->load->library('upload');
	
				$this->upload->initialize($config);
	
				if($this->upload->do_upload('image'))
				{
					$data = array('upload_data' => $this->upload->data());
					$fdata=$this->upload->data();
					$image_url=$fdata['file_name'];
				}
				else
				{
					$image_url=" ";
				}
	
				$config1['upload_path'] ='resources/employee_assets/cv';
				$config1['allowed_types'] ='doc|pdf|txt';
				$config1['max_size']	= '40000';
	
				$this->upload->initialize($config1);
	
				if($this->upload->do_upload('cv'))
				{
					$data1 = array('upload_data' => $this->upload->data());
					$fdata1=$this->upload->data();
					$cv_url=$fdata1['file_name'];
				}
				else
				{
					$cv_url=" ";
				}
					
				$contact_no_list=$this->input->post('contact_no');
				
				$contact_no = implode(',',$contact_no_list);
				
				$email=$this->input->post('email_id');
				$assigned_under_id=$this->input->post('assigned_under_id');
				$role_id=$this->input->post('role_id');
				$town_ids = $this->input->post('town');
				$territory_ids = $this->input->post('territory');
					
				// prepare employee info for table insert
				$ins_data = array();
				$ins_data['name']=$name=trim($this->input->post('emp_name'));
				$ins_data['fathername']=trim($this->input->post('father_name'));
				$ins_data['mothername']=trim($this->input->post('mother_name'));
				$ins_data['qualification']=trim($this->input->post('edu'));
				$ins_data['dob']=$this->input->post('dob');
				$ins_data['email']=trim($this->input->post('email_id'));
				$ins_data['address']=trim($this->input->post('address'));
				$ins_data['city']=trim($this->input->post('city'));
				$ins_data['postcode']=$this->input->post('postcode');
				$ins_data['contact_no']=$contact_no;
				$ins_data['gender']=$this->input->post('gender');
				$ins_data['job_title']=$this->input->post('role_id');
				$ins_data['cv_url']=$cv_url;
				$ins_data['photo_url']=$image_url;
				$ins_data['created_on']=date('Y-m-d H:i:s');
				$ins_data['created_by']=1;
				$ins_data['assigned_under']=$this->input->post('assigned_under_id');
				
							
				$this->db->insert('m_employee_info',$ins_data);
					
				$emp_id=$this->db->insert_id();
					
				// updated employee role info
				$this->db->query("insert into m_employee_rolelink(employee_id,parent_emp_id,is_active,assigned_on)values(?,?,'1',now())",array($emp_id,$assigned_under_id));
					
				//For Bussiness Executive Towns need to  be selected
	
				
				// reset  town ids if none of towns selected
				if(!$town_ids)
					$town_ids=array(0);
				if(!$territory_ids)
					$territory_ids=array(0);
	
				// add multiple territories
				 
				foreach ($territory_ids as $tr_id)
				{
					foreach ($town_ids as $tw_id)
					{
						$this->db->query("insert into m_town_territory_link(parent_emp_id,employee_id,territory_id,town_id,is_active,created_on)values(?,?,?,?,'1',now())",array($assigned_under_id,$emp_id,$tr_id,$tw_id));
					}
				}
				// inser into king admin : uid update uid in empl info where empid =  $emp_id
			
				$username=$this->input->post('user_name');
				if($username)
				{
					$access = 'EMP_MANAGE_TASK';
					$password=randomChars(6);
					$name=$this->input->post('emp_name');
					$this->db->query("insert into king_admin(user_id,username,name,email,access,password,createdon) values(?,?,?,?,?,?,now())",array(md5($username),$username,$name,$email,$access,md5($password)));
					$uid = $this->db->insert_id();
					$this->db->query("update m_employee_info set user_id = ? where employee_id = ? ",array($uid,$emp_id));
	
				}
				
				$this->erpm->flash_msg("Employee Details Added");
				redirect('admin/list_employee');
			}
		}
	}
     /**
      * To get superior names by role_id
      * @param unknown_type $role_id
      */
    
	function get_superior_names($role_id)
	{
		$output = array();

		$role_id = $role_id-1;

		$emp_list = $this->erpm->get_empbyroleid($role_id);

		if($emp_list)
		{
			$output['status'] = 'success';
			$output['emp_list'] = $emp_list;
		}
		else
		{
			$output['status'] = 'error';
			$output['message'] = 'No superior employees found';
		}

		echo json_encode($output);

	}
//----------------------------------------------END OF ADD EMPLOYEE-------------------------------------------------------------------------------	

//-----------------------------------------------EDIT EMPLOYEE-----------------------------------------------------------------------
	/**
	 * Function to edit
	 * @param unknown_type $emp_id
	 */
	function edit_employee($emp_id='')
	{
		//$user=$this->auth();
		$user = $this->auth_pnh_employee();
		$role_id=$this->get_jobrolebyuid($user['userid']);
		if($role_id<=3)
		{
			$emp_details=$this->erpm->get_empinfo($emp_id);
			if(!$emp_details)
			{
				show_error("Employee Details not Found");
			}
			$data['emp_details']=$emp_details;
			$data['page']='edit_emp';
			$this->load->view('admin',$data);
		}
	}
	/**
	 *
	 * @param unknown_type $emp_id
	 */
	function process_editemployee($emp_id)
	{
		$user = $this->auth_pnh_employee();
		$role_id=$this->get_jobrolebyuid($user['userid']);
		if($role_id<=3)
		{
			$this->load->library('form_validation');
	
			$this->form_validation->set_rules('emp_name','Name','required');
			$this->form_validation->set_rules('gender','Gender','required');
		//	$this->form_validation->set_rules('email_id','Email','required');
			$this->form_validation->set_rules('address','address','required');
			$this->form_validation->set_rules('city','City','required');
			$this->form_validation->set_rules('postcode','Postcode','required');
			$this->form_validation->set_rules('contact_no','Phone Number','required');
	
			if($this->form_validation->run()==false)
			{
				$this->edit_employee($emp_id);
			}
			else
			{
	            $config['upload_path'] = 'resources/employee_assets/image';
				$config['allowed_types'] ='jpg|jpeg|png';
				$config['max_size']	= '2000';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
	
				$this->load->library('upload');
	
				$this->upload->initialize($config);
	
				if($this->upload->do_upload('image'))
				{
					$data = array('upload_data' => $this->upload->data());
					$fdata=$this->upload->data();
					$image_url=$fdata['file_name'];
					
				}
				else
				{
					$image_url=" ";
				}
	
				$config1['upload_path'] ='resources/employee_assets/cv';
				$config1['allowed_types'] ='doc|pdf|txt';
				$config1['max_size']	= '40000';
	
				$this->upload->initialize($config1);
	
				if($this->upload->do_upload('cv'))
				{
					$data1 = array('upload_data' => $this->upload->data());
					$fdata1=$this->upload->data();
					$cv_url=$fdata1['file_name'];
				}
				else
				{
					$cv_url=" ";
				}
				
				$assigned_under_id = $this->input->post('assigned_under_id');
				
				$contact_no_list=$this->input->post('contact_no');
					
				$contact_no = implode(',',$contact_no_list);
				
	
				//insert the data into table
				$ins_data = array();
	
				$ins_data['name']=$this->input->post('emp_name');
				$ins_data['fathername']=$this->input->post('father_name');
				$ins_data['mothername']=$this->input->post('mother_name');
				$ins_data['qualification']=$this->input->post('edu');
				$ins_data['dob']=$this->input->post('dob');
				$ins_data['email']=$this->input->post('email_id');
				$ins_data['address']=$this->input->post('address');
				$ins_data['city']=$this->input->post('city');
				$ins_data['postcode']=$this->input->post('postcode');
				$ins_data['contact_no']=$contact_no;
				$ins_data['gender']=trim($this->input->post('gender'));
				$ins_data['job_title']=$this->input->post('role_id');
				$ins_data['assigned_under']=$assigned_under_id;
					
	
				if($cv_url)
					$ins_data['cv_url']=$cv_url;
				if($image_url)
					$ins_data['photo_url']=$image_url;
	
				$ins_data['modified_on']=date('Y-m-d H:i:s');
				$ins_data['modified_by']=1;
				$this->db->where('employee_id',$emp_id);
				$this->db->update('m_employee_info',$ins_data);
	
				//check if emp tree avail for empid
	
				$ins_assignment = 1;
				$parent_emp_id = $this->db->query('select IFNULL(SUM(parent_emp_id),0) AS parent_emp_id  from m_employee_rolelink where employee_id = ? and is_active = 1 ',$emp_id)->row()->parent_emp_id;
				if($parent_emp_id)
				{
					if($parent_emp_id == $assigned_under_id)
					{
						$ins_assignment = 0;
					}
					else
					{
						$this->db->query('update m_employee_rolelink set is_active = 0,modified_on=now() where is_active = 1 and employee_id = ? and parent_emp_id = ? ',array($emp_id,$parent_emp_id));
	
					}
				}
				if($ins_assignment)
					$this->db->query("insert into m_employee_rolelink (employee_id,parent_emp_id,is_active,assigned_on) values (?,?,1,now())",array($emp_id,$assigned_under_id));
					$rolelink_id=$this->db->insert_id();
			}
	
			// add_employee to emp tree
				
			$town_ids = $this->input->post('town');
			$territory_ids = $this->input->post('territory');
			//set condition for bussiness executive	
			
			if(!$town_ids && $role_id=5)
			{
				$this->erpm->flash_msg('Towns need to alloted');
			}
			
	        //reset towns & territories if empty
	         
			if(!$town_ids)
			{
				$town_ids=array(0);
			}
			if(!$territory_ids)
			{
				$territory_ids=array(0);
			}
			
			//update town territory ids
			$modified_on = date('Y-m-d H:i:s');
			$prev_terr_link = $this->db->query("SELECT GROUP_CONCAT(territory_id*1) AS tr_ids FROM m_town_territory_link WHERE employee_id = ? AND is_active = 1;",$emp_id)->row()->tr_ids;
			
			$this->db->query("update m_town_territory_link set is_active = 0,modified_on=? where is_active = 1 and employee_id = ? ",array($modified_on,$emp_id));
			
			foreach($territory_ids as $territory_id)
			{
				
					foreach($town_ids as $town_id)
					{
						if(!$this->db->query("select count(*) as t from m_town_territory_link where employee_id = ? AND  territory_id=? and town_id = ? and is_active = 0 and modified_on = ? ",array($emp_id,$territory_id,$town_id,$modified_on))->row()->t)
						{
							$this->db->query('insert into m_town_territory_link(parent_emp_id,employee_id,territory_id,town_id,is_active,modified_on)values(?,?,?,?,1,now())',array($assigned_under_id,$emp_id,$territory_id,$town_id));
						}
						else
						{
							$this->db->query("update m_town_territory_link set is_active = 1,modified_on=now() where is_active = 0 and employee_id = ?  AND  territory_id=? and town_id= ? and modified_on = ? ",array($emp_id,$territory_id,$town_id,$modified_on));
						}
					}
			}
			
			$prev_terr_link_arr = explode(',',$prev_terr_link);
			foreach ($prev_terr_link_arr as $e_trid)
			{
				if(!in_array($e_trid, $territory_ids))
				{
					// unlink suboridantes of employee id by terr id
					$this->db->query("update m_town_territory_link set parent_emp_id = 0,is_active = 0 where parent_emp_id = ? and territory_id = ? ",array($emp_id,$e_trid));
				}
			}
			
			$this->erpm->flash_msg("Employee Details Updated");
			redirect('admin/list_employee');
		}
		
	}

		
	/**
	 * function to get superior emp list by role_id
	 *
	 * @param unknown_type $role_id
	 */
	function get_superior_emplist($role_id)
	{
		$output = array();

		$superior_role_id = $role_id-1;

		$emp_list = $this->common->get_empbyroleid($superior_role_id);
		if($emp_list)
		{
			$output['status'] = 'success';
			$output['emp_list'] = $emp_list;
		}
		else
		{
			$output['status'] = 'error';
			$output['message'] = '';
		}

		echo json_encode($output);
	}
	
	function suggest_territories($emp_id=0,$emp_id1=0)
	{
		$role_id = $this->db->query("select job_title from m_employee_info where employee_id = ? ",$emp_id)->row()->job_title;
	
		// if business head
		if($role_id == 2)
		{
			$sql = ("(SELECT  a.id,a.territory_name
					FROM  pnh_m_territory_info a
					LEFT JOIN m_town_territory_link  b ON a.id = b.territory_id and b.is_active=1
					WHERE b.id  IS NULL
					ORDER BY a.territory_name)UNION
					(SELECT a.id,a.territory_name
					FROM  pnh_m_territory_info a
					JOIN m_town_territory_link  b ON a.id = b.territory_id and b.is_active=1
					 and  b.employee_id='$emp_id1')
					union
					(SELECT t.id AS territory_id,t.territory_name
					FROM `m_town_territory_link` a
					JOIN `m_employee_info` b ON a.employee_id = b.employee_id
					JOIN pnh_m_territory_info t ON t.id = a.territory_id
					LEFT JOIN `m_town_territory_link` c ON c.territory_id =  a.territory_id AND  c.is_active = 1 AND a.id != c.id
					WHERE a.is_active = 1 AND c.id IS NULL AND b.job_title > 3
					ORDER BY b.job_title)");
	
					
	
		}
		//if Manager
			else if($role_id == 3)
			{
			$sql = ('(SELECT  c.id,c.territory_name,d.job_title
					FROM `pnh_m_territory_info`c
					JOIN  m_town_territory_link a ON c.id=a.territory_id AND a.employee_id = '.$emp_id.' AND a.is_active=1
						JOIN `m_employee_info`d ON d.employee_id=a.employee_id
					LEFT JOIN m_town_territory_link b ON b.territory_id=a.territory_id AND b.employee_id != '.$emp_id.' AND b.is_active=1
						WHERE  b.territory_id IS NULL)
					UNION
					(SELECT  c.id,c.territory_name,d.job_title
					FROM `pnh_m_territory_info`c
					JOIN  m_town_territory_link a ON c.id=a.territory_id AND a.employee_id = '.$emp_id1.' AND a.is_active=1
						JOIN `m_employee_info`d ON d.employee_id=a.employee_id
					LEFT JOIN m_town_territory_link b ON b.territory_id=a.territory_id AND b.employee_id != '.$emp_id1.' AND b.is_active=1
						WHERE  b.territory_id IS NOT NULL
					GROUP BY c.id)');
			
			
	
		}
		// Bussiness Executive
			else if($role_id == 4)
			{
			$sql = ('(SELECT  c.id,c.territory_name
					FROM `pnh_m_territory_info`c
					JOIN  m_town_territory_link a ON c.id=a.territory_id
					WHERE   a.employee_id = '.$emp_id.'  AND a.is_active=1)
						UNION
					(SELECT  c.id,c.territory_name
					FROM `pnh_m_territory_info`c
					JOIN  m_town_territory_link a ON c.id=a.territory_id
					WHERE   a.employee_id = '.$emp_id1.'  AND a.is_active=1 AND a.id IS NULL)');
		}
		$terr_list_res = $this->db->query($sql);
	
			//echo $sql;
			$output = array();
			if($terr_list_res->num_rows())
			{
			$output['terr_list'] = $terr_list_res->result_array();
			$output['status'] = 'success';
			}
			else
			{
			$output['status'] = 'error';
				$output['message'] = 'no territories added yet';
			}
		echo json_encode($output);
	}
	
	function suggest_towns($territory_id,$emp_id1=0)
		{
	
			$town_list=$this->db->query("(SELECT a.territory_id,a.id AS town_id,a.town_name
			FROM pnh_towns a
			LEFT JOIN m_town_territory_link b ON b.territory_id = a.territory_id AND b.town_id = a.id AND b.is_active = 1
			WHERE a.territory_id = '$territory_id' AND b.id IS NULL )
			UNION
			(SELECT a.territory_id,a.id AS town_id,a.town_name
					FROM pnh_towns a
					JOIN m_town_territory_link b ON b.territory_id = a.territory_id AND b.town_id = a.id AND b.is_active = 1
					WHERE a.territory_id =  '$territory_id' AND b.id IS NOT NULL AND b.employee_id='$emp_id1')");
	
					$output=array();
					if($town_list->num_rows())
					{
					$output['town_list']=$town_list->result_array();
			$output['status']='success';
					}
					else
				{
							$output['status']='error';
						$output['message']='No Towns found to link';
				}
	
			echo json_encode($output);
		}

			function get_assigned_towns($emp_id)
			{
			$twn_terry_link=$this->db->query("SELECT c.territory_name,b.town_name,a.employee_id
				FROM m_town_territory_link a
				JOIN `pnh_towns` b ON b.id=a.town_id
				JOIN `pnh_m_territory_info`c ON c.id=a.territory_id
				WHERE employee_id=?",$emp_id);
	
		$output=array();
			if($twn_terry_link->num_rows())
			{
			$output['twn_terry_link']=$twn_terry_link->result_array();
			$output['status']='success';
			}
	
			else
			{
			$output['status']='error';
				$output['message']='No Territory Link Found';
			}
	
		echo json_encode($output);
	}
		
	function get_terry_link_byempid($role_id)
		{
			$unlinkd_terrylink=$this->db->query("SELECT  c.id,c.territory_name,d.job_title
										FROM `pnh_m_territory_info`c
										JOIN  m_town_territory_link a ON c.id=a.territory_id AND a.employee_id = ? AND a.is_active=1
										JOIN `m_employee_info`d ON d.employee_id=a.employee_id
										LEFT JOIN m_town_territory_link b ON b.territory_id=a.territory_id AND b.employee_id !=? AND b.is_active=1
										WHERE  b.territory_id IS NULL AND  d.job_title=?",$role_id);

				$output=array();
				if($unlinkd_terrylink->num_rows())
				{
					$output['unlinkd_terrylink']=$unlinkd_terrylink->result_array();
					$output['status']='success';
			    }
			else
			{
						$output['status']='error';
						$output['message']='No territory found';
			}
			echo json_encode($output);
		}
	
//---------------------------------------------END OF EDIT EMPLOYEE--------------------------------------------------------------------------
	
//--------------------------------------------------VIEW EMPLOYEE--------------------------------------------------------------------------------	
	function view_employee($emp_id='')
	{
		$this->task_status = $this->config->item('task_status');
		$user=$this->auth();
			
			$emp_details=$this->erpm->get_empinfo($emp_id);
			$isactive_territories=$this->erpm->isactive_territorylist();
			$unactive_territories=$this->erpm->unactive_territorylist();
			$isactive_towns=$this->erpm->isactive_townlist();
			$unactive_towns=$this->erpm->unactive_townlist();
			$assignment_details=$this->erpm->to_get_assignmnt_details($emp_id);
			$upcoming_tasks=$this->erpm->get_upcomingtasksbyempid($emp_id);
			$completed_tasks=$this->erpm->get_completedtasksbyempid($emp_id);
			$closed_tasks=$this->erpm->get_closedtasksbyempid($emp_id);
	
			$data['unactive_territories']=$unactive_territories;
			$data['isactive_territories']=$isactive_territories;
			$data['isactive_towns']=$isactive_towns;
			$data['unactive_towns']=$unactive_towns;
			$data['emp_details']=$emp_details;
			$data['upcoming_tasks']=$upcoming_tasks;
			$data['completed_tasks']=$completed_tasks;
			$data['closed_tasks']=$closed_tasks;
			$data['page']='view_emp';
			
			$this->load->view('admin',$data);
		
	}
	
	function jx_view_activitylog()
	{
		$task_status=array();
		
	 	$task_status[0]='Closed';
	 	$task_status[1]='Pending';
		$task_status[2]='Complete';
		$task_status[3]='Closed';
		
		
		$task_id=$this->input->post('task_id');
		$t_activitylog=$this->db->query('SELECT DATE_FORMAT(start_date,"%d/%m/%y") AS start_date,DATE_FORMAT(end_date,"%d/%m/%y") AS end_date,msg,DATE_FORMAT(logged_on,"%b %d %Y %h:%i %p") AS logged_on,logged_by,task_status,b.name
											FROM t_pnh_taskactivity a
											JOIN king_admin b ON b.id=a.logged_by
											WHERE task_id=?
											ORDER BY logged_on DESC',$task_id);
		
		$output = array();
		if($t_activitylog->num_rows())
		{
			$output['activity_log']=$t_activitylog->result_array();
			$output['task_status_list']=$task_status;
			
		
			$output['status']='success';
		}
		else
		{
			$output['status']='error';
		}
		echo json_encode($output);
	}
//-----------------------------------------------END OF VIEW EMPLOYEE--------------------------------------------------------------------------
	
//-----------------------------------------------LIST EMPLOYEE-------------------------------------------------------------------	
	function list_employee($role_id=0,$territory_id=0,$pg=0)
	{
		$cond='';
		if($role_id)
			$cond.=' and a.job_title='.$role_id;
		
		if($territory_id )
			$cond.=' and c.territory_id='.$territory_id;

		$emp_list=$this->db->query("SELECT *,b.role_name FROM  m_employee_info a
									JOIN m_employee_roles b ON b.role_id=a.job_title
									left join m_town_territory_link c on c.employee_id=a.employee_id
									WHERE 1 $cond 
									group by a.employee_id
									ORDER BY a.name asc
									limit $pg,".MAX_ROWS_DISP)->result_array();

		$access_roles = $this->erpm->get_emp_access_roles();
		$territories = $this->erpm->to_get_all_territories();

		$this->load->library('pagination');
		$config['base_url'] = site_url('/admin/list_employee/'.$role_id.'/'.$territory_id);
		$config['total_rows'] = $this->db->query("SELECT COUNT(DISTINCT a.employee_id) AS total FROM  m_employee_info a
													JOIN m_employee_roles b ON b.role_id=a.job_title
													left join m_town_territory_link c on c.employee_id=a.employee_id
													WHERE 1 $cond")->row()->total;
		$config['per_page'] = MAX_ROWS_DISP;
		$config['uri_segment'] = 5;
		$this->config->set_item('enable_query_strings',false);
		$this->pagination->initialize($config);
		$pagination = $this->pagination->create_links();
		$this->config->set_item('enable_query_strings',true);

		$data['access_roles']=$access_roles;
		$data['pg']=$pg;
		$data['territories']=$territories;
		$data['pagination'] =$pagination;
		$data['emp_list']=$emp_list;
		$data['page']='list_employee';
		$this->load->view("admin",$data);
	}

//----------------------------------------END OF LIST EMPLOYEE--------------------------------------------------------------------------


//-----------------------Assignment History-------------------------------	
	function assignment_histroy()
	{
		$assignment_histroy = $this->erpm->to_get_assignmnt_histroy();
		$data['assignment_histroy']=$assignment_histroy;
		$data['page']="assignment_histroy";
		$this->load->view("admin",$data);
	}
//--------------------------------ROLETREE----------------------------------------------------------------------------------------------
	function roletree_view()
	{
		$data['page']="roletree_view";
		$this->load->view("admin",$data);
	}
	
//-----------------------------------------------------------------------------------------------------------------------------	

	function get_emproleidbyuid($userid)
	{
		return $this->db->query("select job_title from m_employee_info where user_id = ? ",$userid)->row()->job_title;
	}
	
	function get_empidbyuid($userid)
	{
		return $this->db->query("select employee_id from m_employee_info where user_id = ? ",$userid)->row()->employee_id;
	}
	
	function get_jobrolebyuid($userid)
	{
		return $this->db->query("select job_title as role_id from m_employee_info where user_id = ? ",$userid)->row()->role_id;
	}

	function auth_pnh_employee()
	{
		$userdet=$this->auth(PNH_EMPLOYEE);
		$is_superadmin=$this->erpm->auth(true,true);
		if($is_superadmin)
		{
			$userdet['userid'] = 1;
		}
		
		return $userdet;
	}
//-----------------------------------------------------------------------------------------------------------------------------	

//-------------------------------------------CALANDER-----------------------------------------------------------------------	
	/**
	 * function to add Employee Events in Calander
	 */ 
	function calender()
	{
		$user = $this->auth_pnh_employee();
		$role_id=$this->get_jobrolebyuid($user['userid']);
		if($role_id<=3)
		{
		$role_id = $this->get_emproleidbyuid($user['userid']);
		$emp_id  =  $this->get_empidbyuid($user['userid']);
		$sub_emp_ids=$this->get_subordinates($this,$role_id,$emp_id);
		$territory_list=$this->load_territoriesbyemp_id($emp_id,$role_id);
		$t_sub_emp_ids = $sub_emp_ids;
		$get_locationbyempid = $this->assigned_location($emp_id);
		
		array_push($t_sub_emp_ids,$emp_id);
	 	$terry_id = $this->input->post('view_byterry');
		$emp_sub_list = array();
		$sql="SELECT a.employee_id,a.employee_id AS id,IFNULL(b.parent_emp_id ,0) AS parent,a.name as employee_name, a.name
				FROM m_employee_info a
				LEFT JOIN m_employee_rolelink b ON b.employee_id=a.employee_id and b.is_active = 1  
				WHERE  a.employee_id IN (".implode(',',$t_sub_emp_ids).")";
				
        $res=$this->db->query($sql);
		if($res->num_rows())
		{
			$emp_sub_list=$res->result_array();
		}
		$empid = $this->input->post('emp_id');
		$task_list=$this->db->query('SELECT task,asgnd_town_id,assigned_to,b.town_name,c.name,a.on_date 
											FROM pnh_m_task_info a
											JOIN pnh_towns b ON b.id=a.asgnd_town_id
											JOIN m_employee_info c ON c.employee_id=a.assigned_to
											where a.assigned_to = 6 ')->result_array();
		$this->load->plugin('yentree_pi');//plugin to create employeeTree 
		$emp_sub_list[0]['parent']=0;
		$data['emp_tree_config']=build_yentree($emp_sub_list);
		
		unset($emp_sub_list[0]);
		$emp_sub_list = array_values($emp_sub_list);
		
		$data['emp_sub_list']=$emp_sub_list;
		$data['get_locationbyempid']=$get_locationbyempid;
		$data['territory_list']=$territory_list;
		$data['task_list']=$task_list;
		$data['page']="calender";
		$this->load->view("admin",$data);
		
		}
	}
	
	
	/**
	 * function to load employee task via ajax
	 */
	function jx_load_tasklist()
	{
		//$user=$this->auth('EMP_MANAGE_TASK');
		$user=$this->auth_pnh_employee();
		$role_id=$this->get_jobrolebyuid($user['userid']);
		if($role_id<=3)
		{
		$town_id='';
		$territory_id='';
		$tasklist='';
		$town_id = $this->input->post('town_id');
		$empid = $this->input->post('emp_id');
		$loggdin_userid=$this->input->post('user_id');
		$territory_id  = $this->input->post('territory_id');
		
		$st_d = $this->input->post('start')/1000;
		$en_d = $this->input->post('end')/1000;
		//to covert timestamp to date
		$st_dt=gmdate("Y-m-d", $st_d);
		$en_dt=gmdate("Y-m-d", $en_d); 
		$cond= '';
		
		if($territory_id)
		{
			$cond .= ' and a.territory_id = '.$territory_id.' ';
		} 
		if($town_id)
		{
			$cond .= ' and asgnd_town_id = '.$town_id.' ';
		}
		
		
	   if($loggdin_userid)
	    {
			$empid = $this->get_empidbyuid($user['userid']); 
	    }
	    else
	    {
	    	if(!$empid && $cond =='')
	    	{
	    		
	    		$empid = $this->get_empidbyuid($user['userid']);
	    	}
	    }
	    
	    if($empid )
	    {
	    	$cond .= ' and ( assigned_to = '.$empid.' or assigned_by =  '.$empid.' ) ';
	    }
	    
		
		$tasklist = $this->db->query("SELECT a.territory_id as territory_id,b.id as id,b.task_title as title,concat('task_status_',b.task_status) as className,b.task as task,date(on_date) as start,date(due_date) as end,b.asgnd_town_id as town_id,c.town_name as town_name,d.name as employee_name
											FROM m_town_territory_link  a
										 	JOIN pnh_m_task_info b ON b.assigned_to=a.employee_id
										 	JOIN pnh_towns c on c.id = b.asgnd_town_id
										 	JOIN m_employee_info d on d.employee_id=b.assigned_to
											where  date(on_date) >=? and date(due_date) <= ? and a.is_active=1 and b.is_active=1 ".$cond." GROUP BY b.id ",array($st_dt,$en_dt));
		
	  
	    $output = array();
		if($tasklist->num_rows())
		{
			$output['tasklist']=$tasklist->result_array();
			$output['status']='success';
		}
		echo json_encode($output);
	}
	
	}
	
	function jx_add_emptask()
	{
		//print_r($_POST);
		
		$user=$this->auth_pnh_employee();
		$role_id=$this->get_jobrolebyuid($user['userid']);
		if($role_id<=3)
		{
		$emp_id = $this->get_empidbyuid($user['userid']);
		$task_title=$this->input->post('title');
		
		$task=$this->input->post('task');
		$emp_assigned_to = $this->input->post('assigned_to');
		$assignd_town_id=$this->input->post('assigned_town');
		$task_types=$this->input->post('choose_task_type');
		$franchise_ids=$this->input->post('tsk_frid');
		$req_msg=$this->input->post('reqst_msg');
		
		$st_date=$this->input->post('tsk_stdate');
		$st_dt=explode('/',$st_date);
		$d=$st_dt[0];
		$m=$st_dt[1];
		$y=$st_dt[2];
		$st_dte=mktime(0,0,0,$m,$d,$y);
		$st_dt=date('Y-m-d',$st_dte);
		
		//$st_dt=date('Y-m-d H:i:s',strtotime($st_date));
		$due_date=$this->input->post('due_date');
		//$en_dt=date('Y-m-d H:i:s',strtotime($due_date));
		$en_dt=explode('/',$due_date);
		$d=$en_dt[0];
		$m=$en_dt[1];
		$y=$en_dt[2];
		$en_dte=mktime(0,0,0,$m,$d,$y);
		$en_dt=date('Y-m-d',$en_dte);
		
		$task_type_ids = implode(',',$task_types);

		
		
		// creatre task entry
		
		
		
		
		$sql="insert into pnh_m_task_info(task_title,task,task_type,asgnd_town_id,on_date,due_date,assigned_by,assigned_to,is_active,task_status,assigned_on)values(?,?,?,?,?,?,?,?,1,1,now())" ;
		$task_res=$this->db->query($sql,array($task_title,$task,$task_type_ids,$assignd_town_id,$st_dt,$en_dt,$emp_id,$emp_assigned_to)) ;
		$task_id=$this->db->insert_id();
		
		$ins_data=array();
		$ins_data['task_id']=$task_id;
		/* if($req_msg)
		{
		$ins_data['msg']=$req_msg;
		}
		else 
			$req_msg=''; */
		$ins_data['start_date']=$st_dt;
		$ins_data['end_date']=$en_dt;
		$ins_data['task_status']=1;
		$ins_data['logged_by']=$user['userid'];
		$ins_data['logged_on']=date('Y-m-d H:i:s');
		$this->db->insert('t_pnh_taskactivity',$ins_data);
		
		foreach($task_types as $task_type)
		{
			if($task_type==1)
			{
				
				$avg_sales=$this->input->post('avg_sales');
				$target_amt=$this->input->post('tg_sales');
				if($franchise_ids){
				foreach($franchise_ids as $i=>$franchise_id){
				
					$sql="insert into pnh_m_sales_target_info(task_id,f_id,target_amount,avg_amount,status,created_on,created_by)values(?,?,?,?,0,now(),?)";
					$this->db->query($sql,array($task_id,$franchise_id,$target_amt[$i],$avg_sales[$i],$user['userid']));
				
						
					}
				}
			}
			
			if($task_type==2)
			{
				
				$pc_current_bal=$this->input->post('pc_current_bal');
				$req_msg=$this->input->post('reqst_msg');
				if($req_msg)
				foreach($req_msg[$task_type] as $req_text)
				{
					$sql="insert into pnh_task_type_details(task_id,task_type_id,custom_field_1,request_msg,created_on,created_by)values(?,?,?,?,now(),?)";
					$this->db->query($sql,array($task_id,$task_type,$pc_current_bal,$req_text,$user['userid']));
				}
			}
			if($task_type>2)
			{
				$req_msg=$this->input->post('reqst_msg');
				if($req_msg)
				foreach($req_msg[$task_type] as $req_text)
				{
					$sql="insert into pnh_task_type_details(task_id,task_type_id,request_msg,created_on)values(?,?,?,now())";
					$this->db->query($sql,array($task_id,$task_type,$req_text));
				}
			}
		}
		
		
		$output = array();
		if ($task_id)
		{
			$output['id'] = $task_id;
			$output['status'] = 'success';
		}
		else 
		{
			$output['status'] = 'error';
		}
	  	echo json_encode($output);
	}

 }
 
/* function _task_activity($emp_id,$task_id,$req_text)
 {
 	$user=$this->auth_pnh_employee();
 	$emp_id = $this->get_empidbyuid($user['userid']);
 	
 	$ins_data=array();
 	$ins_data['task_id']=$task_id;
 	$ins_data['remarks']=$req_text;
 	$ins_data['task_status']=1;
 	$ins_data['logged_by']=$emp_id;
 	$ins_data['logged_on']=date('dd/mm/yy H:i:s');
 	$this->db->insert('t_pnh_taskactivity',$ins_data);
 		
 }*/
 
	

	/**
	 * function to load task based on emp_id
	 */
	function jx_loadall_tasklist($st=0)
	{
		
		$output = array();
		
		$tr_id=$this->input->post('trid');
		$tw_id=$this->input->post('twid');
		$emp_id=$this->input->post('emp_id');
		
		$cond = '';
		if($tr_id)
			$cond .= ' and b.territory_id = '.$tr_id;
		if($tw_id)
			$cond .= ' and b.id = '.$tw_id;
		if($emp_id)
			$cond .= ' and (a.assigned_to = "'.$emp_id.'" or  a.assigned_by = "'.$emp_id.'" )';
		
		$emp_task_list=$this->db->query('SELECT a.id,task,asgnd_town_id,assigned_to,b.town_name,c.name,date(a.on_date) as on_date_str,DATE_FORMAT(a.on_date,"%D %M %y") as on_date
											FROM pnh_m_task_info a
											JOIN pnh_towns b ON b.id=a.asgnd_town_id
											JOIN m_employee_info c ON c.employee_id=a.assigned_to
											where 1 and a.is_active=1'.$cond.' order by on_date asc  limit '.$st.',5', array($emp_id,$tw_id,$tr_id));
		 
		$output['total_rows']=$this->db->query('select count(*) as total
												FROM pnh_m_task_info a
												JOIN pnh_towns b ON b.id=a.asgnd_town_id
												JOIN m_employee_info c ON c.employee_id=a.assigned_to
												where 1 and a.is_active=1 '.$cond,array($emp_id,$tw_id,$tr_id))->row()->total;
		
		$date_summ_res=$this->db->query('select DATE(on_date) AS assigned_date,COUNT(*) AS ttl_tasks
												FROM pnh_m_task_info a
												JOIN pnh_towns b ON b.id=a.asgnd_town_id
												JOIN m_employee_info c ON c.employee_id=a.assigned_to
												where 1 and a.is_active=1 '.$cond.' 
												GROUP BY assigned_date 
												ORDER BY assigned_date ',array($emp_id,$tw_id,$tr_id));
		 
		$output['date_summ'] = array();
		if($date_summ_res->num_rows())
		{
			$output['date_summ'] = $date_summ_res->result_array();
		}
		
		if($emp_task_list->num_rows())
		{
			$output['emp_task_list']=$emp_task_list->result_array();
			$output['status']='success';
		}
		else 
		{
			$output['status']='error';
			$output['message']='No data found';
		}
		
		echo json_encode($output);
	}
	
	function jx_load_taskdet()
	{
		$user=$this->auth_pnh_employee();
		$emp_id = $this->get_empidbyuid($user['userid']);
		$id = $this->input->post('id');
		$task_types=$this->db->query("select task_type from pnh_m_task_info where id=?",$id)->row()->task_type	;
		
		$task_type_names_res = $this->db->query("SELECT * FROM `pnh_m_task_types` ")->result_array();
		$franchise_names_res=$this->db->query("select * from pnh_m_franchise_info")->result_array();
	
		$task_type_names = array();
		foreach($task_type_names_res as $task_type_name)
		{
			$task_type_names[$task_type_name['id']] = $task_type_name['task_type']; 
		}
	
		$tasks_type_for=array();
		foreach($task_type_names_res as $task_for)
		{
			$tasks_type_for[$task_for['id']] = $task_for['task_for'];
		}
	
		$franchise_names=array();
		foreach($franchise_names_res as $franchise_name)
		{
			$franchise_names[$franchise_name['franchise_id']] = $franchise_name['franchise_name'];
		}
	
		$task_types_arr = explode(',',$task_types);
		
		$taskdet_res=$this->db->query("SELECT a.id AS id,h.contact_no,a.task_title,a.task,a.task_type,a.asgnd_town_id,a.assigned_by,a.assigned_to,CONCAT('task_status_',a.task_status) AS className,task_status AS `status`, a.assigned_on,a.comments AS reason,b.town_name AS town_name,h.name AS assigned_toname,i.name AS assigned_byname,DATE_FORMAT(on_date,'%d/%m/%Y')  AS `start`,DATE_FORMAT(due_date,'%d/%m/%Y')  AS `end`,j.short_frm AS assigned_torole,CONCAT(h.name ,'(',j.short_frm,')') AS assignedto_byrole_name,
										k.short_frm AS assigned_byrole,CONCAT(i.name ,'(',k.short_frm,')') AS assigned_byrole_name
										FROM pnh_m_task_info a
										JOIN pnh_towns b ON b.id=a.asgnd_town_id
										JOIN m_employee_info h ON h.employee_id=a.assigned_to
										JOIN m_employee_info i ON i.employee_id=a.assigned_by
										JOIN m_employee_roles j ON j.role_id=h.job_title
										JOIN m_employee_roles k ON k.role_id=i.job_title
										WHERE a.id =?" ,array($id));
		
		$output = array();
		if($taskdet_res->num_rows())
		{
			$output['task']=$taskdet_res->row_array();
			$output['task_type_names']=$task_type_names;
			$output['franchise_names']=$franchise_names;
			$output['tasks_type_for']=$tasks_type_for;
			
			foreach($task_types_arr as $task_type)
			{
				if($task_type==1)
				{
					$sales_target=$this->db->query("SELECT a.f_id,a.avg_amount,a.target_amount,a.actual_target,c.franchise_name
													FROM `pnh_m_sales_target_info`a
													JOIN pnh_m_task_info b ON a .task_id=b.id
													JOIN pnh_m_franchise_info c ON c.franchise_id=a.f_id
													WHERE  b.id=?
													GROUP BY a.f_id",$id);
					$output['sales_target']=$sales_target->result_array();
				}
				
				
				if($task_type==2)
				{
					$task_description=$this->db->query("SELECT custom_field_1,task_id,request_msg,b.task_type,b.task_for FROM `pnh_task_type_details`a
														JOIN pnh_m_task_types b ON b.id=a.task_type_id
														WHERE task_id=? and  task_type_id=?",array($id,$task_type));
					$output['task_type_list'][$task_type]=$task_description->result_array();
				}
				
				if($task_type>2)
				 {
					$task_description=$this->db->query("SELECT task_id,request_msg,b.task_type,b.task_for FROM `pnh_task_type_details`a
														JOIN pnh_m_task_types b ON b.id=a.task_type_id
														WHERE task_id=? and  task_type_id=?",array($id,$task_type));
					$output['task_type_list'][$task_type]=$task_description->result_array();
				}
				
				
			}
			$output['task_type']=$task_type;
			$output['status']='success';
		}
		else
		{
			$output['status']='error';
		}
		$output['emp_id']= $emp_id;
		echo json_encode($output);
	}
	
	function jx_upd_emptask()
	{ 
		
		$user=$this->auth_pnh_employee();
		$emp_id = $this->get_emproleidbyuid($user['userid']);
		$role_id=$this->get_jobrolebyuid($user['userid']);
		if($role_id<=3)
		{
		$id=$this->input->post('task_id');
		$task_title=$this->input->post('title');
		$task=$this->input->post('task');
		$st_date=$this->input->post('tsk_stdate');
			if($st_date)
			{
				$st_dt = explode('/',$st_date);
				$d=$st_dt[0];
				$m=$st_dt[1];
				$y=$st_dt[2];
				$st_dte=mktime(0,0,0,$m,$d,$y);
				$st_dt=date('Y-m-d',$st_dte);
			}
		//$st_dt=date('Y-m-d H:i:s',strtotime($st_date));

			$due_date=$this->input->post('due_date');
			if($due_date)
			{
				$en_dt=explode('/',$due_date);
				$d=$en_dt[0];
				$m=$en_dt[1];
				$y=$en_dt[2];
				$en_dte=mktime(0,0,0,$m,$d,$y);
				$en_dt=date('Y-m-d',$en_dte);
			}
		//$en_dt=date('Y-m-d H:i:s',strtotime($due_date));

		$emp_assigned_to = $this->input->post('assigned_to');
		$task_status=$this->input->post('status');
		$task_msg=$this->input->post('msg');
		
		
		$assignd_town_id=$this->input->post('assigned_town');
		$franchise_ids=$this->input->post('tsk_frid');
			if(!$franchise_ids)
				$franchise_ids=array(0);
			
			
			$task_types=$this->db->query("select task_type from pnh_m_task_info where id=?",$id)->row()->task_type	;
			$task_types_arr = explode(',',$task_types);
			$target_amt=$this->input->post('sales_tg');
			$collected_amt=$this->input->post('collectd_amt');
			$response_msg=$this->input->post('msg');
			
			
			$ins_data=array();
			$ins_data['task_id']=$id;
			if($st_date)
			{
			$ins_data['start_date']=$st_dt;
			}
			else 
				$st_dt=' ';
			
			if($due_date)
			{
			$ins_data['end_date']=$en_dt;
			}else 
				$en_dt=' ';
			
			if($response_msg)
			{
			$ins_data['msg']=$response_msg;
			}
			if($task_status)
			{
			$ins_data['task_status']=$task_status;
			}
			$ins_data['logged_by']=$user['userid'];
			$ins_data['logged_on']=date('Y-m-d H:i:s');
			$this->db->insert('t_pnh_taskactivity',$ins_data);
			
			foreach($task_types_arr as $task_type)
			{

				if($task_status==1)
				{
					$sql="update pnh_m_task_info set on_date=?,due_date=?,comments=? where id=?";
					$this->db->query($sql,array($st_dt,$en_dt,$task_msg,$id));
					
				}
					if($task_type==1)
					{
						
						$target_amt=$this->input->post('tg_sales');
						if($target_amt)
						foreach($target_amt as $franchise_id=>$target_cash)
						{
							$sql="update pnh_m_sales_target_info set target_amount=?,status=1,modified_on=now(),modified_by = ? where task_id=? and f_id=?";
							$this->db->query($sql,array($target_cash,$user['userid'],$id,$franchise_id));
						}
					}
					
					if($task_type!=1)
					{
						$req_msg =$this->input->post('view_reqst_msg');
						if($req_msg)
						foreach($req_msg as $task_type_id=> $req_text){
						$sql="update pnh_task_type_details set request_msg=? where task_id=? and task_type_id=?";
						$this->db->query($sql,array($req_text,$id,$task_type_id));
					}
				}
			}
			
			if($task_status==2)
			{
				$sql="update pnh_m_task_info set task_status=?,comments=?,completed_on=now(),completed_by=? where id=?";
				$this->db->query($sql,array($task_status,$task_msg,$user['userid'],$id));
				

			}
			if($task_status==3)
			{
				$sql="update pnh_m_task_info set task_status=?,comments=?,cancelled_on=now(),cancelled_by=? where id=?";
				$this->db->query($sql,array($task_status,$task_msg,$user['userid'],$id));
			}
		$output = array();
		if ($id)
		{
			$output['status'] = 'success';
				
		}
		else
	 	{
			$output['status'] = 'error';
		}
		echo json_encode($output);
		
	}
}	

	function assigned_location($emp_id)
	{
		$user=$this->auth_pnh_employee();
		$role_id=$this->db->query('select job_title from m_employee_info where employee_id=?',$emp_id)->row()->job_title;
	
		if($role_id < 3)
		{
			/*return $this->db->query("SELECT d.id,d.territory_name,c.id as town_id,c.town_name
			 FROM `pnh_m_territory_info` d
					JOIN `pnh_towns`c ON d.id=c.territory_id
					WHERE  1
					ORDER BY territory_name,town_name")->result_array();*/
			return $this->db->query("SELECT employee_id,parent_emp_id,a.territory_id,a.town_id,b.town_name,c.territory_name
										FROM m_town_territory_link a
										LEFT JOIN pnh_towns b ON b.id=a.town_id
										RIGHT JOIN pnh_m_territory_info c ON c.id=a.territory_id
										WHERE a.is_active=1")->result_array();
		}
		else if($role_id < 5)
		{
			return $this->db->query("SELECT d.id,d.territory_name,c.id as town_id,c.town_name
					FROM `m_town_territory_link` a
					LEFT JOIN `pnh_m_territory_info` d ON d.id=a.territory_id
					LEFT JOIN `pnh_towns`c ON c.territory_id=d.id
					WHERE a.is_active=1 AND  a.employee_id  = $emp_id
					GROUP BY town_name
					ORDER BY territory_name,town_name")->result_array();
		}else
		{
			return $this->db->query("SELECT d.id,d.territory_name,c.id as town_id,c.town_name
					FROM `m_town_territory_link` a
					LEFT JOIN `pnh_m_territory_info` d ON d.id=a.territory_id
					LEFT JOIN `pnh_towns`c ON c.id=a.town_id
					WHERE a.is_active=1 AND town_id <> 0 AND a.employee_id = $emp_id
					ORDER BY territory_name,town_name")->result_array();
			}
					 
	
			}

			function load_territoriesbyemp_id($emp_id,$role_id)
			{
	
			if($role_id<2)
				{
				return $territory_list=$this->db->query("SELECT id,territory_name
											FROM pnh_m_territory_info")->result_array();
	
				}
				if($role_id >2)
				{
				return $territory_list=$this->db->query("SELECT territory_id as id,b.territory_name
											 FROM m_town_territory_link
											 JOIN pnh_m_territory_info b ON b.id=territory_id
											 WHERE employee_id=?
											 GROUP BY territory_id",$emp_id)->result_array();
				}
	
				}
		function get_subordinates($obj,$role_id,$emp_id)
		{
			$user=$this->auth_pnh_employee();

			$sql="SELECT a.employee_id,a.job_title AS role_id,a.name,c.employee_id AS sub_emp_id,b.parent_emp_id AS sup_emp_id,c.job_title AS sub_role_id,c.name AS sub_emp_name
					FROM m_employee_info a
					JOIN `m_employee_rolelink` b ON a.employee_id = b.parent_emp_id  AND b.is_active = 1
					LEFT JOIN m_employee_info c ON c.employee_id = b.employee_id
					WHERE  a.job_title = ? AND a.employee_id = ? ";

		$res = $obj->db->query($sql,array($role_id,$emp_id));
		$temp=array();
			if($res->num_rows())
			{
				$sub_roles_data = $res->result_array();
				foreach($sub_roles_data as $sub_roles_det)
				{
				array_push($temp,$sub_roles_det['sub_emp_id']);
				$sub_temp=$this->get_subordinates($obj,$sub_roles_det['sub_role_id'],$sub_roles_det['sub_emp_id']);
				$temp=array_merge($temp,$sub_temp);
				$temp=array_filter($temp);
				}
			}
			return $temp;

		}
		
	function get_assignedempid($town_id)
	{
		$user=$this->auth_pnh_employee();
		$assignd_empid='';
		$assignd_empid=$this->db->query("SELECT a.employee_id,a.parent_emp_id,b.territory_id ,c.name AS b_name,d.name AS t_name,e.role_id,e.short_frm AS be ,d.job_title,f.short_frm AS tm
											FROM m_town_territory_link a
											JOIN pnh_towns b ON b.id= a.town_id
											JOIN m_employee_info c ON c.employee_id=a.employee_id
											LEFT JOIN m_employee_info d ON d.employee_id=a.parent_emp_id
											JOIN `m_employee_roles`e ON e.role_id=c.job_title
											JOIN `m_employee_roles`f ON f.role_id=d.job_title
											WHERE a.town_id=? AND a.is_active=1",$town_id);
		$output=array();
		if($assignd_empid->row())
			{
													
				$assignd_empid_row=$assignd_empid->row_array();
				$tmp=array();
				$tmp[0]=array('employee_id'=>$assignd_empid_row['employee_id'],'name'=>$assignd_empid_row['b_name'].' ('.$assignd_empid_row['be'].')');
				$tmp[1]=array('employee_id'=>$assignd_empid_row['parent_emp_id'],'name'=>$assignd_empid_row['t_name'].' ('.$assignd_empid_row['tm'].')');
				$output['assigned_empid']=$tmp;
				$output['status']='success';
			}
		else
			{
				$output['status']="error";
				$output['message']='No Bussiness Executive found';
			}
			echo json_encode($output);
		}

		function get_franchisebytwn_id($twn_id)
		{
			$user=$this->auth_pnh_employee();
			$franchise_list = $this->db->query("SELECT franchise_id,franchise_name
					FROM `pnh_m_franchise_info`
					WHERE town_id=? ", $twn_id);
			$output=array();
			if($franchise_list ->num_rows())
			{
				$output['franchise_list'] = $franchise_list->result_array();
				$output['status']='success';
			}
			else
			{
				$output['status']='errorr';
				$output['message']='No franchise Found';
			}
			echo json_encode($output);
		}
	

		function showtwn_lnkterr($terry_id=0)
		{
			$user=$this->auth_pnh_employee();
			$town_linkedtoterry=$this->db->query("SELECT town_name,id FROM pnh_towns WHERE territory_id = ?",$terry_id);
			$output=array();
			if($town_linkedtoterry->num_rows())
			{
				$output['town_linkedtoterry']=$town_linkedtoterry->result_array();
				$output['status']="Success";
			}
			else
			{
				$output['status']="Errorr";
				$output['message']='No Towns found For the Territory';
			}
			echo json_encode($output);
		}

		function taskdet_bytwnid($town_id=9999999999)
		{
			$user=$this->auth_pnh_employee();
			$town_id =(!is_numeric($town_id))?9999999999:$town_id;
     		$ttl_tsk_res=$this->db->query('select count(*) as total from pnh_m_task_info where asgnd_town_id=?',$town_id);
	     	$output=array();
	     	if($ttl_tsk_res->num_rows())
	     	{
		     	$output['task']=$ttl_tsk_res->row_array();
		     	$output['status']="success";
	     	}
	     	else
	     	{
	     		$output['status']='error';
	     		$output['message']='No data Found';
			}
			echo json_encode($output);
		}
					 
		
		function pendingtaskdet_bytwnid($town_id=9999999999)
		{
			$user=$this->auth_pnh_employee();
			$town_id =(!is_numeric($town_id))?9999999999:$town_id;
     		$pending_tsk_res=$this->db->query('select count(*) as pending from pnh_m_task_info where task_status=1 and is_active=1 and asgnd_town_id=?',$town_id);
			$output=array();
			if($pending_tsk_res->num_rows())
			{
				$output['task']=$pending_tsk_res->row_array();
				$output['status']="success";
			}
			else
			{
				$output['status']='error';
				$output['message']='No data Found';
			}
			echo json_encode($output);
		}
					 
					
		function get_fran_dailysales()
		{
			$user=$this->auth_pnh_employee();
			$output = array();
			$fids = $this->input->post('fids');
			if($fids)
			{
				$sql = "SELECT a.franchise_id,b.franchise_name,(amount*7) as avg_sales
				FROM king_transactions a
				JOIN pnh_m_franchise_info b ON a.franchise_id = b.franchise_id
				WHERE is_pnh = 1 AND a.franchise_id IN ($fids)
				GROUP BY a.franchise_id ";
				$res = $this->db->query($sql);
				if($res->num_rows())
					$output['data'] = $res->result_array();
				else
					$output['status'] = 'error';
			}
			else
			{
				$output['status'] = 'error';
			}
			echo json_encode($output);
		}
		
		function get_twn_currentbalance($town_id)
		{
			$user=$this->auth_pnh_employee();
			$town_id =(!is_numeric($town_id))?9999999999:$town_id;
			$output=array();
			//$town_id=$this->input->post('assigned_town');
			
				$current_balance=$this->db->query("SELECT SUM(current_balance) AS total_balance
						FROM `pnh_m_franchise_info`
						WHERE town_id=? AND FROM_UNIXTIME(created_on) <=NOW()",$town_id);
				
				if($current_balance->num_rows())
				{
					$output['balance']=$current_balance->row_array();
					$output['status']='Success';
				}
		
			else
		 	{
			 	$output['status']='error';
			 	$output['message']='No data Found';
			}
		echo json_encode($output);
		}
//------------------------------------------------END OF CALANDER-----------------------------------------------------------------------------	
/*	function jx_update_deltedtask()
	{
		$id=$this->input->post('task_id');
		$sql="update pnh_m_task_info set is_active=0 where id=?";
		$task_active_status=$this->db->query($sql,$id);
		$output=array();
		if($this->db->affected_rows()==1)
		{
			$output['task_active_status']=$task_active_status;
			$output['status']='success';
		}else{
			$output['status'] = 'error';
		}
		echo json_encode($output);
	}*/
	
//------------------------------------------ROUTES------------------------------------------------------------------------------	
	function manage_routes()
	{
		$routes=$this->db->query("select id,route_name from pnh_routes where is_active=1 order by route_name asc")->result_array();
		$data['routes']=$routes;
		$data['page']="manage_routes";
		$this->load->view("admin",$data);
	
	}
	
	function view_routes($route_id='')
	{
		$route_linked_twns=$this->erpm->route_linkd_twns($route_id);
		$data['route_linked_twns']=$route_linked_twns;
		$data['page']="pnh_towns";
		$this->load->view("admin",$data);
	}
	
	function process_routes()
	{
		$data['page']="manage_routes";
		$this->load->library('form_validation');
		$this->form_validation->set_rules('route_name','Name','required');
		$this->form_validation->set_rules('territory','Territory','required');
		if($this->form_validation->run()==true)
		{
				
			$route_name=$this->input->post('route_name');
			$linked_terry_ids=$this->input->post('territory');
			if($route_name)
				if($linked_terry_ids)
				foreach($linked_terry_ids as $linked_terry)
				{
					$sql="update pnh_towns set route_id='.$route_id.' where town_id='.town_id.'";
					$route_link = $this->db->query($sql,array($route_name,$linked_terry));
					$this->erpm->flash_msg("Employee Details Updated");
					redirect('admin/list_employee');
				}
		}else
			return false;
	}
	
	/**
	 * function to assign routes
	 */
	function jx_add_route()
	{
		$route_name=$this->input->post('route_name');
		$town=$this->input->post('towns');
			$t=0;
			$sql="insert into pnh_routes(route_name,is_active,created_on)values(?,1,now())";
			$this->db->query($sql,$route_name);
			$route_id=$this->db->insert_id();
			foreach($town as $twn)
			{
				
				$sql="update pnh_towns set route_id=? where id=?";
				$this->db->query($sql,array($route_id,$twn));
				$t+=$this->db->affected_rows();
		    }
		$output=array();
		if($t)
		{
			$output['status']="success";
		}
	  	else
	  	{
			$output['status']="error";
		}
		echo json_encode($output);
	}
	
	function jx_load_routedet()
	{
		$id = $this->input->post('route_id');
		$routedet_res=$this->db->query("SELECT town_name,b.route_name,route_id 
										FROM pnh_towns a
										JOIN pnh_routes b ON b.id=a.route_id
										where a.route_id = ? " ,array($id));
		$output = array();
		if($routedet_res->num_rows())
		{
			$output['route']=$routedet_res->result_array();
			$output['status']='success';
		}
		else
		{
			$output['status']='error';
		}
		echo json_encode($output);
	}
	
	function jx_upd_route()
	{
		$route_id=$this->input->post('route_id');
		$route_name=$this->input->post('route_name');
		$town=$this->input->post('towns');
		$t=0;
		$sql="update pnh_routes set route_name=? where id= ?";
		$this->db->query($sql,array($route_name,$route_id));
		$route_id=$this->db->insert_id();
		foreach($town as $twn)
		{
			$sql="update pnh_towns set route_id=? where id=?";
			$this->db->query($sql,array($route_id,$twn));
			$t+=$this->db->affected_rows();
		}
		$output=array();
		if($t)
		{
			$output['status']="success";
		}
		else
		{
			$output['status']="error";
		}
		echo json_encode($output);
	}
	
//-----------------------------------------------END OF ROUTES-------------------------------------------------------------------------------	
	

//--------------------------------------------------------------------------------------------------------------------------     
  /*   function get_fran_currentbalance()
     {
     	$user=$this->auth_pnh_employee();
     	$output=array();
     	
     	$fids=$this->input->post('fids');
     	if($fids){
     		$sql="select franchise_id,franchise_name,current_balance from pnh_m_franchise_info where franchise_id in ($fids) ";
     		$res=$this->db->query($sql);
     		if($res->num_rows())
     		
     			$output['data'] = $res->result_array();
     			$output['status']='success';
     		}
     		else
     		{
     			$output['status']='error';
     			$output['message']='No Data Found';
     		}
     	echo json_encode($output);
     }*/
     
//---------------------------------------------TASK PRINT---------------------------------------------------------------- 
     function task_print($employee_id='0',$start_date='0')
     {
     	$user=$this->auth(PNH_PRINT_EMPLOYEE_TASKS);
     	$end_date=date('Y-m-d',strtotime($start_date)+6*24*60*60);
     	$task_details=$this->db->query("SELECT a.id,b.town_name,c.territory_name,a.assigned_to AS emp_id,d.name AS assigned_toname,f.role_name,e.name AS assigned_byname,a.task_type,DATE(a.on_date) AS on_date,a.due_date,a.task
		FROM pnh_m_task_info a
		JOIN pnh_towns b ON b.id=a.asgnd_town_id
		JOIN pnh_m_territory_info c ON c.id=b.territory_id
		JOIN m_employee_info d ON d.employee_id = a.assigned_to
		JOIN m_employee_info e ON e.employee_id = a.assigned_by
		JOIN m_employee_roles f ON f.role_id=d.job_title
		WHERE a.assigned_to = ? AND  a.on_date BETWEEN ? AND ? and a.is_active = 1 and a.task_status=1",array($employee_id,$start_date,$end_date));
     		if($task_details->num_rows())
	     	{
	     		
	     	}
	     	else 
	     	{
	     		
	     		return false;
	     	}
	     	//print_r($task_details);
	     	
	     	$week_dts=array();
	     	$startdt_ts=strtotime($start_date);
	     	for($i=0;$i<7;$i++)
	     	{
	     		$dt = date('Y-m-d',$startdt_ts +($i*24*60*60));
	  		 	if(!isset($week_dts[$dt]))
	     			$week_dts[$dt]=array();
	     	}
	     	
	     	$emp_list = array();
	     	
	     	foreach($task_details->result_array() as $task_det)
	     	{
	     		$tsk_ondt =  $task_det['on_date'];
	     		$tsk_duedt =  $task_det['due_date'];
	     		$tsk_empid = $task_det['emp_id']; 
	     		$employee_name=$task_det['assigned_toname'];
	     		$territory_name=$task_det['territory_name'];
	     		$role_name=$task_det['role_name'];
	     		
	     		$d_rem = (strtotime($tsk_duedt)-strtotime($tsk_ondt))/(24*60*60);
	     		for($k=0;$k<=$d_rem;$k++)
	     		{
	     			$tsk_schdt = date('Y-m-d',strtotime($tsk_ondt)+($k*24*60*60));
	     			if(!isset($week_dts[$tsk_schdt][$tsk_empid]))
	     				$week_dts[$tsk_schdt][$tsk_empid] = array();
	     			
	     			array_push($week_dts[$tsk_schdt][$tsk_empid],$task_det);
	     			$emp_list[$tsk_empid]=$employee_name ;
	     			 
	     		}
	     	 		
	     	}
	   	$data['page']="task_print";
	   	$data['week_dts']=$week_dts;
	   	$data['emp_list']=$emp_list;
	   	$data['tsk_ondt']=$tsk_ondt;
	   	$data['start_date']=$start_date;
	   	$data['end_date']=$end_date;
	   	$data['tsk_empid']=$tsk_empid;
	   	$data['territory_name']=$territory_name;
	   	$data['role_name']=$role_name;
	   	$data['assigned_toname']=$employee_name;
	   	

     	$this->load->view("admin/body/task_print",$data);
     }
//----------------------------------------------------END OF TASK PRINT----------------------------------------------------------------------     
     /**
      * function load employees by terr or town id 
      */
     function jx_getpnhemployees()
     {
     	$terr_id = $this->input->post('terr_id')*1;
     	$twn_id = $this->input->post('twn_id')*1;
     	
     	$cond = '';
     	if($terr_id)
     		$cond .= " AND territory_id = ".$terr_id;
     	
     	if($twn_id)
     		$cond .= " AND town_id = ".$twn_id;
     	
     	
     	$sql = "SELECT b.employee_id,b.name AS employee_name,b.job_title,c.short_frm AS role_name
						FROM `m_town_territory_link` a 
						JOIN m_employee_info b ON a.employee_id = b.employee_id 
						JOIN m_employee_roles c ON c.role_id=b.job_title
						WHERE b.job_title > 2 AND a.is_active = 1 $cond
						GROUP BY b.employee_id   
						ORDER BY employee_name ASC ";
    	$res = $this->db->query($sql);
     	$output = array();
     	if($res->num_rows())
     	{
     		$output['emp_list'] = $res->result_array();
     		$output['status'] = 'success';
     	}
     	else
     	{
     		$output['status'] = 'error';
     	}
     	echo json_encode($output);
	 }
	 
	 

	 

}