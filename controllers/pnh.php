<?php

class Pnh extends Controller{
	
	private $logid=0;
	private $fid=0;
	
	function __construct()
	{
		parent::__construct();
		if(empty($_POST))
			$_POST=$_GET;
		$this->load->model("erpmodel","erpm");
	}
	
	function pdie($msg)
	{
		if($msg)
			echo $msg;
		$ob=ob_get_contents();
		$this->db->query("insert into pnh_sms_log(msg,franchise_id,reply_for,created_on) values(?,?,?,?)",array($ob,$this->fid,$this->logid,time()));
		ob_flush();
		die;
	}
	
	function auth($from,$msg)
	{
		$this->db->query("insert into pnh_sms_log(msg,sender,created_on) values(?,?,?)",array($msg,$from,time()));
		$this->logid=$logid=$this->db->insert_id();
		$from=substr($from,1);
		if(empty($from) || strlen($from)<10)
			$this->pdie("Invalid mobile number");
		$fran=$this->db->query("select * from pnh_m_franchise_info where login_mobile1=? or login_mobile2=?",array($from,$from))->row_array();

		if(empty($fran))
			$this->pdie("Sorry, not able to authenticate you");
		if($fran['is_suspended']==1)
			$this->pdie("Your account is suspended. Please contact customer support");
			
		$this->fid=$fran['franchise_id'];
		$this->db->query("update pnh_sms_log set franchise_id=? where id=? limit 1",array($fran['franchise_id'],$logid));
		return $fran;
	}
	
	function process()
	{
		ob_start();
		 
		$msg=$this->input->post("Body");
		$from=$this->input->post("From");
		$fran=$this->auth($from, $msg);
		list($call)=explode(" ",$msg);
		$call=strtolower($call);
		$from=substr($from,1);
		switch($call)
		{
			case 'bal':
				$this->balance($from,$msg,$fran);
				break;
			case 'sta':
				$this->status($from,$msg,$fran);
				break;
			case 'cnl':
				$this->cancelOrder($from,$msg,$fran);
				break;
			case 'rcn':
				$this->recent($from,$msg,$fran);
				break;
			case 'che':
			case 'chk':
				$this->stock_check($from,$msg,$fran);
				break;
			case 'p':
			case 'price':
				$this->price_details($from,$msg,$fran);
				break;
			default :
				$this->createOrder($from, $msg,$fran);
				break;
		}
		$this->pdie(false);
	}
	
	function recent($from,$msg)
	{
		$fran=$this->db->query("select * from pnh_m_franchise_info where (login_mobile1=? and login_mobile1!=0) or (login_mobile2=? and login_mobile2!=0)",array($from,$from))->row_array();
		$orders=$this->db->query("select t.transid,t.amount,o.status,o.time,o.actiontime from king_transactions t join king_orders o on o.transid=t.transid where t.franchise_id=? group by o.transid order by o.sno desc,o.status desc limit 5",$fran['franchise_id'])->result_array();
		$stat=array("Confirmed","Confirmed","Shipped","Cancelled");
		if(empty($orders))
			$this->pdie("No orders made by you");
		foreach($orders as $o)
			echo "{$o['transid']} Rs {$o['amount']} ".($o['actiontime']==0?date("jS M",$o['time']):date("jS M",$o['actiontime']))." {$stat[$o['status']]}\n";
	}
	
	function status($from,$msg)
	{
		$inp=explode(" ",$msg);
		if(count($inp)<2)
			$this->pdie("No Order ID provided");
		list($call,$transid)=$inp;
		if(empty($transid))
			$this->pdie("No Order ID provided");
		$order=$this->db->query("select transid,status,actiontime from king_orders where transid=? order by status desc, actiontime desc limit 1",$transid)->row_array();
		if($order['status']==3)
			$this->pdie("Your order {$order['transid']} was cancelled on ".date("d/m/y",$order['actiontime']));
		else if($order['status']==2)
			$this->pdie("Your order {$order['transid']} was shipped on ".date("d/m/y",$order['actiontime']));
		else
			$this->pdie("Your order {$order['transid']} is confirmed and is yet to be shipped");
	}
	
	function stock_check($from,$msg)
	{
		$a=explode(" ",$msg);
		foreach($a as $i=>$s)
			if($i>0)
				$raw[]=$s;
		if(empty($raw))
			$this->pdie("No Product ID mentioned");
		$ppids=array();
		foreach($raw as $r)
			foreach(explode(",",$r) as $i)
				$ppids[]=$i;
		$ppids=array_unique($ppids);
		$itemids=array();
		foreach($this->db->query("select id,pnh_id from king_dealitems where pnh_id in ('".implode("','",$ppids)."') and is_pnh=1")->result_array() as $i)
			$itemids[$i['pnh_id']]=$i['id'];
		$avail=$this->erpm->do_stock_check($itemids);
		foreach($itemids as $ppid=>$itemid)
		 if(in_array($itemid,$avail))
		 	echo "$ppid-Y, \n";
		 else
		 	echo "$ppid-N, \n";
	}

	function balance($from,$msg)
	{
		$fran=$this->db->query("select current_balance from pnh_m_franchise_info where (login_mobile1=? and login_mobile1!=0) or (login_mobile2=? and login_mobile2!=0)",array($from,$from))->row_array();
		echo 'Dear Franchise, your current balance is Rs '.number_format($fran['current_balance'],2);
	}
	
	function price_details($from,$msg,$fran)
	{
		$frags=explode(" ",$msg);
		if(count($frags)<2)
			$this->pdie("Product ID is not entered. Please check your msg");
		$pid=$pnhid=$frags[1];
		$deal=$this->db->query("select dealid,is_combo,name,orgprice as mrp,price from king_dealitems where pnh_id=? and is_pnh=1",$pid)->row_array();
		if(empty($deal))
			$this->pdie("There is no product with entered PID $pid");
		if($this->db->query("select publish from king_deals where dealid=?",$deal['dealid'])->row()->publish==0)
			$this->pdie("Sorry, the product is not available now");
		$margin=$this->erpm->get_pnh_margin($fran['franchise_id'],$pid);
		if($deal['is_combo']=="1")
			$discount=$deal['price']/100*$margin['combo_margin'];
		else
			$discount=$deal['price']/100*$margin['margin'];
		$name=ucfirst($deal['name']);
		$cost=round($deal['price']-$discount,2);
		echo "{$name}\n Mrp : Rs {$deal['mrp']}, Landing Cost : Rs {$cost}";
	}
	
	function createOrder($from,$msg)
	{
		if(empty($from) || strlen($from)<10)
			$this->pdie("Invalid mobile number");
		$fran=$this->db->query("select * from pnh_m_franchise_info where (login_mobile1=? and login_mobile1!=0) or (login_mobile2=? and login_mobile2!=0)",array($from,$from))->row_array();
		$margin=$this->db->query("select margin,combo_margin from pnh_m_class_info where id=?",$fran['class_id'])->row_array();
		if($fran['sch_discount_start']<time() && $fran['sch_discount_end']>time() && $fran['is_sch_enabled'])
			$margin['margin']+=$fran['sch_discount'];
		$msg=str_replace("* ","*",$msg);
		$msg=str_replace(" *","*",$msg);
		$msg=str_replace(","," ",$msg);
		$msg=str_replace("  "," ",$msg);
		$payload=explode(" ",$msg);
		if(empty($payload) || count($payload)==1)
			$this->pdie("Invalid syntax");
		$npayload=array();
		foreach($payload as $p)
		{
			if($p{0}=="2")
				$mid=$p;
			else
				$npayload[]=$p;
		}
		$payload=$npayload;
		$npayload=array();
		foreach($payload as $p)
		{
			if(stripos($p,",")===false)
			{
				$npayload[]=$p;
				continue;
			}
			$npp=explode(",",$p);
			foreach($npp as $np)
				$npayload[]=$np;
		}
		$payload=$npayload;
		if(!isset($mid)) $this->pdie("No Member ID available");
		$items=array();
		foreach($payload as $p)
		{
			$pi=explode("*",$p);
			$it['pid']=trim($pi[0])+1-1;
			if(count($pi)!=2)
				$it['qty']=1;
			else
				$it['qty']=$pi[1];
			$items[]=$it;
		}
		$total=0;$d_total=0;
		$itemids=array();
		$itemnames=array();
		foreach($items as $i=>$item)
		{
			$prod=$this->db->query("select i.*,d.publish from king_dealitems i join king_deals d on d.dealid=i.dealid where i.is_pnh=1 and i.pnh_id=? and i.pnh_id!=0",$item['pid'])->row_array();
			if(empty($prod))
				$this->pdie("There is no product with ID : ".$item['pid']);
			if($prod['publish']!=1)
				$this->pdie("Product {$prod['name']} is not available");
			$items[$i]['mrp']=$prod['orgprice'];
			if($fran['is_lc_store'])
				$items[$i]['price']=$prod['store_price'];
			else
				$items[$i]['price']=$prod['price'];
			$margin=$this->erpm->get_pnh_margin($fran['franchise_id'],$item['pid']);
			$items[$i]['itemid']=$prod['id'];
			if($prod['is_combo']=="1")
				$items[$i]['discount']=$items[$i]['price']/100*$margin['combo_margin'];
			else
				$items[$i]['discount']=$items[$i]['price']/100*$margin['margin'];
			$total+=$items[$i]['price']*$items[$i]['qty'];
			$d_total+=($items[$i]['price']-$items[$i]['discount'])*$items[$i]['qty'];
			$itemids[]=$prod['id'];
			$itemnames[]=$prod['name'];
			$items[$i]['margin']=$margin;
			$items[$i]['tax']=$prod['tax'];
		}
		$avail=$this->erpm->do_stock_check($itemids);
		foreach($itemids as $i=>$itemid)
		 if(!in_array($itemid,$avail))
		 	$this->pdie("{$itemnames[$i]} is out of stock");
		if($fran['credit_limit']+$fran['current_balance']<$d_total)
			$this->pdie("Insufficient balance! Balance in your account Rs {$fran['current_balance']} Total order amount : Rs $d_total");
		if($this->db->query("select 1 from pnh_member_info where pnh_member_id=?",$mid)->num_rows()==0)
		{ 
			if($this->db->query("select 1 from pnh_m_allotted_mid where ? between mid_start and mid_end and franchise_id=?",array($mid,$fran['franchise_id']))->num_rows()==0)
			$this->pdie("Member ID $mid is not allotted to you");
			$this->db->query("insert into king_users(name,is_pnh,createdon) values(?,1,?)",array("PNH Member: $mid",time()));
			$userid=$this->db->insert_id();
			$this->db->query("insert into pnh_member_info(user_id,pnh_member_id,franchise_id) values(?,?,?)",array($userid,$mid,$fran['franchise_id']));
			$npoints=$this->db->query("select points from pnh_member_info where user_id=?",$userid)->row()->points+PNH_MEMBER_FEE;
			$this->db->query("update pnh_member_info set points=? where user_id=? limit 1",array($npoints,$userid));
			$this->db->query("insert into pnh_member_points_track(user_id,transid,points,points_after,created_on) values(?,?,?,?,?)",array($userid,"",PNH_MEMBER_FEE,$npoints,time()));
			$this->erpm->pnh_fran_account_stat($fran['franchise_id'],1,PNH_MEMBER_FEE-PNH_MEMBER_BONUS,"50 Credit Points purchase for Member $mid","member",$mid);
		}
		else
			$userid=$this->db->query("select user_id from pnh_member_info where pnh_member_id=?",$mid)->row()->user_id;
		$transid=strtoupper("PNH".random_string("alpha",3).$this->p_genid(5));
		$this->db->query("insert into king_transactions(transid,amount,paid,mode,init,actiontime,is_pnh,franchise_id) values(?,?,?,?,?,?,?,?)",array($transid,$d_total,$d_total,3,time(),time(),1,$fran['franchise_id']));
		foreach($items as $item)
		{
			$inp=array("id"=>$this->p_genid(10),"transid"=>$transid,"userid"=>$userid,"itemid"=>$item['itemid'],"brandid"=>"");
			$inp["brandid"]=$this->db->query("select d.brandid from king_dealitems i join king_deals d on d.dealid=i.dealid where i.id=?",$item['itemid'])->row()->brandid;
			$inp["bill_person"]=$inp['ship_person']=$fran['franchise_name'];
			$inp["bill_address"]=$inp['ship_address']=$fran['address'];
			$inp["bill_city"]=$inp['ship_city']=$fran['city'];
			$inp['bill_pincode']=$inp['ship_pincode']=$fran['postcode'];
			$inp['bill_phone']=$inp['ship_phone']=$fran['login_mobile1'];
			$inp['bill_email']=$inp['ship_email']=$fran['email_id'];
			$inp['bill_state']=$inp['ship_state']=$fran['state'];
			$inp['quantity']=$item['qty'];
			$inp['time']=time();
			$inp['ship_landmark']=$inp['bill_landmark']=$fran['locality'];
			$inp['bill_country']=$inp['ship_country']="India";
			$inp['i_orgprice']=$item['mrp'];
			$inp['i_price']=$item['price'];
			$inp['i_discount']=$item['mrp']-$item['price'];
			$inp['i_coup_discount']=$item['discount'];
			$inp['i_tax']=$item['tax'];
			$this->db->insert("king_orders",$inp);
			$m_inp=array("transid"=>$transid,"itemid"=>$item['itemid'],"mrp"=>$item['mrp'],"price"=>$item['price'],"base_margin"=>$item['margin']['base_margin'],"sch_margin"=>$item['margin']['sch_margin'],"bal_discount"=>$item['margin']['bal_discount'],"qty"=>$item['qty'],"final_price"=>$item['price']-$item['discount']);
			$this->db->insert("pnh_order_margin_track",$m_inp);
		}
		$this->erpm->pnh_fran_account_stat($fran['franchise_id'],1, $d_total,"Order $transid - Total Amount: Rs $total","order",$transid);
		$balance=$this->db->query("select current_balance from pnh_m_franchise_info where franchise_id=?",$fran['franchise_id'])->row()->current_balance;
		echo "Your order is placed successfully! Total order amount :Rs $total. Amount deducted is Rs $d_total. Your order ID is $transid Balance in your account Rs $balance";
		$points=$this->db->query("select points from pnh_loyalty_points where amount<? order by amount desc limit 1",$total)->row_array();
		if(!empty($points))
			$points=$points['points'];
		else $points=0;
		$apoints=$this->db->query("select points from pnh_member_info where user_id=?",$userid)->row()->points+$points;
		$this->db->query("update pnh_member_info set points=points+? where user_id=? limit 1",array($points,$userid));
		$this->db->query("insert into pnh_member_points_track(user_id,transid,points,points_after,created_on) values(?,?,?,?,?)",array($userid,$transid,$points,$apoints,time()));

		$franid=$fran['franchise_id'];
		$billno=10001;
		$nbill=$this->db->query("select bill_no from pnh_cash_bill where franchise_id=? order by bill_no desc limit 1",$franid)->row_array();
		if(!empty($nbill))
			$billno=$nbill['bill_no']+1;
		$inp=array("bill_no"=>$billno,"franchise_id"=>$franid,"transid"=>$transid,"user_id"=>$userid,"status"=>1);
		$this->db->insert("pnh_cash_bill",$inp);
		$this->erpm->do_trans_changelog($transid,"PNH Order placed through SMS by $from");
		
	}

		
	function cancelOrder($from,$msg)
	{
		$fran=$this->db->query("select * from pnh_m_franchise_info where (login_mobile1=? and login_mobile1!=0) or (login_mobile2=? and login_mobile2!=0)",array($from,$from))->row_array();
		list($call,$transid)=explode(" ",$msg);
		if(empty($transid))
			$this->pdie("No Order ID provided");
		if($this->db->query("select 1 from king_transactions where transid=? and franchise_id=?",array($transid,$fran['franchise_id']))->num_rows()==0)
			$this->pdie("Invalid Order ID");
		if($this->db->query("select 1 from king_orders where transid=? and status=3",$transid)->num_rows()!=0)
			$this->pdie("One or more of the items in Order $transid is already cancelled. Please contact customer support");
		if($this->db->query("select 1 from king_orders where transid=? and status=2",$transid)->num_rows()!=0)
			$this->pdie("Order $transid is already invoiced and cannot be cancelled");
		if($this->db->query("select 1 from king_orders where transid=? and status=1",$transid)->num_rows()!=0)
			$this->pdie("Order $transid is already shipped and cannot be cancelled");
		$this->erpm->do_trans_changelog($transid,"PNH Order cancelled through SMS");
		$this->db->query("update king_orders set status=3 where transid=?",$transid);
		$this->erpm->do_trans_changelog($transid,"PNH Order cancelled through SMS");
		$trans=$this->db->query("select * from king_transactions where transid=?",$transid)->row_array();
		$this->db->query("insert into t_refund_info(transid,amount,status,created_on,created_by) values(?,?,?,?,?)",array($transid,$trans['amount'],1,time(),1));
		$rid=$this->db->insert_id();
		foreach($this->db->query("select id,quantity from king_orders where transid=?",$transid)->result_array() as $i=>$ord)
		{
			$o=$ord['id'];
			$qty=$ord['quantity'];
			$this->db->query("insert into t_refund_order_item_link(refund_id,order_id,qty) values(?,?,?)",array($rid,$o,$qty));
			$this->db->query("update king_orders set status=3,actiontime=".time()." where id=? limit 1",$o);
		}
		$this->erpm->pnh_fran_account_stat($fran['franchise_id'],0,$trans['amount'],"Refund - Order $transid cancelled","refund",$transid);
		$nbalance=$this->db->query("select current_balance as b from pnh_m_franchise_info where franchise_id=?",$fran['franchise_id'])->row()->b;
		echo "Order $transid is cancelled and Rs {$trans['amount']} is credited back to your account. Your new balance is Rs $nbalance";
	}
	
	
	function p_genid($len)
	{
		$st="";
		for($i=0;$i<$len;$i++)
			$st.=rand(1,9);
		return $st;
	}
	
}

