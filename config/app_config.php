<?php
/**
 * Application Status Settings 
 */

/*
 * Transaction status flags
 */
$config ['trans_status'] = array ();
$config ['trans_status'] [0] = 'Pending';
$config ['trans_status'] [1] = 'Partially Invoiced';
$config ['trans_status'] [2] = 'Invoiced';
$config ['trans_status'] [3] = 'Partially Shipped';
$config ['trans_status'] [4] = 'Shipped';
$config ['trans_status'] [5] = 'Closed';
$config ['trans_status'] [6] = 'Cancelled';


/*
 * Order status flags 
 */
$config ['order_status'] = array ();
$config ['order_status'] [0] = 'Pending';
$config ['order_status'] [1] = 'Invoiced';
$config ['order_status'] [2] = 'Outscanned';
$config ['order_status'] [3] = 'Shipped';
$config ['order_status'] [4] = 'Delivered';
$config ['order_status'] [5] = 'Returned';
$config ['order_status'] [6] = 'Cancelled';



$config['task_for']=array();
$config['task_for'][0]='Undefined';
$config['task_for'][1]='Existing Franchise';
$config['task_for'][2]='New Franchise';


$config['task_status']=array();
$config['task_status'][0]='Undefined';
$config['task_status'][1]='Pending';
$config['task_status'][2]='Complete';
$config['task_status'][3]='Cancelled';
